master thesis
=============

Topic: NetAtmo private weather station data.

## Environment

Create the anaconda environment with:

```bash
conda env create -f environment.yml
# install gtk3 (not possible via environment.yml somehow...)
conda install -c conda-forge -c pkgw-forge gtk3
```
