\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{formulas}[2017/03/10 Formulas for my master thesis]

\RequirePackage{translations}
\RequirePackage{siunitx}
\RequirePackage{etoolbox}

% translations
\DeclareTranslationFallback{temperature}{temperature}
\DeclareTranslationFallback{surface}{surface}
\DeclareTranslationFallback{surface_temperature}
  {\GetTranslation{surface} \GetTranslation{temperature}}
\DeclareTranslationFallback{thermal_inertia}{thermal inertia}
\DeclareTranslationFallback{long_wave_out}{long-wave out}
\DeclareTranslationFallback{long_wave_in}{long-wave in}
\DeclareTranslationFallback{short_wave_in}{short-wave in}
\DeclareTranslationFallback{sensible_heat_flux}{sensible heat flux}
\DeclareTranslationFallback{latent_heat_flux}{latent heat flux}
\DeclareTranslationFallback{albedo}{albedo}
\DeclareTranslationFallback{internal_heat_conduction}{int. heat conduction}

% math style utilities
\newcommand{\Prime}[1]{{#1}'}
\newcommand{\minus}[1][1.0]{\scalebox{#1}[1.0]{-}}
\newcommand{\underbracetext}[2]{ \underbrace{#1}_{\substack{#2}} }
\newcommand{\Squared}[1]{{#1} ^ 2}
\newcommand{\Cubic}[1]{{#1} ^ 3}
\newcommand{\InSquareBraces}[1]{\left[ {#1} \right]}
\newcommand{\InRoundBraces}[1]{\left( {#1} \right)}
\newcommand{\ConditionBar}[2]{\left. {#1} \right|_{#2}}
\newcommand{\UnitBraces}[1]{\InSquareBraces{#1}}
\newcommand{\FunctionOf}[2]{{#1} \InRoundBraces{#2}}
\newcommand{\abs}[1]{\left| {#1} \right|}
\newcommand{\Mean}[1]{{\overline{#1}}}
\newcommand{\PartialDerivative}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\DiscreteDerivative}[2]{\frac{\Delta #1}{\Delta #2}}
\newcommand{\TotalDerivative}[2]{\frac{d #1}{d #2}}
\newcommand{\Minimum}[2]{\FunctionOf{\mathrm{min}}{#1,#2}}
\newcommand{\Maximum}[2]{\FunctionOf{\mathrm{max}}{#1,#2}}
\newcommand{\Error}[1]{\Delta {#1}}
\newcommand{\MaxError}[1]{\Error{#1}_\mathrm{max}}
\newcommand{\PartialMaxError}[2]{
  \abs{\PartialDerivative{#1}{#2}}_\Mean{#2}\,\MaxError{#2}}

% functions
\newcommand{\ExpFunPow}[1]{e ^ {#1}}
\newcommand{\ExpFun}[1]{\FunctionOf{exp}{#1}}
\newcommand{\StefanBoltzmann}[2]{#1 \, \sigma \, #2 ^ 4}

% default variables for units
\newcommand{\Time}{t}
\newcommand{\TimeConstant}{\tau}
\newcommand{\Pressure}{p}
\newcommand{\AirPressure}{\Pressure}
\newcommand{\Temperature}{T}
\newcommand{\Volume}{V}
\newcommand{\Diameter}{d}
\newcommand{\Height}{h}
\newcommand{\Mass}{m}
\newcommand{\WindVelocity}{v}
\newcommand{\Albedo}{\alpha}
\newcommand{\Emissivity}{\varepsilon}
\newcommand{\GasConstant}{R}
\newcommand{\GasConstantDryAir}{\GasConstant_\mathrm{d}}
\newcommand{\HeatCapacity}{C}
\newcommand{\HeatCapacitySpecific}{c}



% Variable makros
\newcommand{\ModuleTemperatureMeasured}{\Temperature_\mathrm{m}}
\newcommand{\ModuleTemperatureReal}{\Temperature_\mathrm{i}}
\newcommand{\ModuleTemperatureCalibrationFunction}{f_\mathrm{c}}
\newcommand{\ModuleTemperatureRealParameterised}{
  \ModuleTemperatureMeasured}
\newcommand{\ModuleDiameter}{\Diameter_\mathrm{m}}
\newcommand{\ModuleHeight}{\Height_\mathrm{m}}
\newcommand{\ModuleVolume}{\Volume_\mathrm{m}}
\newcommand{\ModuleDimensionScale}{r}
\newcommand{\ModuleDiameterScaled}{\ModuleDimensionScale \, \ModuleDiameter}
\newcommand{\ModuleHeightScaled}{\ModuleDimensionScale \, \ModuleHeight}
\newcommand{\ModuleMass}{\Mass_\mathrm{m}}
\newcommand{\ModuleEffectiveSpecificHeatCapacity}
  {\HeatCapacitySpecific_\mathrm{eff}}
\newcommand{\ModuleEffectiveHeatCapacity}{\HeatCapacity_\mathrm{m}}
\newcommand{\ModuleEffectiveHeatCapacityProduct}{
    \ModuleEffectiveSpecificHeatCapacity \, \ModuleMass }

\newcommand{\AirTemperature}{\Temperature_\mathrm{a}}
\newcommand{\RoomTemperature}{\Temperature_\mathrm{r}}
\newcommand{\AirSpecificHumidity}{q_\mathrm{a}}
\newcommand{\AirRelativeHumidity}{RH}
\newcommand{\AirSaturationSpecifiyHumidity}{q ^ *}
\newcommand{\AirSaturationSpecifiyHumidityAtSurfaceTemperature}{
    \FunctionOf{\AirSaturationSpecifiyHumidity}{\SurfaceTemperature}}
\newcommand{\AirDensity}{\rho_\mathrm{a}}
\newcommand{\AirWindVelocity}{\WindVelocity}

\newcommand{\GroundTemperature}{\Temperature_\mathrm{g}}
\newcommand{\SurfaceTemperature}{\Temperature_\mathrm{s}}
\newcommand{\SurfaceSpecificHumidity}{q_\mathrm{s}}
\newcommand{\SurfaceLWEmissivity}{\Emissivity_\mathrm{s}}
\newcommand{\SurfaceSWAlbedo}{\alpha_\mathrm{s}}
\newcommand{\SurfaceExposedArea}{A_\mathrm{s}}
\newcommand{\SurfaceReceivingArea}{A_\mathrm{r}}
\newcommand{\SurfaceWetAreaFraction}{a_\mathrm{wet}}
\newcommand{\SurfaceLWin}{LW_\mathrm{in}}
\newcommand{\SurfaceLWinWeight}{w_\mathrm{lw}}
\newcommand{\SurfaceLWout}{LW_\mathrm{out}}
\newcommand{\SurfaceTotalLWout}{I_\mathrm{lw_\mathrm{out}}}
\newcommand{\SurfaceTotalLWin}{I_\mathrm{lw_\mathrm{in}}}
\newcommand{\SurfaceTotalSkyLWin}{I_\mathrm{lw,sky}}
\newcommand{\SurfaceTotalSWin}{I_\mathrm{sw}}
\newcommand{\SurfaceGlobalSWin}{I_\mathrm{sw,global}}
\newcommand{\SurfaceDirectSWin}{I_\mathrm{sw,direct}}
\newcommand{\SurfaceDiffuseSWin}{I_\mathrm{sw,diff}}
\newcommand{\SurfaceSWin}{SW_\mathrm{in}}
\newcommand{\SurfaceWetAreaCoverage}{a_\mathrm{wet}}
\newcommand{\SurfaceSensibleHeatFlux}{H_\mathrm{S}}
\newcommand{\SurfaceMoistureFlux}{F_\mathrm{w}}
\newcommand{\SurfaceLatentHeatFlux}{H_\mathrm{L}}
\newcommand{\SurfaceEffectiveHeatCapacity}{C_\mathrm{s}}

\newcommand{\RadiationTemperatureDevIndex}{rad}
\newcommand{\RadiationTemperatureDevEmissivity}
  {\Emissivity_\mathrm{\RadiationTemperatureDevIndex}}
\newcommand{\SurfaceBlackBodyTemperature}{\Temperature_\mathrm{b}}
\newcommand{\SurfaceMeasuredRadiationTemperature}
  {\Temperature_\mathrm{\RadiationTemperatureDevIndex}}

\newcommand{\HeatConductionInwards}{\Delta H_\mathrm{I}}
\newcommand{\HeatConductionInwardsPerArea}{
    \frac{\HeatConductionInwards}{\SurfaceExposedArea}}

% physical constants
\newcommand{\AirCp}{c_\mathrm{p}}
\newcommand{\WaterSpecificEnthalpyOfVaporization}{L}
\newcommand{\ModuleTemperatureRealTimeConstant}{\TimeConstant_\mathrm{m}}

% equation parameters
\newcommand{\HeatFluxCalmParameter}{\gamma}
\newcommand{\InternalHeatConductionParameter}{\lambda}
\newcommand{\HeatFluxVentilationParameter}{\eta}


% term makros
\newcommand{\EBModuleTemperatureRealChange}{
    \ModuleEffectiveHeatCapacity \,
    \frac{d \ModuleTemperatureReal}{dt}
    }
\newcommand{\EBModuleTemperatureRealChangePerArea}{
    \frac{\ModuleEffectiveHeatCapacity}{\SurfaceExposedArea} \,
    \frac{d \ModuleTemperatureReal}{dt}
    }
\newcommand{\EBModuleTemperatureRealChangePerAreaScaled}{
    \ModuleDimensionScale \,
    \frac{\ModuleEffectiveHeatCapacity}{\SurfaceExposedArea} \,
    \frac{d \ModuleTemperatureReal}{dt}
    }
\newcommand{\SurfaceTemperatureChange}{
    \frac{d \SurfaceTemperature}{dt}
    }
\newcommand{\EBSurfaceTemperatureChange}{
    \SurfaceEffectiveHeatCapacity \,
    \frac{d \SurfaceTemperature}{dt}
    }
\newcommand{\EBSurfaceTemperatureChangePerArea}{
    \frac{\SurfaceEffectiveHeatCapacity}{\SurfaceExposedArea} \,
    \frac{d \SurfaceTemperature}{dt}
    }
\newcommand{\EBSurfaceTemperatureChangePerAreaScaled}{
    \ModuleDimensionScale \,
    \frac{\SurfaceEffectiveHeatCapacity}{\SurfaceExposedArea} \,
    \frac{d \SurfaceTemperature}{dt}
    }
\newcommand{\EBSurfaceLongWaveOutTotal}{
    \SurfaceLWEmissivity \, \sigma \, \SurfaceExposedArea \,
    \SurfaceTemperature ^ 4
    }
\newcommand{\EBSurfaceLongWaveOutPerArea}{
    \SurfaceLWEmissivity \, \sigma \, \SurfaceTemperature ^ 4
    }
\newcommand{\EBSurfaceLongWaveInPerArea}{
    \SurfaceLWEmissivity \, \InSquareBraces{
      \InRoundBraces{1-\SurfaceLWinWeight} \, \sigma \, \GroundTemperature^4 +
        \SurfaceLWinWeight \, \SurfaceTotalSkyLWin }
    }
\newcommand{\EBSurfaceLongWaveInGeneralPerArea}{
    \SurfaceLWEmissivity \, \SurfaceTotalLWin
    }
\newcommand{\EBSurfaceLongWaveInGeneralTotal}{
    \EBSurfaceLongWaveInGeneralPerArea \, \SurfaceExposedArea
    }
\newcommand{\EBSurfaceLongWaveInTotal}{
    \EBSurfaceLongWaveInPerArea \, \SurfaceExposedArea
    }
\newcommand{\EBSurfaceAbsorbedShortWaveFraction}{
    1 - \SurfaceSWAlbedo
    }
\newcommand{\SurfaceReceivingAreaParameterised}{
  \ModuleDiameter \, \ModuleHeight
  }
\newcommand{\SurfaceReceivingAreaParameterisedScaled}{
  \ModuleDimensionScale ^ 2 \, \ModuleDiameter \, \ModuleHeight
  }
\newcommand{\ModuleVolumeParametrised}{
  \ModuleHeight \, \SurfaceTopArea
  }
\newcommand{\ModuleVolumeParametrisedScaled}{
  \ModuleDimensionScale ^ 3 \, \ModuleVolumeParametrised
  }
\newcommand{\SurfaceMantleArea}{
  \pi \, \ModuleDiameter \, \ModuleHeight
  }
\newcommand{\SurfaceMantleAreaScaled}{
  \pi \, \ModuleDimensionScale ^ 2 \, \ModuleDiameter \, \ModuleHeight
  }
\newcommand{\SurfaceTopArea}{
  \pi \, \Squared{\InRoundBraces{\frac{\ModuleDiameter}{2}}}
  }
\newcommand{\SurfaceTopAreaScaled}{
  \pi \, \ModuleDimensionScale ^ 2 \,
    \Squared{\InRoundBraces{\frac{\ModuleDiameter}{2}}}
  }
\newcommand{\SurfaceExposedAreaParameterised}{
  \SurfaceTopArea + \SurfaceMantleArea
  }
\newcommand{\SurfaceExposedAreaParameterisedScaled}{
  \SurfaceTopAreaScaled + \SurfaceMantleAreaScaled
  }
\newcommand{\EBSurfaceShortWaveInTotal}{
    \InRoundBraces{\EBSurfaceAbsorbedShortWaveFraction} \,
    \SurfaceReceivingArea \, \SurfaceTotalSWin
    }
\newcommand{\EBSurfaceShortWaveInPerArea}{
    \InRoundBraces{\EBSurfaceAbsorbedShortWaveFraction} \,
      \frac{\SurfaceReceivingArea}{\SurfaceExposedArea}
      \SurfaceTotalSWin
    }
\newcommand{\EBSurfaceShortWaveInPerAreaNegative}{
    \InRoundBraces{\EBSurfaceAbsorbedShortWaveFraction} \, \SurfaceTotalSWin
    }
\newcommand{\EBSurfaceLongWaveTotalBudgetPerArea}{
    \sigma \SurfaceTemperature ^ 4 - \SurfaceTotalSkyLWin
    }

% heat flux
\newcommand{\EBSurfaceHeatFluxParameterPart}{
    \HeatFluxVentilationParameter \, \AirWindVelocity
    }
\newcommand{\EBSurfaceHeatFluxParameterPartWithCalm}{
    \InRoundBraces{
      \HeatFluxCalmParameter +
      \HeatFluxVentilationParameter \, \AirWindVelocity}
    }
% latent heat flux
\newcommand{\EBSurfaceMoistureFluxPerArea}{
    \EBSurfaceHeatFluxParameterPart \,
        \InRoundBraces{ \SurfaceSpecificHumidity - \AirSpecificHumidity }
    }
\newcommand{\EBSurfaceMoistureFluxTotal}{
    \EBSurfaceMoistureFluxPerArea \,
    \SurfaceExposedArea
    }
\newcommand{\EBSurfaceLatentHeatFluxNoParameterPartNoWetAreaCoverage}{
    \AirDensity \,
    \WaterSpecificEnthalpyOfVaporization
        \InRoundBraces{ \SurfaceSpecificHumidity - \AirSpecificHumidity }
    \SurfaceExposedArea
    }
\newcommand{\EBSurfaceLatentHeatFluxPerAreaNoParameterPartNoWetAreaCoverage}{
    \AirDensity \,
    \WaterSpecificEnthalpyOfVaporization
        \InRoundBraces{ \SurfaceSpecificHumidity - \AirSpecificHumidity }
    }
\newcommand{\EBSurfaceLatentHeatFluxPerAreaNoParameterPart}{
    \SurfaceWetAreaFraction \,
    \EBSurfaceLatentHeatFluxPerAreaNoParameterPartNoWetAreaCoverage
    }
\newcommand{\EBSurfaceLatentHeatFluxPerAreaSaturationNoParameterPart}{
    \SurfaceWetAreaFraction \, \AirDensity \,
    \WaterSpecificEnthalpyOfVaporization
        \InRoundBraces{ \AirSaturationSpecifiyHumidityAtSurfaceTemperature -
        \AirSpecificHumidity }
    }
\newcommand{\EBSurfaceLatentHeatFluxPerArea}{
    \EBSurfaceLatentHeatFluxPerAreaNoParameterPart \,
    \EBSurfaceHeatFluxParameterPart
    }
\newcommand{\EBSurfaceLatentHeatFluxSaturationPerArea}{
    \EBSurfaceLatentHeatFluxPerAreaSaturationNoParameterPart \,
    \EBSurfaceHeatFluxParameterPart
    }
\newcommand{\EBSurfaceLatentHeatFluxNoSaturationTotal}{
    \EBSurfaceLatentHeatFluxNoParameterPartNoWetAreaCoverage \,
    \EBSurfaceHeatFluxParameterPart
    }
\newcommand{\EBSurfaceLatentHeatFluxTotal}{
    \EBSurfaceLatentHeatFluxNoParameterPart \, \SurfaceExposedArea \,
    \EBSurfaceHeatFluxParameterPart
    }
\newcommand{\EBSurfaceLatentHeatFluxSaturationTotal}{
    \EBSurfaceLatentHeatFluxPerAreaSaturationNoParameterPart \,
    \SurfaceExposedArea \, \EBSurfaceHeatFluxParameterPart
    }
\newcommand{\Calm}[1]{
  \renewcommand{\EBSurfaceHeatFluxParameterPart}
    {\EBSurfaceHeatFluxParameterPartWithCalm} #1
}
% sensible heat flux
\newcommand{\EBSurfaceSensibleHeatFluxPerAreaNoParameterPart}{
    \AirDensity \, \AirCp \InRoundBraces{\SurfaceTemperature - \AirTemperature }
    }
\newcommand{\EBSurfaceSensibleHeatFluxPerArea}{
    \EBSurfaceSensibleHeatFluxPerAreaNoParameterPart \,
    \EBSurfaceHeatFluxParameterPart
    }
\newcommand{\EBSurfaceSensibleHeatFluxTotal}{
    \EBSurfaceSensibleHeatFluxPerAreaNoParameterPart \, \SurfaceExposedArea \,
    \EBSurfaceHeatFluxParameterPart
    }

% inward energy flux
\newcommand{\EBHeatConductionInwardsTotal}{
    \InternalHeatConductionParameter \,
    \frac{\SurfaceExposedArea}{\ModuleDiameter} \,
    \InRoundBraces{ \SurfaceTemperature -
    \ModuleTemperatureReal }
    }
\newcommand{\EBHeatConductionInwardsPerArea}{
    \frac{\InternalHeatConductionParameter}{\ModuleDiameter} \,
    \InRoundBraces{ \SurfaceTemperature -
    \ModuleTemperatureReal }
    }


%%%%%%%%%%%%%%%%%
%%% Scenarios %%%
%%%%%%%%%%%%%%%%%

\newcommand{\ScenarioDarkCondition}{\SurfaceTotalSWin = 0}
\newcommand{\ScenarioDryCondition}{\SurfaceWetAreaFraction = 0}
\newcommand{\ScenarioCalmCondition}{\AirWindVelocity = 0}
\newcommand{\ScenarioVentilatedCondition}{\AirWindVelocity > 0}
\newcommand{\ScenarioStationaryConditionTemperaturesEqual}{
    \SurfaceTemperature = \ModuleTemperatureReal}
\newcommand{\ScenarioStationaryConditionTemperaturesConstant}{
    \frac{d \SurfaceTemperature}{dt} =
    \frac{d \ModuleTemperatureReal}{dt} =
    0
    }
\newcommand{\ScenarioFullyWetCondition}{
    \SurfaceWetAreaFraction = 1
    }


%%%%%%%%%%%%%%%%%
%%% Equations %%%
%%%%%%%%%%%%%%%%%
\DeclareTranslationFallback{short_wave_in}{short-wave in}
\DeclareTranslationFallback{long_wave_in}{long-wave in}
\DeclareTranslationFallback{long_wave_out}{long-wave out}
\newcommand{\EBEnergyBalanceSurfaceGeneral}[1]{
    \underbracetext{ \EBSurfaceTemperatureChange }
               { \text{ \GetTranslation{thermal_inertia} } }
    = % equals
    \ifstrequal{#1}{split}{&}
    ~ \underbracetext{ - \, \SurfaceLWout } % long-wave out
                     { \text{ \GetTranslation{long_wave_out} } }
    ~ \underbracetext{ + \, \SurfaceLWin } % long-wave in
                     { \text{ \GetTranslation{long_wave_in} } }
    ~ \underbracetext{ + \, \SurfaceSWin } % sh-wave in
                     { \text{ \GetTranslation{short_wave_in} } }%
    \ifstrequal{#1}{split}{\\&}
    ~ \underbracetext{ + \SurfaceSensibleHeatFlux }
                     { \text{ \GetTranslation{sensible_heat_flux} } }
    ~ \underbracetext{ + \SurfaceLatentHeatFlux }
                     { \text{ \GetTranslation{latent_heat_flux} } }
    ~ \underbracetext{ - \HeatConductionInwards}
               { \text{ \GetTranslation{internal_heat_conduction} } }
}

\newcommand{\EBEnergyBalanceInternalGeneral}{
\underbracetext{ \EBModuleTemperatureRealChangePerArea }
               {  \text{ \GetTranslation{thermal_inertia} } }
 = % equals
~ \underbracetext{ \HeatConductionInwards}
               { \text{ \GetTranslation{internal_heat_conduction} } }
}

\newcommand{\EBEnergyBalanceSurfaceParameterised}[1]{
\underbracetext{ \EBSurfaceTemperatureChangePerArea }
               {  \text{ \GetTranslation{thermal_inertia} } }
 =
\ifstrequal{#1}{split}{&}
~ \underbracetext{ - \, \EBSurfaceLongWaveOutPerArea } % long-wave out
                 { \text{ \GetTranslation{long_wave_out} } }
~ \underbracetext{ + \, \EBSurfaceLongWaveInPerArea } % long-wave in
                 { \text{ \GetTranslation{long_wave_in} } }
~ \underbracetext{ + \, \EBSurfaceShortWaveInPerArea } % sh-wave in
                 { \text{ \GetTranslation{short_wave_in} } }
\ifstrequal{#1}{split}{\\&}
~ \underbracetext{ - \EBSurfaceSensibleHeatFluxPerArea }
                 { \text{ \GetTranslation{sensible_heat_flux} } }
~ \underbracetext{ - \EBSurfaceLatentHeatFluxSaturationPerArea }
                 { \text{ \GetTranslation{latent_heat_flux} } }
~ \underbracetext{ - \EBHeatConductionInwardsPerArea}
               { \text{ \GetTranslation{internal_heat_conduction} } }
}


\DeclareTranslationFallback{tendency}{tendency}
\DeclareTranslationFallback{temperature}{temperature}
\DeclareTranslationFallback{surface_energy_budget}{surface energy budget}
\newcommand{\EBSurfaceHeatCapacityRegressionFormula}{
  \SurfaceEffectiveHeatCapacity \,
  \underbracetext
    {\DiscreteDerivative{\SurfaceTemperature}{\Time}}
    {\text{\ac{cover}}\\\text{\GetTranslation{temperature}}\\%
      \text{\GetTranslation{tendency}}}
  =
  \underbracetext
    {\SurfaceExposedArea \,
    \InSquareBraces{
      \SurfaceLWEmissivity \, \sigma \,
                 \InRoundBraces{\AirTemperature ^ 4 - \SurfaceTemperature ^ 4}
      ~ - \EBHeatConductionInwardsPerArea
      }}
     {\text{\GetTranslation{surface_energy_budget}}}
  }


\DeclareTranslationFallback{internal_heat_conduction}{int. heat conduction}
\newcommand{\EBSensorUnitHeatCapacityRegressionFormula}{
  \ModuleEffectiveHeatCapacity \,
  \underbracetext
    {\DiscreteDerivative{\ModuleTemperatureReal}{\Time}}
    {\text{\ac{sensor unit}}\\\text{\GetTranslation{temperature}}\\%
      \text{\GetTranslation{tendency}}}
  =
  \underbracetext
    {\EBHeatConductionInwardsTotal}
    {\text{\GetTranslation{internal_heat_conduction}}}
 }

\DeclareTranslationFallback{radiation_budget}{radiation budget}
\DeclareTranslationFallback{thermal_inertia}{thermal inertia}
\newcommand{\EBBulkTransferParameterRegressionFormula}{
\underbracetext{ \EBSurfaceTemperatureChangePerArea }
               {  \text{\acs*{cover}}\\\text{\GetTranslation{thermal_inertia}}}
~
\underbracetext{ - \SurfaceLWEmissivity \, \sigma \,
                  \InRoundBraces{\RoomTemperature ^ 4 -\SurfaceTemperature ^ 4}
                  }
                 { \text{\GetTranslation{radiation_budget}} }
~
\underbracetext{ + \EBHeatConductionInwardsPerArea }
               { \text{\GetTranslation{internal_heat_conduction}} }
 = % equals
~ \underbracetext{ - \EBSurfaceSensibleHeatFluxPerArea }
                 { \text{\GetTranslation{sensible_heat_flux}} }
 }

\newcommand{\EBEnergyBalanceInternalParameterised}{
\underbracetext{ \EBModuleTemperatureRealChangePerArea }
               {  \text{ \GetTranslation{thermal_inertia} } }
=
\underbracetext{ \EBHeatConductionInwardsPerArea }
               { \text{ \GetTranslation{internal_heat_conduction} } }
}

\newcommand{\EmissivityFormula}{
  \SurfaceLWEmissivity =
  \frac{\RadiationTemperatureDevEmissivity\,
    \SurfaceMeasuredRadiationTemperature^4 - \AirTemperature ^ 4}
    {\SurfaceTemperature ^ 4 - \AirTemperature ^ 4}
  }

\endinput
