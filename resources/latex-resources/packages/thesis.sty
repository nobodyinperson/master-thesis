\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{thesis}[2017/03/10 Content for my master thesis]

\RequirePackage{translations}
\RequirePackage{formulas}
\RequirePackage{subcaption}
\RequirePackage{etoolbox}
\RequirePackage{nth}
\RequirePackage{tabularx}
\RequirePackage{iflang}
\RequirePackage[
  printonlyused,
  % withpage
  ]{acronym}
\RequirePackage{tikz}
\usetikzlibrary{patterns,arrows,arrows.meta,positioning,calc}

\RequirePackage{array}
\newcolumntype{L}[1]
  {>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]
  {>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]
  {>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}%

\newcommand{\NameAlpha}{Alpha}
\newcommand{\NameBeta}{Beta}
\newcommand{\NameGamma}{Gamma}
\newcommand{\NameDelta}{Delta}
\newcommand{\NameEpsilon}{Epsilon}
\newcommand{\NameZeta}{Zeta}
\newcommand{\NameEta}{Eta}
\newcommand{\device}[1]{\textsc{#1}}
\newcommand{\DevAlpha}{\device{Alpha}}
\newcommand{\DevBeta}{\device{Beta}}
\newcommand{\DevGamma}{\device{Gamma}}
\newcommand{\DevDelta}{\device{Delta}}
\newcommand{\DevEpsilon}{\device{Epsilon}}
\newcommand{\DevZeta}{\device{Zeta}}
\newcommand{\DevEta}{\device{Eta}}

\newcommand{\Manufacturer}[1]{\textsc{#1}}

\newcommand{\Scenario}[1]{#1}

\providecommand{\stretchtable}[3]{{\def\arraystretch{#1}\tabcolsep=#2 #3}}

\newcommand{\HeatFluxParameterPrecision}{4}
\newcommand{\HeatConductionParameterPrecision}{3}

\DeclareTranslationFallback{forcing}{forcing}
\DeclareTranslationFallback{cover}{cover}
\DeclareTranslationFallback{parameter}{parameter}
\DeclareTranslationFallback{parameters}{parameters}
\DeclareTranslationFallback{outdoor_module}{outdoor module}
\DeclareTranslationFallback{indoor_module}{indoor module}
\DeclareTranslationFallback{sensorunit}{sensor unit}
\newcommand{\listofacronyms}{%
  \begin{acronym}[Wettermast HH]
    \acro{API}{Application Programming Interface}
    \acro{bottom plate}[bottom plate]{\acl*{outdoor module}'s plastic bottom
      plate covering the \acs*{sensor board} from below}
    \acro{cover}[\GetTranslation{cover}]{\acl*{outdoor module}'s
      aluminium shell including the plastic inlet}
    \acro{CWS}{citizen weather station}
    \acro{forcing}[\GetTranslation{forcing}]{environmental conditions
      actively influencing the \acl*{outdoor module}'s temperature reading}
    \acrodefplural{forcing}[\GetTranslation{forcing}]
      {environmental conditions actively
      influencing the \acl*{outdoor module}'s temperature reading}
    \acro{HMP}[HMP]{\Manufacturer{Vaisala} HMP155 temperature and humidity
      probe}
    \acro{KT19}[KT19]{\Manufacturer{Heitronics} KT19 radiation pyrometer}
    \acro{LBFGSB}[L-BFGS-B]{L-BFGS-B bound-constrained optimization algorithm}
    \acro{indoor module}[\GetTranslation{indoor_module}]
      {Netatmo \GetTranslation{indoor_module}}
    \acro{outdoor module}[\GetTranslation{outdoor_module}]
      {Netatmo \GetTranslation{outdoor_module}}
    \acro{parameter}[\GetTranslation{parameter}]
      {value defining the model's characteristics}
    \acrodefplural{parameter}[\GetTranslation{parameters}]
      {quantities defining the model's characteristics}
    \acro{RMSD}{root mean squared difference}
    \acro{RMSE}{root mean squared error}
    \acro{RK4}{Runge-Kutta-4th order numerical scheme}
    \acro{SHT20}[SHT20]{\Manufacturer{Sensirion} SHT20 temperature and humidity
      sensor}
    \acro{sensor board}[sensor board]{\acl*{outdoor module}'s circuitery board
      including the \acs*{SHT20} sensor and the communication components}
    \acro{sensor unit}[\GetTranslation{sensorunit}]
      {\acl*{outdoor module}'s plastic battery casing including the
      \acs*{sensor board}, the batteries and the bottom plate}
    \acro{UHI}[UHI]{urban heat island}
    \acro{Wettermast}[Wettermast HH]{Wettermast Hamburg mea\-sure\-ment site}
    \acro{Windmaster}[Windmaster]{\Manufacturer{Kaindl electronic} Windmaster
      2}
  \end{acronym}

  \acused{outdoor module}
  \acused{indoor module}
  \acused{Wettermast}
  \acused{Windmaster}
  \acused{bottom plate}
  \acused{sensor board}
  \acused{sensor unit}
  \acused{cover}
  \acused{forcing}
  \acused{parameter}
  \acused{SHT20}
  }

\DeclareTranslationFallback{input_guess}{input guess}
\DeclareTranslationFallback{improve_guess}{improve guess}
\DeclareTranslationFallback{optimum}{optimum}
\DeclareTranslationFallback{modelled_state}{modelled state}
\DeclareTranslationFallback{reference_state}{reference state}
\DeclareTranslationFallback{optimized_solution}{optimized solution}
\DeclareTranslationFallback{measurements}{measurements}
\DeclareTranslationFallback{example_given}{e.g.}
\DeclareTranslationFallback{compare}{compare}
\newcommand{\BoxLabel}[2]{\phantomsubcaption\label{#1}(\thesubfigure) #2}
\newcommand{\OptimizingPrinciple}{%
  \usetikzlibrary{arrows,arrows.meta}
  \usetikzlibrary{patterns}
  \tikzset{
    box/.style={fill=white, align=center,
      rounded corners},
    bigbox/.style={draw, box, inner sep=0.5em, outer sep=0.2em, },
    modelbox/.style={bigbox},
    obsbox/.style={bigbox},
    arrowtype/.style={-latex,thick},
    arrowlabel/.style={sloped},
    node distance=4em,
    }
  \begin{tikzpicture}
    \node[modelbox] (guess)
      {\BoxLabel{FIGURExOptimizationInputGuess}
        {\textbf{\GetTranslation{input_guess}}
          \\(\acsp*{parameter}/\acsp*{forcing})}};
    \node[modelbox,above right=of guess] (modelled state)
      {\BoxLabel{FIGURExOptimizationModeledState}{
        \textbf{\GetTranslation{modelled_state}}}};
    \node[obsbox,below right=of modelled state] (reference state)
      {\BoxLabel{FIGURExOptimizationReferenceState}{
        \textbf{\GetTranslation{reference_state}}\\
          (\GetTranslation{example_given}
          \GetTranslation{measurements})}};
    \node[modelbox,above right=0em and 3em of modelled state] (optimized guess)
      {\BoxLabel{FIGURExOptimizationOptimizedSolution}
        {\textbf{\GetTranslation{optimized_solution}}\\
          (\acsp*{parameter}/\acsp*{forcing})}};
    \path[arrowtype,bend left]
      (guess.north) edge
      node[above,arrowlabel] {\GetTranslation{forward_modelling}}
        (modelled state.west);
    \path[arrowtype,latex-latex,dotted]
      (modelled state.south east) edge
      node[above,arrowlabel] (comparison)
        {\GetTranslation{compare}} (reference state.north west);
    \path[arrowtype,bend left]
      (comparison.south) edge
      node[below,arrowlabel] {\GetTranslation{improve_guess}}
        (guess.east);
    \path[arrowtype,bend right]
      (comparison.north) edge
      node[below,arrowlabel] {\GetTranslation{optimum}}
        (optimized guess.south);
  \end{tikzpicture}
  }


\DeclareTranslationFallback{optimization}{optimization}
\DeclareTranslationFallback{inverse_modelling}{inverse modelling}
\DeclareTranslationFallback{forward_modelling}{forward modelling}
\DeclareTranslationFallback{inverse_modelling_by_optimization}
  {\GetTranslation{inverse_modelling} by
  \GetTranslation{optimization}}
\DeclareTranslationFallback{temperature}{temperature}
\DeclareTranslationFallback{real_temperature}
  {real \GetTranslation{temperature}}
\DeclareTranslationFallback{measured_temperature}
  {measured \GetTranslation{temperature}}
\newcommand{\InverseModellingPrinciple}{%
  \usetikzlibrary{arrows,arrows.meta}
  \tikzset{
    box/.style={draw, inner sep=1em, outer sep=0.2em, thick, rounded corners,
    font=\bf},
    arrowtype/.style={-latex,thick},
    }
  \begin{tikzpicture}[node distance=2em]
    \node[box,align=center] (forcing) {\acs*{forcing}~\&\\\acsp*{parameter}};
    \node[box,left=of forcing] (real values)
      { \GetTranslation{real_temperature} };
    \node[box,right=of forcing]
      (measured values)
      { \GetTranslation{measured_temperature} };
    \path[arrowtype,bend left=45]
      (real values.north) edge
      node[above] { \GetTranslation{forward_modelling} }
      (measured values.north);
    \path[arrowtype,latex-latex,bend left,out=90,in=135]
      (forcing.north)
      edge
      node[below=0.5em]  { \GetTranslation{optimization} }
      (measured values.north);
    \path[arrowtype,latex-latex,bend left,out=90,in=135]
      (forcing.south)
      edge
      node[above=0.5em]  { \GetTranslation{optimization} }
      (real values.south);
    \path[arrowtype,bend left=45]
      (measured values.south) edge
      node[below]
      { \GetTranslation{inverse_modelling_by_optimization} }
      (real values.south);
  \end{tikzpicture}
  }

\DeclareTranslationFallback{wind}{wind}
\DeclareTranslationFallback{long_wave_input}{long-wave input}
\DeclareTranslationFallback{long_wave_output}{long-wave output}
\DeclareTranslationFallback{short_wave_input}{short-wave input}
\DeclareTranslationFallback{latent_heat_flux}{latent heat flux}
\DeclareTranslationFallback{sensible_heat_flux}{sensible heat flux}
\DeclareTranslationFallback{insulated}{insulated}
\DeclareTranslationFallback{surface_temperature}{surface temperature}
\DeclareTranslationFallback{internal_heat_conduction}{internal heat conduction}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{calc}
\usetikzlibrary{shapes,patterns,positioning, arrows.meta}
\newcommand{\EnergyBalanceModelChart}{%
  \tikzset{
  rectangle/.style={
    draw=black,
    text centered,
    rounded corners,
    },
  line/.style={draw, -latex'},
  edge/.style={draw},
  every node/.style = {align=center,rounded corners},
  arrowtype/.style={-{Latex[width=3mm]}},
  heatfluxarrow/.style={thick,dashed,{Latex[width=2mm]}-{Latex[width=2mm]},
    draw=green!60!black},
  radiationarrow/.style={thick,decorate,arrowtype,draw=orange},
  lwradiationarrow/.style={radiationarrow,draw=red!80!black,decoration={snake,
      amplitude=2mm,segment length=15mm,post length=.5mm}},
  swradiationarrow/.style={radiationarrow,draw=blue,decoration={snake,
      amplitude=2.5mm,segment length=5mm,post length=3mm}},
  arrowtext/.style={sloped, anchor=center,above=3mm},
  modulepart/.style={rectangle,fill opacity=0.2,draw=black, ultra thick},
  windarrow/.style={arrowtype,dotted,ultra thick,draw=blue!50!black},
  }


  \begin{tikzpicture}

  \newcommand{\chartscale}{1}

  \newlength\coverwidth\pgfmathsetlength{\coverwidth}{\chartscale*4.5cm}%
  \newlength\coverheight\pgfmathsetlength{\coverheight}{\chartscale*8cm}%
  \newlength\windarrowvoffset\pgfmathsetlength{\windarrowvoffset}
    {\chartscale*1.4cm}%
  \newlength\toparrowvoffset\pgfmathsetlength{\toparrowvoffset}
    {0.1*\coverheight}
  \newlength\arrowxsize\pgfmathsetlength{\arrowxsize}{1.5*\coverwidth}%

  \node (cover) [modulepart,fill=black!10!white,
    minimum height=\coverheight,minimum width=\coverwidth] {};

  \path let \p1 = (cover.south west), \p2 = (cover.north east) in
    node (sensorunit) [modulepart, fill=black!20!white,
    minimum width=(\x2-\x1-\pgflinewidth)*0.5,
    minimum height=(\y2-\y1-\pgflinewidth)*0.5] at (0,0) {};

  \path let \p1 = (cover.south west), \p2 = (cover.north east) in
    node (bottom) [modulepart,
    minimum width=\x2-\x1-\pgflinewidth,minimum height=1cm,
    pattern=north east lines,below=0mm of cover.south] {};

  \node (sensorunit_t) at (sensorunit)
    {\GetTranslation{temperature}\\$\ModuleTemperatureReal$};

  \node [below left=5mm and 1cm of cover.west] (surface_t)
    {\GetTranslation{surface_temperature} $\SurfaceTemperature$};
  \draw [dotted,thick] (cover.195) -- (surface_t.east);

  \node [fill=white] (bottom_text) at (bottom) {\GetTranslation{insulated}};

  \node [fill=white] (cover_text) at
    ($(cover.95)!-0.5!(sensorunit.north)$) {\ac{cover}};
  \draw [dotted,thick] (cover_text.south) -- (cover.95);

  \node [below=2mm of sensorunit.north] (sensorunit_text) {\ac{sensor unit}};

  \draw let \p1=(cover.north) in
    [lwradiationarrow] (-\arrowxsize,\y1+\toparrowvoffset) --
    node (lw_in_start) [pos=0] {} node[arrowtext]
      {\GetTranslation{long_wave_in} $\SurfaceLWin$} (cover.135)
    node (lw_in_end) [pos=1] {};
  \draw let \p1=(lw_in_start),\p2=(lw_in_end),\p3=(cover.west) in
    [lwradiationarrow] (\p3) --
    node[arrowtext] {\GetTranslation{long_wave_out} $\SurfaceLWout$}
      (\x1,{\y1-(\y2-\y3)});

  \draw  let \p1=(cover.north) in [swradiationarrow]
    (\arrowxsize,\y1+\toparrowvoffset)  --
    node (sw_in_start) [pos=0] {}
    node[arrowtext] {\GetTranslation{short_wave_in} $\SurfaceSWin$}
      (cover.45)
    node (sw_in_end) [pos=1] {};


  \pgfmathtruncatemacro{\coveraspectratioangle}
    {atan2(\coverheight,\coverwidth)}
  \pgfmathtruncatemacro{\ihfanglestart}{0-\coveraspectratioangle}
  \pgfmathtruncatemacro{\ihfangleend}{180+\coveraspectratioangle}
  \pgfmathtruncatemacro{\ihfangles}{13}
  \pgfmathtruncatemacro{\ihfanglenext}
    {\ihfanglestart+(\ihfangleend-\ihfanglestart)/(\ihfangles-1)}
  \foreach \angle in {\ihfanglestart,\ihfanglenext, ..., \ihfangleend} {
    \draw [heatfluxarrow] (sensorunit.\angle) -- (cover.\angle);
    }
  \node [fill=white,align=center] (ihf_text) at ($(cover.north)!0.5!(sensorunit.north)$)
    {\GetTranslation{internal_heat_conduction}\\$\HeatConductionInwards$};

  \foreach \angle in {-4,-2,...,4} {
    \path let \p1=(cover.south west),\p2=(cover.south east) in
      node (wind_start_\angle) at
        ({\x1-0.5*(\x2-\x1)},\y1+\windarrowvoffset-2*\angle mm) {};
    \path let \p1=(cover.south west),\p2=(cover.south east) in
      node (wind_end_\angle) at
        ({\x2+0.5*(\x2-\x1)},\y2+\windarrowvoffset-2*\angle mm) {};
    \draw (wind_start_\angle) edge [windarrow,bend right=\angle]
      (wind_end_\angle);
  }
  \node (wind_text) [right=0mm of wind_end_0]
    {\GetTranslation{wind} $\WindVelocity$};

  \draw let \p1=(cover.east),\p2=(sw_in_start),\p3=(sw_in_end) in
    [heatfluxarrow,ultra thick] (cover.east) -- node[arrowtext]
      {\GetTranslation{sensible_heat_flux} $\SurfaceSensibleHeatFlux$}
      (\x2,{\y2-(\y3-\y1)})
    node (lhf_end) [pos=1] {};
  \draw let \p1=(cover.east),\p2=(sw_in_start),\p3=(sw_in_end),\p4=(lhf_end) in
    [heatfluxarrow,ultra thick] (cover.-45) -- node[arrowtext]
      {\GetTranslation{latent_heat_flux} $\SurfaceLatentHeatFlux$}
      (\x2,{\y4-(\y3-\y1)});

  \end{tikzpicture}
  }

\DeclareTranslationFallback{symbol}{symbol}
\DeclareTranslationFallback{unit}{unit}
\DeclareTranslationFallback{description}{description}
\providecommand{\thead}[1]{\textbf{\titlecap{#1}}}
\newcolumntype{T}{R{1.8cm} C{2cm} X}
\newcommand{\symboltableheader}
  {\thead{\GetTranslation{symbol}} & \thead{\GetTranslation{unit}} &
    \thead{\GetTranslation{description}}}

\newcommand{\EnergyBalanceSymbolTableVariables}[1]{
  \begin{tabularx}{\textwidth}{T}
    \ifstrequal{#1}{header}{\symboltableheader \\\hline}{}
    $\SurfaceTemperature$ & \si{\kelvin} &
         (surface) temperature of the \ac{cover} \\
    $\ModuleTemperatureReal$ & \si{\kelvin} &
        real temperature of the \ac{sensor unit} \\
  \end{tabularx}
  }

\DeclareTranslationFallback{diameter}{diameter}
\DeclareTranslationFallback{height}{height}
\DeclareTranslationFallback{heat_capacity}{heat capacity}
\DeclareTranslationFallback{outdoor_module_diameter}
  {Netatmo \GetTranslation{outdoor_module} diameter}
\DeclareTranslationFallback{outdoor_module_height}
  {Netatmo \GetTranslation{outdoor_module} height}
\DeclareTranslationFallback{effective_sensorunit_heat_capacity}
  {effective \GetTranslation{sensorunit} \GetTranslation{heat_capacity}}
\DeclareTranslationFallback{effective_cover_heat_capacity}
  {effective \GetTranslation{cover} \GetTranslation{heat_capacity}}
\DeclareTranslationFallback{cover_long_wave_emissivity}
  {\GetTranslation{cover} long-wave emissivity}
\DeclareTranslationFallback{cover_short_wave_albedo}
  {\GetTranslation{cover} short-wave albedo}
\DeclareTranslationFallback{sky_and_ground_long_wave_radiation_weight}
  {share of sky and ground long-wave radiation}
\DeclareTranslationFallback{bulk_transfer_coefficient_heat_moisture}
  {bulk transfer parameter for heat and moisture}
\DeclareTranslationFallback{effective_internal_conductivity}
  {effective internal conductivity}
\newcommand{\EnergyBalanceSymbolTableParameters}[1]{
  \begin{tabularx}{\textwidth}{T}
    \ifstrequal{#1}{header}{\symboltableheader \\\hline}{}
    $\ModuleDiameter$ & \si{\metre} &
        \GetTranslation{outdoor_module_diameter} \\
    $\ModuleHeight$ & \si{\metre} &
        \GetTranslation{outdoor_module_height} \\
    $\SurfaceLWinWeight$ & \si{1} &
        \GetTranslation{sky_and_ground_long_wave_radiation_weight} \\
    $\InternalHeatConductionParameter$ & \si{\watt\per\kelvin\per\metre} &
        \GetTranslation{effective_internal_conductivity} \\
    $\SurfaceSWAlbedo$ & \si{1} &
        \GetTranslation{cover_short_wave_albedo} \\
    $\SurfaceLWEmissivity$ & \si{1} &
        \GetTranslation{cover_long_wave_emissivity} \\
    $\SurfaceEffectiveHeatCapacity$ &
        \si{\joule\per\kelvin} &
        \GetTranslation{effective_cover_heat_capacity} \\
    $\ModuleEffectiveHeatCapacity$ &
        \si{\joule\per\kelvin} &
        \GetTranslation{effective_sensorunit_heat_capacity} \\
    $\HeatFluxVentilationParameter$ & \si{1} &
        \GetTranslation{bulk_transfer_coefficient_heat_moisture} \\
  \end{tabularx}
  }


\newcommand{\EnergyBalanceSymbolTableForcing}[1]{
  \begin{tabularx}{\textwidth}{T}
    \ifstrequal{#1}{header}{\symboltableheader \\\hline}{}
    $\SurfaceWetAreaFraction$ & \si{1} &
        wet area fraction on the \acs*{cover} \\
    $\AirTemperature$ & \si{\kelvin} &
        surrounding air temperature \\
    $\GroundTemperature$ & \si{\kelvin} &
        ground surface temperature \\
    $\AirDensity$ &
        \si{\kilogram\per\metre\cubed} &
        surrounding air density \\
    $\AirSpecificHumidity$ &
        \si{\kilogram\per\kilogram} &
        specific humidity of surrounding air \\
    $\WindVelocity$ &
        \si{\metre\per\second} &
        wind velocity \\
    $\SurfaceTotalSkyLWin$ &
        \si{\watt\per\meter\squared} &
        total incoming sky long-wave radiation \\
    $\SurfaceTotalSWin$ &
        \si{\watt\per\meter\squared} &
        total incoming short-wave radiation \\
  \end{tabularx}
  }

\newcommand{\EnergyBalanceSymbolTableConstants}[1]{
  \begin{tabularx}{\textwidth}{T}
    \ifstrequal{#1}{header}{\symboltableheader \\\hline}{}
    $\WaterSpecificEnthalpyOfVaporization$ &
        \si{\joule\per\kilogram} &
        enthalpy of vaporization of water \\
    $\AirCp$ &
        \si{\joule\per\kilogram\per\kelvin} &
        specific heat capacity of dry air under constant pressure \\
    \end{tabularx}
  }

\newcommand{\EnergyBalanceSymbolTableOther}[1]{
  \begin{tabularx}{\textwidth}{T}
    \ifstrequal{#1}{header}{\symboltableheader \\\hline}{}
    $\SurfaceTotalLWin$ &
        \si{\watt\per\meter\squared} &
        effective incoming long-wave radiation \\
    $\SurfaceSpecificHumidity$ &
        \si{\kilogram\per\kilogram} &
        specific humidity in immediate proximity of the \acs*{cover} \\
    $\AirSaturationSpecifiyHumidity$ &
        \si{\kilogram\per\kilogram} &
        saturation specific humidity \\
    $\SurfaceExposedArea$ & \si{\metre\squared} &
        exposed surface area of \acl*{outdoor module} \\
    $\SurfaceReceivingArea$ & \si{\metre\squared} &
        radiation receiving surface area of \acl*{outdoor module} \\
    $\ModuleTemperatureMeasured$ & \si{\kelvin} &
        \acl*{outdoor module}'s measured temperature\\
    \end{tabularx}
  }

\newcommand{\EnergyBalanceSymbolTable}{%
  \sisetup{per-mode = symbol}
  \captionsetup[sub]{skip=1em}
  \newcommand{\tablesep}{\vspace{1em}}

  \stretchtable{1.2}{8pt}{
  \begin{subtable}{\textwidth}
    \caption{state variables}
    \label{TABLExEnergyBalanceVariables}
    \EnergyBalanceSymbolTableVariables{header}
  \end{subtable}
  \begin{subtable}{\textwidth}
    \tablesep
    \caption{\acp{parameter}}
    \label{TABLExEnergyBalanceParameters}
    \EnergyBalanceSymbolTableParameters{}
  \end{subtable}
  \begin{subtable}{\textwidth}
    \tablesep
    \caption{\ac{forcing}}
    \label{TABLExEnergyBalanceForcing}
    \EnergyBalanceSymbolTableForcing{}
  \end{subtable}
  \begin{subtable}{\textwidth}
    \tablesep
    \caption{physical constants}
    \EnergyBalanceSymbolTableConstants{}
    \label{TABLExEnergyBalanceConstants}
  \end{subtable}
  \begin{subtable}{\textwidth}
    \tablesep
    \caption{other}
    \label{TABLExEnergyBalanceOther}
    \EnergyBalanceSymbolTableOther{}
  \end{subtable}
  }
  }

\newcommand{\ModelAssumptionLine}[2]{%
    \phantomsubcaption\label{#1}\thesubtable & #2 \\}
\newcommand{\ModelAssumptionsTable}{%
  \renewcommand{\thesubtable}{\Roman{subtable}}
  \stretchtable{1.5}{8pt}{
  \begin{tabularx}{\textwidth}{c X}
    \thead{Nr.} & \thead{Assumption} \\\hline
    \ModelAssumptionLine
      {TABLExModelAssumptionsTwoParts}
      {The \acl*{outdoor module} consists of {two heat reservoirs}:
        the \ac*{cover} which {fully encloses} the \ac*{sensor unit}.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsInternalOnlyHeatConduction}
      {The \ac*{cover} and the \ac*{sensor unit} are connected solely by
        {heat conduction}.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsSurfaceFluxes}
      {The \ac*{cover}'s surface is under the influence of incoming short- and
      long-wave radiation as well as sensible and latent heat flux. It emits
      long-wave radiation itself.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsZeroDimensional}
      {The \ac*{cover} and the \ac*{sensor unit} each have no spatial
      temperature differences, i.e. they are both effectively treated as
      zero-dimensional.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsStanding}
      {The \acl*{outdoor module} is {standing} on the ground,
         i.e. the \ac{bottom plate} has no air contact from below.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsNoBottomHeatFlux}
      {There is {no heat flux at the bottom} of the \acl*{outdoor
      module}, i.e. the \ac*{bottom plate} acts as an insulator.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsFirstOrderClosure}
      {{First order heat flux closure}: heat fluxes
        are proportional to the gradient\Language.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsHeatFluxesLinearlyOnWind}
      {Sensible and latent heat flux are {proportional to the wind
        velocity}.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsTransferCoeffientsEqual}
      {Bulk transfer coefficients of heat and moisture are equal.}
    \ModelAssumptionLine
      {TABLExModelAssumptionsNoFeedback}
      {There is {no feedback} to the environment. The device is
        reacting passively to the environmental conditions.}
  \end{tabularx}
  }
  }

\newcommand{\CalibrationResultsConstruingEquationsTable}{%
  \stretchtable{1.5}{5pt}{
    \begin{tabular}{c c c}
      \thead{parameter} & \thead{value} & \thead{uncertainty} \\\hline
      $\ModuleHeight$
        & \SI[round-mode=off]{ \CalibrationTotalHeightMm }{ \milli \metre}
        & $-$ \\
      $\ModuleDiameter$
        & \SI[round-mode=off ]{\CalibrationTotalDiameterMm }{\milli \metre}
        & $-$ \\
      $\SurfaceSWAlbedo$
        & \SI{\CalibrationAluminiumAlbedo}{}
        & $-$  \\
      $\SurfaceLWEmissivity$
        & \SI{\CalibrationAluminiumEmissivity}{}
        & \SI[round-precision=0]{\pm
          \CalibrationAluminiumEmissivityRelErrorPercent}{\percent} \\
      $\InternalHeatConductionParameter$
        & \SI[round-precision=\HeatConductionParameterPrecision]{
        \CalibrationHeatConductionParameter}{\watt\per\metre\per\kelvin} & $-$
        \\
      $\SurfaceEffectiveHeatCapacity$
        & \SI{\CalibrationSurfaceHeatCapacity}{\joule\per\kelvin}
        & \SI[round-precision=0]{\pm
          \CalibrationSurfaceHeatCapacityRelStdPercent}{\percent} \\
      $\ModuleEffectiveHeatCapacity$
        & \SI{\CalibrationModuleHeatCapacity}{\joule\per\kelvin}
        & \SI[round-precision=0]{\pm
          \CalibrationModuleHeatCapacityRelStdPercent}{\percent} \\
      $\HeatFluxVentilationParameter$
        & \SI[round-precision=\HeatFluxParameterPrecision]
          {\CalibrationHeatFluxParameter}{}
        & \SI[round-precision=0]{\pm
          \CalibrationHeatFluxParameterStdPercent}{\percent} \\
      \end{tabular}
    }
}

\DeclareTranslationFallback{value}{value}
\newcommand{\CalibrationResultsAfterOptimizationTable}{%
  \stretchtable{1.5}{5pt}{
    \begin{tabular}{c c}
      \thead{\GetTranslation{parameter}}
        & \thead{\GetTranslation{value}}\\\hline
      $\ModuleHeight$
        & \SI[round-mode=off]{ \CalibrationTotalHeightMm }{ \milli \metre}
        \\
      $\ModuleDiameter$
        & \SI[round-mode=off ]{\CalibrationTotalDiameterMm }{\milli \metre}
        \\
      $\SurfaceSWAlbedo$
        & \SI{\CalibrationAluminiumAlbedo}{}
        \\
      $\SurfaceLWEmissivity$
        & \SI{\CalibrationAluminiumEmissivity}{}
        \\
      $\InternalHeatConductionParameter$
        & \SI[round-precision=\HeatConductionParameterPrecision]
          {\CalibrationOptimizedHeatConductionParameter}
          {\watt\per\metre\per\kelvin}
        \\
      $\SurfaceEffectiveHeatCapacity$
        & \SI{\CalibrationOptimizedSurfaceHeatCapacity}
          {\joule\per\kelvin}
        \\
      $\ModuleEffectiveHeatCapacity$
        & \SI{\CalibrationOptimizedModuleHeatCapacity}{\joule\per\kelvin}
        \\
      $\HeatFluxVentilationParameter$
        & \SI[round-precision=\HeatFluxParameterPrecision]
          {\CalibrationOptimizedHeatFluxParameter}{}
        \\
      \end{tabular}
    }
}

\endinput
