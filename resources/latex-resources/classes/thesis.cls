\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}[2017/12/22 v 0.0.1 thesis class]

% etoolbox for options bools
\RequirePackage{etoolbox}

% pass given options to parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}

\newcommand{\DeclareToggleOption}[1]{
  \newtoggle{#1}
  \togglefalse{#1}
  \DeclareOption{#1}{\toggletrue{#1}}
}

% custom options
\DeclareToggleOption{editing-mode}
\DeclareToggleOption{sans-serif}
\DeclareToggleOption{titlepage-logos-top}

% process options
\ProcessOptions \relax

% load "parent" class
\LoadClass[% the default options
  a4paper,
  11pt,
]{book} % based on book class

\newcommand{\IfDraftElse}[2]{\iftoggle{editing-mode}{#1}{#2}}
\newcommand{\IfDraft}[1]{\IfDraftElse{#1}{}}
\newcommand{\NotDraft}[1]{\IfDraftElse{}{#1}}

%%%%%%%%%%%%%%%%
%%% Packages %%%
%%%%%%%%%%%%%%%%

%%%% Page margins %%%
\RequirePackage[
    inner=3cm,
    outer=2cm,
    top=2cm,
    bottom=1.5cm,
    includeheadfoot,
    headheight=14pt,
    ]{geometry}

%%% Be able to do line numbers
\RequirePackage[
  switch*, % line numbers on "outer" side
  % modulo, % only multiples of five
  mathlines, % line numbers for equations
  ]{lineno}

%%% Graphics %%%
\RequirePackage{graphicx} % be able to include graphics
\RequirePackage[export]{adjustbox} % be able to position things
\RequirePackage{float}    % keep floats where they are supposed to be
\RequirePackage[labelfont=bf,skip=6pt]{caption} %
\RequirePackage{calc}
\RequirePackage[super]{nth}
\RequirePackage{subcaption}
\RequirePackage{titlecaps}
\Addlcwords{are or of on for and etc by the}
\captionsetup{skip=1em}
\captionsetup[sub]{margin=1em,skip=0.5em,format=hang}
\addtolength{\floatsep}{2em} % space between floats
\addtolength{\intextsep}{2em} % space between inline floats and text
\addtolength{\textfloatsep}{2em} % space between top/bottom floats and text

%%% Maths %%%
\RequirePackage{amsmath}    % sophisticated math
\setlength{\jot}{1em} % more space between split math lines
\RequirePackage{mathtools}  % ?
\RequirePackage{siunitx}    % SI units

\RequirePackage{enumitem}   % prettier enumerations
\setlist[1]{itemsep=-5pt}
\RequirePackage[normalem]{ulem} % be able to cross out text

\RequirePackage{xcolor} % colors

\RequirePackage[
  bottom, % footnotes always on the bottom of the page
  stable, % footnote handling in section titles
  perpage, % start footnote numbers at 1 for every page
  multiple, % handle multiple footnotemarks intelligently
  ]{footmisc}

\RequirePackage{fancyhdr} % be able to do headers
\pagestyle{fancy} % all pages are 'fancy' by default
\RequirePackage{emptypage} % don't show anything on automatic empty pages

\setlength{\parindent}{0pt} % no paragraph indentation
\setlength{\parskip}{0.2em} % paragraph vertical space
\RequirePackage{setspace} % be able to set line spacing
\linespread{1.1} % set line spacing
\newcommand{\lb}{\smallbreak} % line break command

% serif-less font
\iftoggle{sans-serif}{\renewcommand{\familydefault}{\sfdefault}}{}

%%% Links and References %%%
\RequirePackage[
  % breaklinks, % break links
  ]{hyperref} % let links work
\hypersetup{pdfborder={0 0 0}} % no ugly boxes around links
\IfDraftElse{
    \hypersetup{
      colorlinks=true,
      linkcolor=[HTML]{AB0000},
      anchorcolor=[HTML]{767676},
      citecolor=[HTML]{2F8B00},
      filecolor=[HTML]{00ADC6},
      menucolor=[HTML]{AD0008},
      urlcolor=[HTML]{990092},
      }
  }{
    \hypersetup{colorlinks=false}
  }
\renewcommand{\chapterautorefname}{Chapter}
\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Subsection}

\def\studentid#1{\protected@xdef\@studentid{#1}}
\def\course#1{\protected@xdef\@course{#1}}
\def\institute#1{\protected@xdef\@institute{#1}}
\def\university#1{\protected@xdef\@university{#1}}
\def\thesistype#1{\protected@xdef\@thesistype{#1}}
\def\thesistitle#1{\protected@xdef\@thesistitle{#1}}
\def\thesissubtitle#1{\protected@xdef\@thesissubtitle{#1}}
\def\institutelogo#1{\protected@xdef\@institutelogo{#1}}
\def\universitylogo#1{\protected@xdef\@universitylogo{#1}}
\def\surveyorone#1{\protected@xdef\@surveyorone{#1}}
\def\surveyortwo#1{\protected@xdef\@surveyortwo{#1}}

\studentid{\@empty}
\course{\@empty}
\institute{\@empty}
\university{\@empty}
\thesistype{\@empty}
\thesistitle{\@empty}
\thesissubtitle{\@empty}
\institutelogo{\@empty}
\universitylogo{\@empty}
\surveyorone{\@empty}
\surveyortwo{\@empty}

\newcommand{\thestudentid}{\@studentid}
\newcommand{\thecourse}{\@course}
\newcommand{\theinstitute}{\@institute}
\newcommand{\theuniversity}{\@university}
\newcommand{\thethesistype}{\@thesistype}
\newcommand{\thethesistitle}{\@thesistitle}
\newcommand{\thethesissubtitle}{\@thesissubtitle}
\newcommand{\theinstitutelogo}{\@institutelogo}
\newcommand{\theuniversitylogo}{\@universitylogo}
\newcommand{\thesurveyorone}{\@surveyorone}
\newcommand{\thesurveyortwo}{\@surveyortwo}

\newcommand{\stretchtable}[3]{{\def\arraystretch{#1}\tabcolsep=#2 #3}}

%%% Titlepage %%%
\newcommand{\maketitlepagelogos}{
  \begin{minipage}[b]{0.49\textwidth}%
    \ifdefempty{\@universitylogo}{~}{%
      \includegraphics[
        width=0.8\textwidth,height=0.2\textwidth,keepaspectratio,left
        ]{\@universitylogo}%
      }%
  \end{minipage}%
  \hfil
  \begin{minipage}[b]{0.49\textwidth}%
    \ifdefempty{\@institutelogo}{~}{%
      \includegraphics[
        width=0.8\textwidth,height=0.2\textwidth,,keepaspectratio,right
        ]{\@institutelogo}%
      }%
  \end{minipage}%
  }
% class-specific titlepage
\newcommand{\titlepageinfoline}[2]{\large{#1}&\large{#2}\\}
\renewcommand{\maketitle}{
  \clearpage
  \begin{titlepage}%
    \iftoggle{titlepage-logos-top}{\maketitlepagelogos}{}%
    \vspace*{\fill}%
    \begin{center}%
      \ifdefempty{\@thesistype}{}{\LARGE{\@thesistype}\\[3em]}%
      \ifdefempty{\@thesistitle}{}{\huge{\textsc{\@thesistitle}}\\[1em]}%
      \ifdefempty{\@thesissubtitle}{}{\Large{\textsc{\@thesissubtitle}}\\[3em]}%
      \ifdefempty{\@author}{}{\Large{\@author}}%
      \ifstrempty{\@course\@studentid\@surveyorone\@surveyortwo}{}{%
        \\[0.5em]%
        \stretchtable{1.1}{5pt}{
          \begin{tabular}{r l}
            \ifdefempty{\@studentid}{}
              {\titlepageinfoline{\titlecap{id}}{\@studentid}}
            \ifdefempty{\@course}{}
              {\titlepageinfoline{\titlecap{degree programme}}{\@course}}
            \ifdefempty{\@surveyorone}{}
              {\titlepageinfoline{\nth{1} supervisor}{\@surveyorone}}
            \ifdefempty{\@surveyortwo}{}
              {\titlepageinfoline{\nth{2} supervisor}{\@surveyortwo}}
          \end{tabular}%
        }
        }\\[1em]
      \ifdefempty{\@institute}{}{\large{\@institute}\\[0.5em]}%
      \ifdefempty{\@university}{}{\large{\@university}\\[2em]}%
      \large{{\today}}\\
    \end{center}%
    \vspace*{\fill}%
    \iftoggle{titlepage-logos-top}{}{\maketitlepagelogos}%
  \end{titlepage}%
  \clearpage
}

%%% colors %%%
\definecolor{darkgreen}{HTML}{008000}
\definecolor{darkblue}{HTML}{000080}

%%% custom commands %%%
\newcommand{\DraftColorNote}[2]{\IfDraftElse{ \textit{[{\color{#1}#2}]} }{}}
\newcommand{\Todo}[1]{\DraftColorNote{orange}{#1}}
\newcommand{\Note}[1]{\DraftColorNote{darkgreen}{#1}}
\newcommand{\Language}{\DraftColorNote{darkblue}{language}}
\newcommand{\SourceNeeded}{\DraftColorNote{red}{source needed}}
\newcommand{\CheckInfo}{\DraftColorNote{red}{check info!}}

\newcommand{\code}[1]{\texttt{#1}}

% referenceing with capitalizing at beginning of sentence
\sfcode`\.=1001
\sfcode`\?=1001
\sfcode`\!=1001
\sfcode`\:=1001
\newcommand{\figref}[1]{\ifnum\spacefactor=1001 Figure\else figure\fi ~\ref{#1}}
\newcommand{\chpref}[1]{\ifnum\spacefactor=1001 Chapter\else chapter\fi ~\ref{#1}}
\newcommand{\secref}[1]{\ifnum\spacefactor=1001 Section\else section\fi ~\ref{#1}}

\newcommand{\parensubref}[1]{(\subref{#1})}

\newcommand{\thead}[1]{\textbf{\titlecap{#1}}}

% show 'DRAFT' note in topleft, when in draft mode
\IfDraft{\lhead{\texttt{{\color{red}DRAFT \today}}}}

% line numbers in draft mode
\IfDraft{% do linenumbers
  \linenumbers
  }{ % no linenumbers
  \nolinenumbers
  }

\endinput
