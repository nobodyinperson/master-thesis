#!/usr/bin/make -f
-include pre.mk

ifeq ($(TEX_FILE_PATTERN),)
TEX_FILE_PATTERN:=*.tex
endif
TEXFILES = $(shell find -type f -iname '$(TEX_FILE_PATTERN)')

PDFFILES = $(TEXFILES:tex=pdf)

TEMPLATES_FOLDER = templates
TEMPLATES = $(wildcard $(TEMPLATES_FOLDER)/*.sty)
CUSTOM_PACKAGES = $(wildcard *.sty)

# programs
PDF_OPENER = xdg-open

LATEX_RESOURCES_DIR = latex-resources
LATEX_RESOURCES = $(shell find ./$(LATEX_RESOURCES_DIR)/ -type f)
LATEX_RESOURCES_DIRS = $(shell find ./$(LATEX_RESOURCES_DIR)/ -type d)
SET_TEXINPUTS = TEXINPUTS="$(shell echo -n $(LATEX_RESOURCES_DIRS) | perl -pe 's/\s+/:/g'):$$TEXINPUTS" BIBINPUTS="$$TEXINPUTS"
LATEX_BIN = latexmk
LATEX_OPTIONS = -lualatex -halt-on-error -use-make
LATEX_CMD = $(SET_TEXINPUTS) $(LATEX_BIN)

NETATMO_SCHEMATIC_DIR = images/netatmo/schematic
NETATMO_SCHEMATIC_SVG = $(wildcard $(NETATMO_SCHEMATIC_DIR)/*.svg)

BW_SUFFIX = _bw

all: $(PDFFILES)

.PHONY: show
show: $(PDFFILES)
	for file in $^;do $(PDF_OPENER) $$file;done

.PHONY: interactive
interactive:
	$(LATEX_CMD) $(LATEX_OPTIONS) -pvc -quiet $(firstword $(TEXFILES))

$(NETATMO_SCHEMATIC_DIR): $(NETATMO_SCHEMATIC_SVG)
	cd $(NETATMO_SCHEMATIC_DIR) && $(MAKE)

%.pdf: %.tex $(LATEX_RESOURCES)
	$(LATEX_CMD) $(LATEX_OPTIONS) $<

%$(BW_SUFFIX).pdf: %.pdf
	gs \
		-sOutputFile='$@' \
		-dNOPAUSE -dBATCH -dSAFER \
		-sDEVICE=pdfwrite \
		-sColorConversionStrategy=Gray \
		-dProcessColorModel=/DeviceGray \
		-dCompatibilityLevel=1.4 \
		 $< < /dev/null

.PHONY: clean
clean:
	rm -rf $(PDFFILES)
	rm -rf *.out *.lo[tfg] *.blg *.toc *.aux *.run.xml *.bbl *.nav *.snm *.fls *.bcf

# all subdirs with Makefiles in it
SUBDIRS = $(shell find -L -mindepth 2 -iname 'Makefile' -exec dirname {} \;)
define subdir_rule
$(1)/%:
	cd $(1) && $(MAKE) $$*
endef
$(foreach subdir,$(patsubst ./%,%,$(SUBDIRS)),$(eval $(call subdir_rule,$(subdir))))

define subdir_make
cd $(1) && $(MAKE)

endef
.PHONY: subdirs
subdirs:
	$(foreach subdir,$(SUBDIRS),$(call subdir_make,$(subdir)))



-include post.mk
