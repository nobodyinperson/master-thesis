#!/usr/bin/make -f
GPD_SNAPSHOT_CSV = gpd.csv
GPD_SNAPSHOT_PNG = $(GPD_SNAPSHOT_CSV:.csv=.png)

all:

EVALUATION_DIR = evaluation

WEATHERMAP_PLOTTER = weathermap/scripts/plot-weathermap.py

$(GPD_SNAPSHOT_CSV):
	netatmo-getpublicdata \
		--lat_ne 53.7499 --lat_sw 53.3809 --lon_ne 10.3471 --lon_sw 9.7085 \
		--full -v \
		-o $@

WEATHERMAP_BGFILE = weathermap/media/openstreetmap.png
%.png : %.csv $(WEATHERMAP_BGFILE) | $(WEATHERMAP_PLOTTER)
	$(WEATHERMAP_PLOTTER) \
		-i $< \
		--bgfile $(WEATHERMAP_BGFILE) \
		-o $@

CROPPED_SUFFIX=_cropped
PDFCROP_MARGIN=0
PDF=pdf
PDFCROP=pdfcrop
# crop a pdf
%$(CROPPED_SUFFIX).$(PDF): %.$(PDF)
	$(PDFCROP) --margins $(PDFCROP_MARGIN) $< $@

.PHONY: cleancropped
cleancropped:
	rm -f *$(CROPPED_SUFFIX).$(PDF)

clean: cleancropped

RESULTS_DIR = results
JSON2TEX = ./json2tex.py
CALIBRATION_RESULTS_JSON_FILES = $(wildcard $(RESULTS_DIR)/*.json)
CALIBRATION_RESULTS_TEX_FILE = tmpcalibration_results.texincl

$(CALIBRATION_RESULTS_TEX_FILE): $(CALIBRATION_RESULTS_JSON_FILES) $(JSON2TEX)
	$(JSON2TEX) -i $(filter %.json,$^) --prefix calibration -o $@

$(PDFFILES) := $(filter-out $(CALIBRATION_RESULTS_TEX_FILE:.tex=.pdf),\
	$(PDFFILES))

clean: calibration_tex_clean

.PHONY: calibration_tex_clean
calibration_tex_clean:
	rm -f $(CALIBRATION_RESULTS_TEX_FILE)

# $(firstword $(PDFFILES)): $(GPD_SNAPSHOT_PNG)

