#!/usr/bin/make -f

GPD_SNAPSHOT_CSV = gpd.csv
GPD_SNAPSHOT_PNG = $(GPD_SNAPSHOT_CSV:.csv=.png)

all: $(GPD_SNAPSHOT_PNG)

EVALUATION_DIR = evaluation

.PHONY: evaluation
evaluation:
	cd $(EVALUATION_DIR) && $(MAKE)

dkg2017-slides.pdf: evaluation

WEATHERMAP_PLOTTER = weathermap/scripts/plot-weathermap.py

$(GPD_SNAPSHOT_CSV):
	netatmo-getpublicdata \
		--lat_ne 53.7499 --lat_sw 53.3809 --lon_ne 10.3471 --lon_sw 9.7085 \
		--full -v \
		-o $@

WEATHERMAP_BGFILE = weathermap/media/openstreetmap.png
%.png : %.csv $(WEATHERMAP_BGFILE) | $(WEATHERMAP_PLOTTER)
	$(WEATHERMAP_PLOTTER) \
		-i $< \
		--bgfile $(WEATHERMAP_BGFILE) \
		-o $@

$(firstword $(wildcard *.tex)): $(GPD_SNAPSHOT_PNG)
