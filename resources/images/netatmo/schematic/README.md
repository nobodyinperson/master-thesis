# Schematic netatmo drawing

## Makefile

To create several combinations of overlays, run

```bash
make
```

## Requirements

You will need

- Inkscape
- pdftk
- pdfcrop

On a Debian/Ubuntu-like system, run

```bash
sudo apt-get install inkscape
sudo apt-get install pdftk
sudo apt-get install texlive-extra-utils # for pdfcrop
```

