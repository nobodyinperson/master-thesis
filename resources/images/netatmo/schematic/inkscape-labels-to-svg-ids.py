#!/usr/bin/env python3
# system modules
import textwrap
import logging
import argparse
import sys

# external modules
from lxml import etree

# CMD-line arguments
parser = argparse.ArgumentParser( 
    description = textwrap.dedent("""
        Given an SVG file, replace all id attributes with the corresponding
        inkscape:label attribute, if present.

        Setting files to "-" means STDIN or STDOUT respectively.
        """)
    )
parser.add_argument("-i","--input", help = "input svg file",default = "-")
parser.add_argument("-o","--output", help = "output svg file",default = "-")
parser.add_argument("-v","--verbose", help = "verbose output",
    action = "store_true",default = False)
parser.add_argument("-l","--labels_file", 
    help = "save inkscape:labels to file", required = False)
args = parser.parse_args()

if args.verbose:
    loglevel = logging.DEBUG
else: 
    loglevel = logging.WARNING
logging.basicConfig( level = loglevel )

if args.input == "-":
    input_file = sys.stdin
else:
    input_file = open(args.input,"r")

# parse XML
doc = etree.parse(input_file)
# XML root
root = doc.getroot()

assert "inkscape" in root.nsmap, ("No inkscape namespace definition found. " 
    "File {} does not seem to be an SVG file created with Inkscape."
    ).format(args.input)
inkscape_label_attrib = "{{{}}}label".format(root.nsmap.get("inkscape"))

# find all 'g' tags
g_namespace = root.nsmap.get( "g", root.nsmap.get(None, "" ))
if g_namespace: g_namespace = "{{{}}}".format(g_namespace)
g_elements = root.findall( "{}g".format( g_namespace ) )
logging.debug( "{} <g>-tags found".format( len(g_elements) ) )

inkscape_labels = []
for g in g_elements:
    inkscape_label = g.attrib.get(inkscape_label_attrib)
    svg_id = g.attrib.get("id")
    logging.debug("svg id of this tag: {}".format(svg_id))
    if inkscape_label:
        inkscape_labels.append(inkscape_label)
        logging.debug("inkscape label: {}".format(inkscape_label))
        g.attrib["id"] = inkscape_label
        logging.debug("svg id of this tag changed from '{}' to '{}'".format(
            svg_id,inkscape_label))
    else:
        logging.debug("no inkscape label in this tag. Skipping this tag.")



if args.output == "-":
    output_file = sys.stdout
else:
    output_file = open(args.output,"w")

# write XML
xml_string = etree.tostring( doc, pretty_print = True, encoding = str )
output_file.write( xml_string )

if args.labels_file:
    if args.labels_file == "-":
        label_file = sys.stdout
    else:
        label_file = open(args.labels_file,"w")
    # write labels
    label_file.write( "\n".join(inkscape_labels) )

