DKG 2017 poster
===============

Compilation
+++++++++++

.. code:: bash 
    
    make

Requirements
++++++++++++

If you have system privileges on a Debian-like system:

.. code:: bash
    
    sudo apt-get install texlive texlive-luatex texlive-xetex texlive-science

Otherwise if it doesn't work, install `your own texlive distribution
<https://www.tug.org/texlive/build.html>`_. Compilation should then work
out-of-the-box.
