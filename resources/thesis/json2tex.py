#!/usr/bin/env python3
import json
import re
import argparse

parser = argparse.ArgumentParser(description = "Convert JSON to TEX")
parser.add_argument("-i","--input",help="input JSON file",nargs="+")
parser.add_argument("-o","--output",help="output TEX file",required=True)
parser.add_argument("-p","--prefix",help="prefix TEX commands with this",
    default="")

args = parser.parse_args()

def str2texmacroname(k,prefix=""):
    onlyletters = re.sub(pattern="[^a-zA-Z]+",repl=" ",string=k)
    withprefix = "{} {}".format(prefix, onlyletters)
    uppercased = withprefix.title()
    concatenated = re.sub(pattern="\s+",repl="",string=uppercased)
    return concatenated

def texmacro(name,content):
    return r"\newcommand{{\{n}}}{{{c}}}".format(n=name,c=content)

JSON = {}
for fn in args.input:
    with open(fn) as f:
        d = json.load(f)
        JSON.update(d)

JSON = {str2texmacroname(k,args.prefix):v for k,v in JSON.items()}

MACROS = [texmacro(k,v) for k,v in JSON.items()]

TEX = "\n".join(MACROS)

with open(args.output,"w") as f:
    f.write(TEX)
