RESULTS_DIR = results
JSON2TEX = ./json2tex.py
CALIBRATION_RESULTS_JSON_FILES = $(wildcard $(RESULTS_DIR)/*.json)
CALIBRATION_RESULTS_TEX_FILE = tmpcalibration_results.tex

$(CALIBRATION_RESULTS_TEX_FILE): $(CALIBRATION_RESULTS_JSON_FILES) $(JSON2TEX)
	$(JSON2TEX) -i $(filter %.json,$^) --prefix calibration -o $@

MSc-YB.pdf: $(CALIBRATION_RESULTS_TEX_FILE)

.PHONY: mono
mono: MSc-YB$(BW_SUFFIX).pdf

.PHONY: monoclean
monoclean:
	rm -f MSc-YB$(BW_SUFFIX).pdf

.PHONY: calibration_tex_clean
calibration_tex_clean:
	rm -f $(CALIBRATION_RESULTS_TEX_FILE)

clean: monoclean calibration_tex_clean

.PHONY: count
count: MSc-YB.pdf
	pdftotext $< - | tr -d '.' | wc -w

