#!/usr/bin/env python3
import argparse
import logging
import re
import json

from meteorology.temperature import cel2kel

import pandas as pd
import numpy as np

logging.basicConfig(level=logging.INFO)

def key_equals_val(arg):
    try:
        oldname,newname = arg.split("=")
        return (oldname,newname)
    except:
        raise argparse.ArgumentTypeError(
            "expected 'NAME=NEWNAME', got '{}'".format(arg))

parser = argparse.ArgumentParser(
    description="Convert Netatmo files for a numericalmodel")
parser.add_argument("-i","--input",help="input Netatmo file",
    required=True)
parser.add_argument("-o","--output",help="output numericalmodel CSV file",
    required=True)
parser.add_argument("--with_metadata",help="write metadata to file?",
    action = "store_true")

args = parser.parse_args()

fulltime_col = "FULLTIME"
seconds_col = "SECONDS"
time_col = "time"

logging.info("Reading data from '{}'...".format(args.input))
data = pd.read_csv( args.input, parse_dates={fulltime_col:["time"]})

logging.info("Calculate seconds...")
data[seconds_col] = \
    (data[fulltime_col] - data[fulltime_col].min()) / np.timedelta64(1,'s')

zero_date = data[fulltime_col].min()

logging.info("Dropping NAN...")
data = data.dropna(axis="columns", how="all").dropna(axis="index",how="any")

logging.info("Renaming...")
data = data.rename({seconds_col:time_col}, axis = "columns")

logging.info("Converting...")
data["Temperature"] = cel2kel(data["Temperature"])
data["Humidity"] = data["Humidity"] / 100

logging.info("setting '{}' as index...".format(time_col))
data.set_index(time_col, inplace = True)

logging.info("Dropping '{}'...".format(fulltime_col))
data = data.drop(fulltime_col,axis="columns")


logging.info("Writing output to '{}'".format(args.output))
with open(args.output, "w") as f:
    if args.with_metadata:
        metadata = {"zero_date":zero_date.isoformat()}
        logging.info("Writing metadata {} to file "
            "'{}'..".format(metadata,args.output))
        jsonblock = json.dumps(
            metadata,indent=4,sort_keys=True)
        commentblock = "\n".join(["# {}".format(l) \
            for l in jsonblock.split("\n")])
        f.write(commentblock + "\n")
    f.write(data.to_csv())

