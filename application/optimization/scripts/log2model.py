#!/usr/bin/env python3
import argparse
import logging
import re
import sys
import csv

import numpy as np
import pandas as pd

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(
    description="Convert model optimization log to model input")
parser.add_argument("-i","--input",help="model optimization log file. "
    "Defaults to STDIN.")
parser.add_argument("-o","--output",help="output numericalmodel CSV file. "
    "Defaults to STDOUT.")

args = parser.parse_args()

assignment_regex = re.compile(
    r"Setting\s+(\S+)\s+to\s+\[([^]]+)\][^[]+\[([^]]+)\]",flags=re.DOTALL)

def str2numberlist(string):
    return [float(s) for s in re.split(pattern=r"[^\w.-]+",string=string) if s]


f = open(args.input) if args.input else sys.stdin
logcontent = f.read()

assignments = {
    vid: {
        "times":str2numberlist(t),
        "values":str2numberlist(v)
        } for vid,v,t in assignment_regex.findall(logcontent) }

# check data
for k,data in assignments.items():
    try:
        t,v = data["times"], data["values"]
        assert len(t)==len(v), \
            "{}: {} times but {} values".format(k,len(t),len(v))
        np.testing.assert_almost_equal(times,t,
            err_msg="{}: times do not match previous times".format(k))
    except NameError:
        pass
    times = data["times"]

df_dict = {k:v["values"] for k,v in assignments.items()}
df_dict.update({"time":times})

df = pd.DataFrame(df_dict)
df.set_index("time", inplace = True)

df.to_csv(args.output if args.output else sys.stdout)
