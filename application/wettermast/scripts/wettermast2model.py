#!/usr/bin/env python3
import argparse
import logging
import re
import json

from meteorology.constants import zero_celsius_in_kelvin

import pandas as pd
import numpy as np

logging.basicConfig(level=logging.INFO)

def key_equals_val(arg):
    try:
        oldname,newname = arg.split("=")
        return (oldname,newname)
    except:
        raise argparse.ArgumentTypeError(
            "expected 'NAME=NEWNAME', got '{}'".format(arg))

def regex(arg):
    try:
        rex = re.compile(arg)
        assert rex.groups == 1, \
            "Regular expression does not capture exactly one group"
        return rex
    except BaseException as e:
        raise argparse.ArgumentTypeError(
            "Expected a regular expression that captures exactly one group, "
            "got '{}': {}".format(arg, e))

parser = argparse.ArgumentParser(
    description="Convert Wettermast Hamburg MASTER files for a numericalmodel")
parser.add_argument("-i","--input",help="input Wettermast MASTER file",
    required=True)
parser.add_argument("-o","--output",help="output numericalmodel CSV file",
    required=True)
parser.add_argument("--only",help="only what CSV columns to include "
    "(before translation)? Specify just this to use only the "
    "names from --translate",nargs="*", default=False,type=str)
parser.add_argument("--translate",help="translate column names",
    nargs="+",type=key_equals_val, default=[])
parser.add_argument("--kuerzel",help="kuerzel JSON file")
parser.add_argument("--regex",help="suffix to append to the names when "
    "searching the kuerzels", type=regex, default=r"^(.*)$")
parser.add_argument("--with_metadata",help="write metadata to file?",
    action = "store_true")
parser.add_argument("--rain_detection_col",
    help="rain detection colname. If this is specified, this column is "
	"appended a '_wet' suffix to indicate the wet area coverage")
parser.add_argument("--a_wet_max", help="maximum wet area coverage",
    type=float, default=0.1)

args = parser.parse_args()

fulltime_col = "FULLTIME"
seconds_col = "SECONDS"
time_col = "time"

logging.info("Reading data from '{}'...".format(args.input))
data = pd.read_csv(
    args.input,
    sep=";",
    skiprows=6, parse_dates={fulltime_col:["$Names=DATE","TIME"]},
    date_parser=lambda x: pd.to_datetime(x,format="%d.%m.%Y %H:%M"),
    )

logging.info("Calculate seconds...")
data[seconds_col] = \
    (data[fulltime_col] - data[fulltime_col].min()) / np.timedelta64(1,'s')

zero_date = data[fulltime_col].min()

if args.rain_detection_col:
    RD_col = args.rain_detection_col
    wet_col = RD_col+"_wet"
    logging.info("Converting rain to a_wet...")
    data[wet_col]=data[RD_col].rolling(4).sum()
    data[wet_col]=np.where(data[wet_col]>1,1,data[wet_col])
    data[wet_col]=data[wet_col].rolling(3,center=True).mean()
    data[wet_col]=np.where(
        data[wet_col]<data[RD_col],data[RD_col],data[wet_col])
    data[wet_col] = data[wet_col] * args.a_wet_max

translation = dict(args.translate)
only = args.only if hasattr(args.only,"__iter__") and args.only else \
    (list(translation.keys()) if args.translate and not args.only is False \
        else list(data.columns))
only.extend([seconds_col]) # keep seconds col



unwanted_cols = [col for col in data.columns if not col in only]
logging.info("Dropping unwanted columns {}...".format(unwanted_cols))
data.drop(unwanted_cols, axis = "columns", inplace = True)

kuerzel = {}
if args.kuerzel:
    with open(args.kuerzel) as f:
        logging.info("Reading kuerzels '{}'...".format(args.kuerzel))
        kuerzel.update(json.load(f))

unit2si = {
    "°C": lambda x: x + zero_celsius_in_kelvin,
    "%": lambda x: x / 100,
    "g/kg": lambda x: x / 1000,
    "g/m³": lambda x: x / 1000,
    "kPa": lambda x: x * 1000,
    "hPa": lambda x: x * 100,
    "kHz": lambda x: x * 1000,
    "kJ/m²": lambda x: x * 1000,
    "kWh/m²": lambda x: x * 1000 * 60 * 60,
    "h": lambda x: x * 60 * 60,
    "1/cm²": lambda x: x * 1e4,
    "1/min": lambda x: x * 60,
    "mm/h": lambda x: x / 1000 / (60*60),
    "mm/min": lambda x: x / 1000 / 60,
    "ms": lambda x: x / 1000,
    "mm": lambda x: x / 1000,
    "min": lambda x: x / 60,
    "mV": lambda x: x / 1000,
    "mA": lambda x: x / 1000,
    }

for col in data.columns:
    # apply regex
    m = args.regex.match(col)
    unitcol = m.group(1) if m else col
    # get unit
    unit = kuerzel.get(unitcol,{}).get("unit")
    converter = unit2si.get(unit)
    if converter:
        logging.info("Converting column {} from unit '{}'...".format(col,unit))
        data[col] = converter(data[col])

translation[seconds_col] = time_col
logging.info("Rename columns: {}...".format(translation))
data.rename(translation, axis = "columns", inplace = True)

logging.info("Dropping lines with NAN...")
data.dropna(axis = "index", how = "any", inplace = True)

logging.info("setting '{}' as index...".format(time_col))
data.set_index(time_col, inplace = True)

logging.info("Writing output to '{}'".format(args.output))
with open(args.output, "w") as f:
    if args.with_metadata:
        metadata = {"zero_date":zero_date.isoformat()}
        logging.info("Writing metadata {} to file "
            "'{}'..".format(metadata,args.output))
        jsonblock = json.dumps(
            metadata,indent=4,sort_keys=True)
        commentblock = "\n".join(["# {}".format(l) \
            for l in jsonblock.split("\n")])
        f.write(commentblock + "\n")
    f.write(data.to_csv())
