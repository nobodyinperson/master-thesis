#!/usr/bin/env python3
import argparse
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser(description="Shrink outdoor module")
parser.add_argument("-i","--input",help="input CSV files", required = True)
parser.add_argument("-o","--output",help="output CSV file", required = True)
parser.add_argument("-s","--shrink",help="shrink dimensions to what ratio",
    default = 0.5, type = float)
args = parser.parse_args()

def cylinder_volume(h,d):
    return np.pi * (d/2)**2 * h

params = pd.read_csv(args.input)

h_m, d_m = params["h_m"], params["d_m"]

current_volume = cylinder_volume(h = params["h_m"], d = params["d_m"])

h_m_s, d_m_s = h_m * args.shrink, d_m * args.shrink

shrinked_volume = cylinder_volume(h = h_m_s, d = d_m_s)

C_m, C_s = params["C_m"], params["C_s"]

volume_shrink_ratio = shrinked_volume / current_volume

C_m_s, C_s_s = C_m * volume_shrink_ratio, C_s * volume_shrink_ratio

shrinked_params = params.copy()

shrinked_params["C_m"] = C_m_s
shrinked_params["C_s"] = C_s_s
shrinked_params["d_m"] = d_m_s
shrinked_params["h_m"] = h_m_s

shrinked_params.to_csv(args.output)
