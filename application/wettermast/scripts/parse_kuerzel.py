#!/usr/bin/env python3
import json
import re
import sys
import argparse

parser = argparse.ArgumentParser(
    description="Parse Wettermast Kürzel file")
parser.add_argument("-i","--input",help="input Wettermast Kürzel file",
    required=True)
parser.add_argument("-o","--output",help="output JSON file",
    required=True)

args = parser.parse_args()

regex = re.compile(r"^[[(\s]*(?P<id>[A-Z0-9_]+)\s{2,}"
    r"(?:(?P<unit>\S+.*?)\s{3,})?(?P<desc>.*)$")

JSON = {}
with open(args.input) as f:
    for line in f:
        m = regex.match(line)
        if m:
            d = m.groupdict()
            JSON[d["id"]] = {k:v for k,v in d.items() if not k == "id"}

with open(args.output,"w") as f:
    json.dump(JSON,f,indent=4,sort_keys=True)
