
# Directories
BASE_DIR=.
SCRIPTS_DIR = scripts
RESULTS_DIR = results
DATA_DIR = $(BASE_DIR)/data
PLOTS_DIR = $(BASE_DIR)/plots
OUTPUT_DIR = $(BASE_DIR)/output

TEMP_DIRS = $(PLOTS_DIR) $(OUTPUT_DIR) $(BASE_DIR)

# Executables
JSON2CSV = $(SCRIPTS_DIR)/json2csv.py
MODEL_BIN = python3 -m netatmulator
WETTERMAST2MODEL = $(SCRIPTS_DIR)/wettermast2model.py
MODEL_PLOTTER = $(SCRIPTS_DIR)/plot_model_results.py
KUERZEL_PARSER = $(SCRIPTS_DIR)/parse_kuerzel.py
PARAM_SHRINKER = $(SCRIPTS_DIR)/shrink_module.py

MODEL_OUTPUT_KW = modeloutput
MODEL_INPUT_KW = modelinput
WETTERMAST_KW = wettermast
RAW_KW = raw
KUERZEL_KW = kuerzel
BACKUP_KW = backup
PLOT_KW = plot
FULL_KW = full
SCATTER_KW = scatter
HIST2D_KW = hist2d
TIMESERIES_KW = timeseries
AIRTEMP_KW = airtemp
MEAN_KW = mean
DETAILED_KW = detailed
GROUNDTEMP_KW = groundtemp
# separator
_=_

RESULT_FILES = $(wildcard $(RESULTS_DIR)/*.json)
CALIBRATED_PARAMS_FILE = $(DATA_DIR)/$(MODEL_INPUT_KW)$_calibrated.csv
SHRINKED_PARAMS_FILE = $(DATA_DIR)/$(MODEL_INPUT_KW)$_calibrated_shrinked.csv
WETTERMAST_KUERZEL_RAW_FILE = $(DATA_DIR)/$(WETTERMAST_KW)$_$(KUERZEL_KW)$_$(RAW_KW).txt
WETTERMAST_KUERZEL_FILE = $(DATA_DIR)/$(WETTERMAST_KW)$_$(KUERZEL_KW).txt
WETTERMAST_KUERZEL_JSON_FILE = $(WETTERMAST_KUERZEL_FILE:.txt=.json)
MATPLOTLIBRC = .matplotlibrc
MATPLOTLIBRC_DETAILED = .matplotlibrc-detailed
USE_MATPLOTLIBRC = MATPLOTLIBRC=$(MATPLOTLIBRC)

PLOT_LANGUAGE=en_GB
USE_PLOT_LANGUAGE=LANGUAGE=$(PLOT_LANGUAGE)

# SIMULATION_PARAMS_FILE = $(CALIBRATED_PARAMS_FILE)
SIMULATION_PARAMS_FILE = $(SHRINKED_PARAMS_FILE)

WETTERMAST_HEIGHTS=2
WETTERMAST_HEIGHTS_STR=$(foreach height,$(WETTERMAST_HEIGHTS),\
	$(shell printf '%03d' $(height)))

WETTERMAST_FILES = \
	$(filter-out $(WETTERMAST_KUERZEL_FILE) $(WETTERMAST_KUERZEL_RAW_FILE),\
	$(wildcard $(DATA_DIR)/$(WETTERMAST_KW)*.txt))

ifeq ($(MAKECMDGOALS),available-plots)
AVAILABLE_WETTERMAST_MODEL_OUTPUT_FILES = \
	$(wildcard $(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)*.csv)
WETTERMAST_FILES:= \
	$(foreach file,$(AVAILABLE_WETTERMAST_MODEL_OUTPUT_FILES),\
	$(shell echo '$(file)' | perl -ne 'print if s|$(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)$_(.*?)$_\d+\.\w+$$|$(DATA_DIR)/$$1.txt|g'))
else
endif

WETTERMAST_MODEL_OUTPUT_FILES = \
	$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),\
		$(addprefix $(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)$_,\
			$(notdir $(WETTERMAST_FILES:.txt=$_$(height_str).csv))))
ifeq ($(MAKECMDGOALS),available-plots)
WETTERMAST_MODEL_OUTPUT_FILES:=$(AVAILABLE_WETTERMAST_MODEL_OUTPUT_FILES)
else
endif

WETTERMAST_MODEL_INPUT_FILES = \
	$(addprefix $(DATA_DIR)/,\
		$(patsubst $(MODEL_OUTPUT_KW)%,$(MODEL_INPUT_KW)%,\
			$(notdir $(WETTERMAST_MODEL_OUTPUT_FILES))))
WETTERMAST_MODEL_PLOT_TIMESERIES_FILES = \
	$(addprefix $(PLOTS_DIR)/,\
		$(patsubst $(MODEL_OUTPUT_KW)%, \
			$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(TIMESERIES_KW)%,\
			$(notdir $(WETTERMAST_MODEL_OUTPUT_FILES:.csv=.pdf))))
WETTERMAST_MODEL_PLOT_SCATTER_FILES = \
	$(addprefix $(PLOTS_DIR)/,\
		$(patsubst $(MODEL_OUTPUT_KW)%, \
			$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(SCATTER_KW)%,\
			$(notdir $(WETTERMAST_MODEL_OUTPUT_FILES:.csv=.pdf))))
WETTERMAST_MODEL_PLOT_HIST2D_FILES += \
	$(addprefix $(PLOTS_DIR)/,\
		$(patsubst $(MODEL_OUTPUT_KW)%, \
			$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)%,\
			$(notdir $(WETTERMAST_MODEL_OUTPUT_FILES:.csv=.pdf))))
WETTERMAST_MODEL_PLOT_HIST2D_FILES += \
	$(addprefix $(PLOTS_DIR)/,\
		$(patsubst $(MODEL_OUTPUT_KW)%, \
			$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)%,\
			$(notdir $(WETTERMAST_MODEL_OUTPUT_FILES:.csv=.pdf))))
WETTERMAST_MODEL_PLOT_HIST2D_FILES += \
	$(addprefix $(PLOTS_DIR)/,\
		$(patsubst $(MODEL_OUTPUT_KW)%, \
			$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(AIRTEMP_KW)$_$(MEAN_KW)%,\
			$(notdir $(WETTERMAST_MODEL_OUTPUT_FILES:.csv=.pdf))))
WETTERMAST_MODEL_PLOT_FILES+=$(WETTERMAST_MODEL_PLOT_TIMESERIES_FILES)
# WETTERMAST_MODEL_PLOT_FILES+=$(WETTERMAST_MODEL_PLOT_SCATTER_FILES)
WETTERMAST_MODEL_PLOT_FILES+=$(WETTERMAST_MODEL_PLOT_HIST2D_FILES)


WETTERMAST_MODEL_PLOT_FULL_HIST2D_FILE = \
	$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),\
		$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(FULL_KW)$_$(height_str).pdf)
WETTERMAST_MODEL_PLOT_FULL_HIST2D_GROUNDTEMP_FILE = \
	$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),\
		$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(FULL_KW)$_$(height_str).pdf)
WETTERMAST_MODEL_PLOT_FULL_HIST2D_GROUNDTEMP_AIRTEMP_MEAN_FILE = \
	$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),\
		$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(AIRTEMP_KW)$_$(MEAN_KW)$_$(FULL_KW)$_$(height_str).pdf)
WETTERMAST_MODEL_PLOT_FULL_TIMESERIES_FILE = \
	$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),\
		$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(TIMESERIES_KW)$_$(FULL_KW)$_$(height_str).pdf)
WETTERMAST_MODEL_PLOT_FILES+= $(WETTERMAST_MODEL_PLOT_FULL_HIST2D_FILE)
WETTERMAST_MODEL_PLOT_FILES+= $(WETTERMAST_MODEL_PLOT_FULL_HIST2D_GROUNDTEMP_FILE)
WETTERMAST_MODEL_PLOT_FILES+= $(WETTERMAST_MODEL_PLOT_FULL_HIST2D_GROUNDTEMP_AIRTEMP_MEAN_FILE)
# WETTERMAST_MODEL_PLOT_FILES+= $(WETTERMAST_MODEL_PLOT_FULL_TIMESERIES_FILE)

vpath %.txt $(DATA_DIR)
vpath %.csv $(DATA_DIR) $(OUTPUT_DIR)
vpath %.py $(SCRIPTS_DIR)

.SECONDARY:

.PHONY: all
all: $(WETTERMAST_MODEL_INPUT_FILES)
all: $(WETTERMAST_MODEL_OUTPUT_FILES)
all: $(WETTERMAST_MODEL_PLOT_FILES)

$(WETTERMAST_KUERZEL_FILE): $(WETTERMAST_KUERZEL_RAW_FILE)
	iconv -f latin1 -t UTF-8 $< > $@

$(DATA_DIR)/$(WETTERMAST_KW)%.json: $(WETTERMAST_KW)%.txt
	$(KUERZEL_PARSER) \
		-i $< \
		-o $@

$(SHRINKED_PARAMS_FILE): $(CALIBRATED_PARAMS_FILE) $(PARAM_SHRINKER)
	$(PARAM_SHRINKER) \
		-i $< \
		-o $@ \
		--shrink 0.5

ifeq ($(MAKECMDGOALS),available-plots)
else
$(CALIBRATED_PARAMS_FILE): $(RESULT_FILES)
endif
$(CALIBRATED_PARAMS_FILE): | $(JSON2CSV)
	$(JSON2CSV) \
		-i $^ \
		-o $@ \
		--only \
		--translate \
			total_diameter=d_m \
			total_height=h_m \
			aluminium_albedo=alpha_s \
			aluminium_emissivity=eps_s \
			optimized-module-heat-capacity=C_m \
			optimized-surface-heat-capacity=C_s \
			optimized-heat-flux-parameter=eta \
			optimized-heat-conduction-parameter=lambda

define wettermast_height_modelinput_rule
$(DATA_DIR)/$(MODEL_INPUT_KW)$_$(WETTERMAST_KW)%$(2)$_$(1).csv: $\\
	$(WETTERMAST_KW)%$(2).txt $\\
	$(WETTERMAST_KUERZEL_JSON_FILE) $\\
	$(WETTERMAST2MODEL)
	$(WETTERMAST2MODEL) $\\
		-i $$< $\\
		-o $$@ $\\
		--only $\\
		--regex '^.*?([^_]+).*?$$$$' $\\
		--translate RH$(1)$(2)=RH FF$$(if $$(filter-out 002,$(1)),$(1),007)$(2)=v TT$(1)$(2)=T_a P$(1)$(2)=p L$(2)=LW_in G$(2)=SW_in RD$(2)_wet=a_wet TS000$(2)=T_g $\\
		--kuerzel $(WETTERMAST_KUERZEL_JSON_FILE) $\\
		--with_metadata $\\
		--rain_detection_col RD$(2)
endef
$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),\
	$(eval $(call wettermast_height_modelinput_rule,$(height_str),$_M10)))

$(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)%.csv: \
	$(MODEL_INPUT_KW)%.csv \
	$(SIMULATION_PARAMS_FILE) \
	| $(OUTPUT_DIR)
	$(MODEL_BIN) -v \
		'read:$(SIMULATION_PARAMS_FILE)[replace]' \
		'read:$(firstword $(filter %.csv,$^))[replace]' \
		'read:$(firstword $(filter %.csv,$^))[replace][only:T_a][T_a=T_m]' \
		'read:$(firstword $(filter %.csv,$^))[replace][only:T_a][T_a=T_s]' \
		'integrate' \
		'dump:$@[only:T_s,T_m]'

GROUNDTEMP_AIRTEMP_MEAN_HIST2D_PLOT_OPTIONS=\
{"scatter":true,\
"hist2d":true,\
"plot_method":"mean",\
"linear_fit":true,\
"time_as_date":true,\
"robust_linear_fit":true,\
"hist2d_bins":100,\
"hist2d_normed":true,\
"show_stats":true,\
"show_yaxis_stats":false,\
"identity_line":true}

AIRTEMP_HIST2D_PLOT_OPTIONS=\
{"scatter":true,\
"hist2d":true,\
"linear_fit":true,\
"robust_linear_fit":true,\
"hist2d_bins":100,\
"hist2d_normed":true,\
"time_as_date":true,\
"show_stats":true,\
"identity_line":true}

GROUNDTEMP_HIST2D_PLOT_OPTIONS=$(AIRTEMP_HIST2D_PLOT_OPTIONS)

TIMESERIES_PLOT_OPTIONS=\
{"scatter":false,\
"time_as_date":true,\
"split_units":true}

$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(TIMESERIES_KW)$_$(DETAILED_KW)%.pdf: \
	$(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)%.csv \
	$(DATA_DIR)/$(MODEL_INPUT_KW)%.csv \
	$(MATPLOTLIBRC_DETAILED) | $(PLOTS_DIR)
	$(USE_PLOT_LANGUAGE) MATPLOTLIBRC=$(MATPLOTLIBRC_DETAILED) $(MODEL_BIN) -v \
		'read:$(SIMULATION_PARAMS_FILE)[replace]' \
		'read:$(firstword $(filter $(DATA_DIR)/$(MODEL_INPUT_KW)%,$^))[replace]' \
		'read:$(firstword $(filter $(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)%,$^))[replace]' \
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(TIMESERIES_KW)$_$(DETAILED_KW)$*.pdf[only:T_s,T_m,T_g,T_a,v,SW_in,LW_in]$(TIMESERIES_PLOT_OPTIONS)'

$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(TIMESERIES_KW)%.pdf \
$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)%.pdf \
$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(AIRTEMP_KW)$_$(MEAN_KW)%.pdf \
$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)%.pdf: \
	$(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)%.csv \
	$(DATA_DIR)/$(MODEL_INPUT_KW)%.csv \
	$(MATPLOTLIBRC) | $(PLOTS_DIR)
	$(USE_PLOT_LANGUAGE) $(USE_MATPLOTLIBRC) $(MODEL_BIN) -v \
		'read:$(SIMULATION_PARAMS_FILE)[replace]' \
		'read:$(firstword $(filter $(DATA_DIR)/$(MODEL_INPUT_KW)%,$^))[replace]' \
		'read:$(firstword $(filter $(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)%,$^))[replace]' \
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(TIMESERIES_KW)$*.pdf[only:T_s,T_m,T_g,T_a]$(TIMESERIES_PLOT_OPTIONS)' \
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$*.pdf[only:T_m,T_g]$(GROUNDTEMP_HIST2D_PLOT_OPTIONS)' \
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(AIRTEMP_KW)$_$(MEAN_KW)$*.pdf[only:T_m,T_a,T_g]$(GROUNDTEMP_AIRTEMP_MEAN_HIST2D_PLOT_OPTIONS)' \
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$*.pdf[only:T_m,T_a]$(AIRTEMP_HIST2D_PLOT_OPTIONS)'

define model_read_file_cmd
	'read:$(1)' $\\

endef

define full_plot_rule
$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_%$_$(1).pdf $\\
$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(AIRTEMP_KW)$_$(MEAN_KW)$_%$_$(1).pdf $\\
$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_%$_$(1).pdf: $$\
	$(SIMULATION_PARAMS_FILE) $\\
	$(WETTERMAST_MODEL_INPUT_FILES) $\\
	$(WETTERMAST_MODEL_OUTPUT_FILES) $\\
	.%.txt $\\
	$(MATPLOTLIBRC) | $(PLOTS_DIR)
	$(USE_PLOT_LANGUAGE) $(USE_MATPLOTLIBRC) $(MODEL_BIN) -v $\\
		'cut[1:0]' LW_in_weight=0.5 gamma=0 $\\
		'read:$(SIMULATION_PARAMS_FILE)[replace]' $\\
	$$(foreach file,$$(sort $$(filter %$(1).csv,$$^)),$$(call model_read_file_cmd,$$(file))) $\\
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$$*$_$(1).pdf[only:T_m,T_a]$(AIRTEMP_HIST2D_PLOT_OPTIONS)' $\\
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$(AIRTEMP_KW)$_$(MEAN_KW)$_$$*$_$(1).pdf[only:T_m,T_a,T_g]$(GROUNDTEMP_AIRTEMP_MEAN_HIST2D_PLOT_OPTIONS)' $\\
		'plot:$(PLOTS_DIR)/$(MODEL_OUTPUT_KW)$_$(PLOT_KW)$_$(HIST2D_KW)$_$(GROUNDTEMP_KW)$_$$*$_$(1).pdf[only:T_m,T_g]$(GROUNDTEMP_HIST2D_PLOT_OPTIONS)'
endef
$(foreach height_str,$(WETTERMAST_HEIGHTS_STR),$(eval $(call full_plot_rule,$(height_str))))

.$(FULL_KW).txt:
	touch $@

.PHONY: available-plots
available-plots: $(WETTERMAST_MODEL_PLOT_FILES)

.PHONY: gui
gui: $(if $(FILE),$(FILE),$(firstword $(WETTERMAST_MODEL_OUTPUT_FILES)))
	$(MODEL_BIN) -v \
		'read:$(SIMULATION_PARAMS_FILE)[replace]' \
		'read:$(patsubst $(OUTPUT_DIR)/$(MODEL_OUTPUT_KW)%,$(DATA_DIR)/$(MODEL_INPUT_KW)%,$<)[replace]' \
		'read:$<[replace]' \
		$(MODEL_CMD) gui

BACKUP_FILE = $(shell date +$(BACKUP_KW)$_%F_%H%M%S.tar.gz)
.PHONY: backup
backup: $(BACKUP_FILE)

define one_per_line
	$(1) $\\

endef
$(BACKUP_KW)%.tar.gz: \
	$(SIMULATION_PARAMS_FILE) \
	$(WETTERMAST_MODEL_OUTPUT_FILES) \
	$(WETTERMAST_MODEL_PLOT_FILES) \
	$(WETTERMAST_MODEL_INPUT_FILES)
	tar -cvzf $@ \
	$(foreach file,$(sort $^),$(call one_per_line,$(file)))


$(TEMP_DIRS): % :
	mkdir -p $@

.PHONY: clean
clean: clean-plots
	rm -f $(WETTERMAST_KUERZEL_JSON_FILE)
	rm -f $(WETTERMAST_MODEL_INPUT_FILES)
	rm -f $(WETTERMAST_MODEL_OUTPUT_FILES)
	rm -f $(SIMULATION_PARAMS_FILE)
	rm -f $(CALIBRATED_PARAMS_FILE)
	rm -f $(WETTERMAST_KUERZEL_FILE)
	rm -f $(FULL_KW).txt

.PHONY: clean-plots
clean-plots:
	rm -f $(WETTERMAST_MODEL_PLOT_FILES)
