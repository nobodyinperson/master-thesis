library(matrixutils)
library(MASS)

rm(list=ls())
#### init plot ####
try(dev.off())
par(mfrow=c(4,1), mar=c(3,4,1,1))

#### data ####
SIZE = 55
KERNELSIZE = 11
ORIGCENTER = ceiling(SIZE/2)
KERNCENTER = ceiling(KERNELSIZE/2)
# original = c(0,0,0,0,0,0.3,0.5,1,1.1,1.2,1.4,0.9,0.7,0.1,0,0,0)
# kernel =     c(0,0.1,0.6,1,0.7,0.3,0.15,0.05,0,0,0,0,0,0,0,0,0)
# original = c(0,0,1,6,3,2,0,0,0)
original = 2*rnorm(SIZE)^2 - rnorm(SIZE)

plot(c(original)
     ,main="orignal timeseries"
     ,panel.first=rect(xleft = par("usr")[1], 
      xright=KERNELSIZE - 0.5, ybottom = par("usr")[3], ytop = par("usr")[4], 
      col="#ff000044",border=NA)
     ,ylim = range(original)
     ,type="b"
     )

kernel = rev(exp(-(1:KERNELSIZE-KERNELSIZE/5)^2/(KERNELSIZE)))
# kernel   = sort(kernel) + sort(kernel,decreasing = T)
# don't let the kernel look into the future
# kernel[ceiling(length(kernel)/3):length(kernel)]= 0
kernel   = kernel / sum(kernel) # norm the kernel

plot(x = -(length(kernel)-1):0 ,y=kernel
     ,main="lag kernel"
     ,ylim = c(0,max(kernel))
     )

#### convolve ####

filtered = convolve(x = original, y = kernel, type="open")
# filtered = zapsmall(filtered)
filtered = filtered[(KERNELSIZE):(length(filtered)-KERNELSIZE+1)]

# plot(c(rep(NA,KERNELSIZE-1),filtered)
#      ,main="lagged timeseries via convolve()"
#      ,ylim = range(original)
#      ,panel.first=rect(xleft = par("usr")[1], 
#       xright=KERNELSIZE - 0.5, ybottom = par("usr")[3], ytop = par("usr")[4], 
#       col="#ff000044",border=NA)
#      ,type="b"
#      )

# check that matfiltered is correct
stopifnot(length(filtered) == SIZE - KERNELSIZE + 1)
stopifnot(zapsmall(c(sum(kernel*original[1:KERNELSIZE]) - filtered[1],original))[1] == 0)
stopifnot(zapsmall(c(sum(kernel*original[(SIZE-KERNELSIZE+1):SIZE]) - filtered[length(filtered)],original))[1] == 0)

# create a filter matrix
SHIFTS <- (KERNCENTER - ORIGCENTER):(ORIGCENTER - KERNCENTER)
stopifnot(length(SHIFTS)==SIZE - KERNELSIZE + 1)
mat = matrix(0, nrow=length(SHIFTS), ncol = SIZE)
for(i in SHIFTS){ # loop over shifts
  half = floor(SIZE/2)
  bounds <- matrix.shift.bounds(
    dimbase = c(1,SIZE), dimshift = c(1,KERNELSIZE),  rowshift = 0, 
    colshift = i)
  mat[match(i,SHIFTS),bounds[7]:bounds[8]] <- kernel[bounds[3]:bounds[4]]
}
# cat("det(mat) = ",det(mat),"\n")

# check that the created matrix is correct
matfiltered <- c(mat%*%original)

plot(c(rep(NA,KERNELSIZE-1),matfiltered)
     ,main="lagged timeseries via matrix multiplication"
     ,ylim = range(original)
     ,panel.first=rect(xleft = par("usr")[1], 
      xright=KERNELSIZE - 0.5, ybottom = par("usr")[3], ytop = par("usr")[4], 
      col="#ff000044",border=NA)
     ,type="b"
     )
# check that matfiltered is correct
stopifnot(length(matfiltered) == SIZE - KERNELSIZE + 1)
stopifnot(zapsmall(c(sum(kernel*original[1:KERNELSIZE]) - matfiltered[1],original))[1] == 0)
stopifnot(zapsmall(c(sum(kernel*original[(SIZE-KERNELSIZE+1):SIZE]) - matfiltered[length(matfiltered)],original))[1] == 0)

cat("check if convolution by fft and matrix multiplicatio yields the same...")
stopifnot(all.equal(zapsmall(matfiltered),zapsmall(filtered)))
cat("done!\n")

#### do the deconvolution ####
# invert this matrix via generalized inverse
cat("calculate ginv...")
ginvmatinv <- ginv(mat)
ginvdeconvoluted <- c(ginvmatinv %*% filtered)
cat("done!\n")

plot(ginvdeconvoluted
     ,main="lagged timeseries deconvoluted via ginv"
     ,ylim = range(original)
     ,panel.first=rect(xleft = par("usr")[1], 
      xright=KERNELSIZE - 0.5, ybottom = par("usr")[3], ytop = par("usr")[4], 
      col="#ff000044",border=NA)
     ,type="b"
     )

# # invert this matrix via qr decomposition functions
# cat("calculate qr...")
# qrdecomposition <- qr(mat)
# qrdeconvoluted <- qr.coef(qrdecomposition, y = filtered)
# qrdeconvoluted[is.na(qrdeconvoluted)] <- 0
# cat("done!\n")
# 
# plot(qrdeconvoluted
#      ,main="lagged timeseries deconvoluted via qr"
#      ,ylim = range(original)
#      ,panel.first=rect(xleft = par("usr")[1], 
#       xright=KERNELSIZE - 0.5, ybottom = par("usr")[3], ytop = par("usr")[4], 
#       col="#ff000044",border=NA)
#      ,type="b"
#      )

rmse.filtered.original = sqrt(mean((filtered-original[KERNELSIZE:length(original)])^2))
cat(paste("rmse filtered to original:",rmse.filtered.original,"\n"))
rmse.deconvoluted.original.ginv = sqrt(mean((ginvdeconvoluted[KERNELSIZE:length(original)]-original[KERNELSIZE:length(original)])^2))
cat(paste("rmse deconvolution to original via ginv:",rmse.deconvoluted.original.ginv,"\n"))
cat(paste("rmse ratio ginv deconvoluted/lagged:",rmse.deconvoluted.original.ginv / rmse.filtered.original,"\n"))
# cat(paste("rmse deconvolution to original via qr:",  sqrt(mean((qrdeconvoluted  -original)^2)),"\n"))


#### Plot ####
