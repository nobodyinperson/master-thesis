#!/usr/bin/env python3
# system modules
import logging
import os
import datetime

# own modules
import crosfee
import netatmo

# external modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

##################
### Device IDs ###
##################
DEVICE_ID_ALPHA   = "70:ee:50:22:e6:ce"
DEVICE_ID_BETA    = "70:ee:50:20:de:62"
DEVICE_ID_GAMMA   = "70:ee:50:22:df:ae"
DEVICE_ID_DELTA   = "70:ee:50:22:e3:f8"
DEVICE_ID_EPSILON = "70:ee:50:22:d7:94"
DEVICE_ID_ZETA    = "70:ee:50:22:e5:40"
DEVICE_ID_ETA     = "70:ee:50:22:e5:6c"

### Module IDs ###
MODULE_ID_ALPHA   = "02:00:00:22:dd:d4"
MODULE_ID_BETA    = "02:00:00:20:da:36"
MODULE_ID_GAMMA   = "02:00:00:22:dc:38"
MODULE_ID_DELTA   = "02:00:00:22:e8:0a"
MODULE_ID_EPSILON = "02:00:00:22:a6:06"
MODULE_ID_ZETA    = "02:00:00:22:e5:c0"
MODULE_ID_ETA     = "02:00:00:22:e8:1a"


logging.basicConfig(level = logging.INFO)

# get a logger
logger = logging.getLogger(__name__)

# data folder
data_folder = os.path.realpath(
    os.path.join(os.path.dirname(__file__),
    '..','..','data',))
logger.debug("data folder: {}".format(data_folder))

# reference instrument data
reference_data_folder = os.path.realpath(
    os.path.join(data_folder,'calibration','001'))
logger.debug("reference data folder: {}".format(reference_data_folder))

# reference data file
reference_data_filename = "HMP155.csv"
logger.debug("reference data filename: {}".format(reference_data_filename))

# reference data file path
reference_data_filepath = os.path.join(
    reference_data_folder, reference_data_filename)
logger.debug("reference data filepath: {}".format(reference_data_filepath))

# read reference data
reference_data = pd.read_csv(reference_data_filepath,
    sep=";",usecols=["DATE","TIME","TIMEZONE","TT3","RH3"])

# create reference time index
reference_time_index = pd.DatetimeIndex( 
    data = pd.to_datetime( # create reference time series
        reference_data['DATE'] + " " + reference_data['TIME'], # input strings
        format = "%d.%m.%Y %H:%M:%S", # the format
        exact = False, # the format may match anywhere in the string
        utc = True, # Input and Output is UTC
        infer_datetime_format = True, # optimization
        ),
    dtype = 'datetime64[ns]' # index is of type datetime64[ns]
    )

# set index
reference_data.index = reference_time_index

logger.info("Reference data available from {min} to {max}".format( 
    min = reference_data.index.min(), max = reference_data.index.max(),))

# get according netatmo data
# create client
netatmo_client = netatmo.api.client.NetatmoClient()
netatmo_credentials_file = os.path.join(
    data_folder,"netatmo","CREDENTIALS.json")
netatmo_client.authentication.credentials = \
    netatmo.utils.read_json_from_file( netatmo_credentials_file )

netatmo_data_response = netatmo_client.Getmeasure( 
    device_id = DEVICE_ID_ETA,
    module_id = MODULE_ID_ETA,
    optimize = False,
    date_begin = (reference_data.index.min() - datetime.datetime.utcfromtimestamp(0)).total_seconds(),
    date_end =   (reference_data.index.max() - datetime.datetime.utcfromtimestamp(0)).total_seconds(),
    real_time = True,
    )
netatmo_data = netatmo_data_response.dataframe

logger.info("Netatmo data available from {min} to {max}".format( 
    min = netatmo_data.index.min(), max = netatmo_data.index.max(),))

# Optimizer

optimizer = crosfee.delag.optimizer.KernelOptimizer( 
    lagged = netatmo_data["Temperature"],
    original = reference_data["TT3"],
    # kernel = crosfee.delag.kernels.LagKernelWeibull(parameters=[1000,10]),
    kernel = crosfee.delag.kernels.LagKernelPolynomGeneral(parameters=[1,-1,0,-1,1,0,0,0,0]),
    )

fig = plt.figure()
plt.plot(optimizer.original_lags,
    optimizer.kernel(optimizer.original_lags),
    label="Kernel")
plt.title("Kernel first guess")
plt.legend()
plt.show()

logger.debug("start optimization...")
optimizer.optimize()
logger.debug("end of optimization")

# Plot
fig = plt.figure()
plt.plot(optimizer.original_lags,
    optimizer.kernel(optimizer.original_lags),
    label="Kernel")
plt.title("Kernel optimized")
plt.legend()

fig = plt.figure()
reference_convolved = optimizer.original_convolved
reference_convolved_at_netatmo = optimizer.original_convolved_at_lagged
plt.plot(reference_data.index,reference_data["TT3"],label="reference")
plt.plot(reference_convolved.index,reference_convolved,'-',
    label="reference convolved")
plt.plot(reference_convolved_at_netatmo.index,
    reference_convolved_at_netatmo,'.',label="reference convolved+interpolated")
plt.plot(netatmo_data.index,netatmo_data["Temperature"],label="netatmo")
plt.title("Convolution")
plt.legend()



plt.show()
