#!/usr/bin/env python3
# system module
import argparse
import textwrap
import logging
import json
import glob
import os
import re

# own modules
import numericalmodel
from numericalmodel.numericalmodel import NumericalModel
from numericalmodel.interfaces import *
from numericalmodel.numericalschemes import *
from numericalmodel.equations import *

import meteorology
from meteorology.temperature import cel2kel,kel2cel
from meteorology.humidity import saturation_water_vapour_pressure as e_magnus
from meteorology.constants import *

# external modules
import numpy as np
import pandas as pd

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser( 
    description = textwrap.dedent("""
        Simulate NETATMO and compare with measurements
        """)
    )

# Getmeasure arguments
parser.add_argument("--netatmo", help = "NETATMO measurement data")
parser.add_argument("-o","--output",help = "output plot file", required = True)
parser.add_argument("--show",help = "show the plot?", action = "store_true", 
    default = False)

# parse the arguments
args = parser.parse_args()

import matplotlib
if not args.show:
    matplotlib.use("pdf")
import matplotlib.pyplot as plt

# read calibration results
calibration = {}
RESULTS_DIR=os.path.join(os.path.dirname(__file__),
    "..","..","..","calibration","results")
calibration_files = glob.glob(os.path.join(RESULTS_DIR,"*.json"))
for f in calibration_files:
    with open(f) as fh:
        calibration.update(json.load(fh))

MEASUREMENT_FOLDER = os.path.join("data")

MEAS = {}
MEAS["kt19"] = pd.read_csv(os.path.join(MEASUREMENT_FOLDER,"kt19.csv"),
    sep=";",parse_dates=["time"],index_col="time")
MEAS["hmp"] = pd.read_csv(os.path.join(MEASUREMENT_FOLDER,"hmp.csv"),
    sep=";",parse_dates={"time":["DATE","TIME"]}, index_col="time")
MEAS["ptb"] = pd.read_csv(os.path.join(MEASUREMENT_FOLDER,"ptb.csv"),
    sep=";",parse_dates={"time":["DATE","TIME"]},
    index_col="time")
MEAS["wind"] = pd.read_csv(os.path.join(MEASUREMENT_FOLDER,"wind.csv"),
    parse_dates=["time"],index_col="time")
MEAS["ptb"]["density"] = MEAS["ptb"].B1 * 1e2 / gas_constant_dry_air / \
    cel2kel(MEAS["hmp"]["TT3"].reindex_like(MEAS["ptb"]))
netatmo_files = glob.glob(os.path.join(MEASUREMENT_FOLDER,"netatmo_*.csv"))
for f in [args.netatmo]:
    netatmo_name = re.sub(string=os.path.basename(f),
        pattern="^.*?_([A-Z]+).*$",repl="\g<1>")
    MEAS["netatmo"] = pd.read_csv(
        f,sep=",",parse_dates=["time"], index_col="time")

# starttime = max([min(x.index) for x in MEAS.values()])
starttime = MEAS["netatmo"].index.min()

for n,df in MEAS.items():
    df["time_seconds"] = (df.index - starttime).total_seconds()

for n,df in MEAS.items():
    if n=="wind":
        inside = df["time_seconds"]>=0
    else:
        inside = np.logical_and(
                df["time_seconds"]>=0,
                df["time_seconds"]<=MEAS["netatmo"]["time_seconds"].max())
    MEAS[n] = df[inside]

# the netatmomodel is a numerical model
netatmomodel = NumericalModel()

# Fill metadata
netatmomodel.name = "netatmo outdoor module energy balance model"
netatmomodel.version = "0.0.1-dev"
netatmomodel.description = """
zero-dimensional energy balance model of a netatmo outdoor module's temperature
""".strip()
netatmomodel.long_description = """
This model is developed as part of the master thesis of Yann Büchau at the
University of Hamburg.
""".strip()
netatmomodel.authors = { 
"theory": "BSc. Met. Yann Büchau & Prof. Dr. Felix Ament",
"idea": "Prof. Dr. Felix Ament",
"code": "BSc. Met. Yann Büchau",
}

# set initial time
# TIME_OFFSET = 1e11
netatmomodel.initial_time = 0

# State variables

module_temperature = StateVariable( 
    id = "T_m", name="measured temperature", unit = "K",
    bounds = [0,np.Inf],
    )
surface_temperature = StateVariable( 
    id = "T_s", name="surface temperature", unit = "K",
    bounds = [0,np.Inf],
    )

state_variables = SetOfStateVariables( 
        [
        module_temperature, 
        surface_temperature,
        ]
    )

# Parameters

surface_exposedarea = Parameter( 
    id = "A_s", name = "outdoor module's surface exposed area", unit = "m^2",
    bounds = [0,np.Inf],
    )
enthalpy_of_vaporization = Parameter( 
    id = "L", name = "enthalpy of vaporization of water", unit = "J/kg",
    bounds = [0,np.Inf],
    )
surface_emissivity = Parameter( 
    id = "eps_s", name = "long-wave outdoor module surface emissivity",
    bounds = [0,1],
    )
surface_albedo = Parameter( 
    id = "alpha_s", name = "short-wave outdoor module surface albedo",
    bounds = [0,1],
    )
module_heatcapacity = Parameter( 
    id = "C_m", name = "effective inner module heat capacity", unit = "J/kg",
    bounds = [0,np.Inf],
    )
surface_heatcapacity = Parameter( 
    id = "C_s", name = "effective outdoor module surface heat capacity",
    unit = "J/kg", bounds = [0,np.Inf],
    )
ventilated_hf_parameter = Parameter( 
    id = "eta", name = "ventilated heat flux parameter",
    )
heat_conduction_parameter = Parameter( 
    id = "lambda", name = "inward heat conduction parameter",
    )
module_diameter = Parameter( 
    id = "d_m", name = "total module diameter",
    )
module_height = Parameter( 
    id = "h_m", name = "total module height",
    )

parameters = SetOfParameters( 
        [
        surface_exposedarea,
        enthalpy_of_vaporization,
        surface_emissivity,
        surface_albedo,
        module_heatcapacity,
        surface_heatcapacity,
        ventilated_hf_parameter,
        heat_conduction_parameter,
        module_diameter,
        module_height,
        ]
    )


# forcing

air_density = ForcingValue( 
    id = "rho_a", name = "surrounding air density", unit = "kg/m^3",
    bounds = [0,np.Inf],
    )
air_spec_hum = ForcingValue( 
    id = "q_a", name = "surrounding air specifiy humidity", unit = "kg/kg",
    bounds = [0,np.Inf],
    )
air_temperature = ForcingValue( 
    id = "T_a", name = "surrounding air temperature", unit = "K",
    bounds = [0,np.Inf],
    )
wind = ForcingValue( 
    id = "v", name = "wind velocity", unit = "m/s",
    bounds = [0,np.Inf],
    )
wet_fraction = ForcingValue( 
    id = "a_wet", name = "fraction of wet area on outdoor module's " 
        "exposed surface", 
    bounds = [0,1],
    )
lw_in_total = ForcingValue( 
    id = "LW_in", name = "total long-wave incoming radiation", unit = "W/m^2",
    bounds = [0,np.Inf],
    )
sw_in_total = ForcingValue( 
    id = "SW_in", name = "total short-wave incoming radiation", unit = "W/m^2",
    bounds = [0,np.Inf],
    )
pressure = ForcingValue( 
    id = "p", name = "pressure", unit = "Pa",
    bounds = [0,np.Inf],
    )

forcing = SetOfForcingValues( 
        [
        air_density,
        air_spec_hum,
        air_temperature,
        wet_fraction,
        lw_in_total,
        sw_in_total,
        wind,
        pressure,
        ]
    )


# add model interfaces to model

netatmomodel.variables = state_variables
netatmomodel.parameters = parameters
netatmomodel.forcing = forcing

# Now the time function is set and value may be set
# https://en.wikipedia.org/wiki/Enthalpy_of_vaporization
netatmomodel.parameters["L"].value = 2257 
netatmomodel.parameters["A_s"].value = calibration["exposed_area"]
netatmomodel.parameters["eps_s"].value = calibration["weighted_emissivity"]
netatmomodel.parameters["alpha_s"].value = calibration["albedo"]
netatmomodel.parameters["eta"].value = calibration["heat_flux_parameter"]
netatmomodel.parameters["C_s"].value = calibration["surface_heat_capacity"]
netatmomodel.parameters["C_m"].value = calibration["module_heat_capacity"]
netatmomodel.parameters["d_m"].value = calibration["total_diameter"]
netatmomodel.parameters["h_m"].value = calibration["total_height"]
netatmomodel.parameters["lambda"].value = \
    calibration["heat_conduction_parameter"]

# estimate taken from 
# http://wettermast-hamburg.zmaw.de/frame.php?doc=Zeitreihen48h.htm#STRAHLUNG
netatmomodel.forcing["SW_in"].value = 10
# same
netatmomodel.forcing["LW_in"].value = \
    meteorology.radiation.blackbody_radiation(cel2kel(23))
# same
netatmomodel.forcing["T_a"].times  = np.array(MEAS["hmp"]["time_seconds"])
netatmomodel.forcing["T_a"].values = np.array(cel2kel(MEAS["hmp"]["TT3"]))
# same
netatmomodel.forcing["rho_a"].times  = np.array(MEAS["ptb"]["time_seconds"])
netatmomodel.forcing["rho_a"].values = np.array(MEAS["ptb"]["density"])
# same
netatmomodel.forcing["q_a"].value = 0
# same
netatmomodel.forcing["v"].times  = np.array(MEAS["wind"]["time_seconds"])
netatmomodel.forcing["v"].values = np.array(MEAS["wind"]["windspeed"])
# assume dry module
netatmomodel.forcing["a_wet"].value = 0
# guess
netatmomodel.forcing["p"].times  = np.array(MEAS["ptb"]["time_seconds"])
netatmomodel.forcing["p"].values = np.array(MEAS["ptb"]["B1"]*1e2)

# initial condition
# surface is hotter than module
netatmomodel.variables["T_s"].value = cel2kel(MEAS["kt19"]["temperature"][0])
# module is cooler than surface
netatmomodel.variables["T_m"].value = cel2kel(MEAS["netatmo"]["Temperature"][0])

#################
### Equations ###
#################
def q_sat(p,T):
    e_s = e_magnus(T)
    return ( 0.622 * e_s ) / (p - 0.378 * e_s)

class SurfaceTemperatureEquation(PrognosticEquation):
    def linear_factor(self, time = None ):
        def v(var): return self.input[var](time)
        res = ( 
        # sensible heat flux
        - v("rho_a") * specific_heat_capacity_dry_air * v("eta") * v("v") \
        # inward energy flux
        - v("lambda") / v("d_m") \
        ) \
        * v("A_s") / v("C_s")
        return res

    def independent_addend(self, time = None ):
        def v(var): return self.input[var](time)
        res = ( 
        # long-wave in
        + v("eps_s") * v("LW_in") \
        # short-wave in
        + ( 1 - v("alpha_s") ) * v("SW_in") / v("A_s") * v("h_m") * v("d_m")  \
        # sensible heat flux
        + v("rho_a") * specific_heat_capacity_dry_air \
            * v("T_a") * v("eta") * v("v") \
        # latent heat flux
        + v("a_wet") * v("rho_a") * v("L") * v("q_a") * v("eta") * v("v") 
        # inward energy flux
        + v("lambda") / v("d_m") * v("T_m") \
        ) \
        * v("A_s") / v("C_s")

        return res

    def nonlinear_addend(self, time = None, variablevalue = None):
        def v(var): 
            if not variablevalue is None and var == self.variable.id: 
                return variablevalue # return the specified value
            return self.input[var](time)

        res = ( 
        # long-wave out
        - v("eps_s") * stefan_boltzmann_constant * v("T_s") ** 4 \
        # latent heat flux
        - v("a_wet") * v("rho_a") * v("L") * q_sat(v("p"),v("T_s")) \
            * v("eta") * v("v") \
        ) \
        * v("A_s") / v("C_s")

        return res


class ModuleTemperatureEquation(PrognosticEquation):
    def linear_factor(self, time = None):
        def v(var): return self.input[var](time)
        res = - v("lambda") / v("C_m") * v("A_s") / v("d_m")
        return res

    def independent_addend(self, time = None):
        def v(var): return self.input[var](time)
        res = v("lambda") / v("C_m") * v("A_s") / v("d_m") * v("T_s") 
        return res

    def nonlinear_addend(self, *args, **kwargs):
        return 0

surface_temperature_equation = SurfaceTemperatureEquation( 
    variable = netatmomodel.variables["T_s"],
    description = "energy balance for the module surface",
    long_description = "energy balance for the module surface",
    input = SetOfInterfaceValues(elements=[
        netatmomodel.parameters["alpha_s"],
        netatmomodel.parameters["A_s"],
        netatmomodel.parameters["C_s"],
        netatmomodel.parameters["eps_s"],
        netatmomodel.parameters["eta"],
        netatmomodel.parameters["L"],
        netatmomodel.parameters["lambda"],
        netatmomodel.parameters["d_m"],
        netatmomodel.parameters["h_m"],
        netatmomodel.forcing["rho_a"],
        netatmomodel.forcing["q_a"],
        netatmomodel.forcing["a_wet"],
        netatmomodel.forcing["LW_in"],
        netatmomodel.forcing["SW_in"],
        netatmomodel.forcing["T_a"],
        netatmomodel.forcing["v"],
        netatmomodel.forcing["p"],
        netatmomodel.variables["T_m"],
        netatmomodel.variables["T_s"],
        ]),
    )

module_temperature_equation = ModuleTemperatureEquation( 
    variable = netatmomodel.variables["T_m"],
    description = "energy balance for the module measured inner",
    long_description = "energy balance for the module measure inner",
    input = SetOfInterfaceValues(elements=[
        netatmomodel.variables["T_s"],
        netatmomodel.variables["T_m"],
        netatmomodel.parameters["C_m"],
        netatmomodel.parameters["lambda"],
        netatmomodel.parameters["d_m"],
        netatmomodel.parameters["A_s"],
        ]),
    )

surface_temperature_equation_scheme = RungeKutta4( 
    equation = surface_temperature_equation,
    fallback_max_timestep = 10,
    )

module_temperature_equation_scheme = EulerExplicit( 
    equation = module_temperature_equation,
    fallback_max_timestep = 10,
    )

equation_schemes = SetOfNumericalSchemes( elements = 
        [
        surface_temperature_equation_scheme,
        module_temperature_equation_scheme,
        ]
        )
equation_schemes.fallback_plan = [ 
    [ module_temperature_equation.variable.id, [0.5,1] ],
    [ surface_temperature_equation.variable.id, [1] ],
    ]

# add equations to model
netatmomodel.numericalschemes = equation_schemes

# the model summary
print(netatmomodel)

# integrate
netatmomodel.integrate(final_time = MEAS["netatmo"]["time_seconds"].max())

# plot
fig, ax = plt.subplots()

ax.set_title("{}\n{}".format(netatmomodel.name,netatmo_name))

times = np.union1d(module_temperature.times,surface_temperature.times)

# ax2 = ax.twinx()

# ax2.plot(times / 60, 
#     netatmomodel.forcing["v"](times),
#     lw = 1,
#     c = "k",
#     ls = "--",
#     label = netatmomodel.forcing["v"].name,
#     )
# ax2.set_ylabel("wind speed [m/s]")

# ax2.legend()

ax.plot(times / 60, 
    kel2cel(netatmomodel.forcing["T_a"](times)),
    lw = 2,
    ls = "--",
    c = "royalblue",
    label = netatmomodel.forcing["T_a"].name
    )

ax.plot(
    MEAS["netatmo"]["time_seconds"] / 60, 
    MEAS["netatmo"]["Temperature"], 
    lw = 3,
    ls = "--",
    c = "darkgreen",
    label = "measured module temperature",
    )

ax.plot(
    times / 60, 
    kel2cel(netatmomodel.variables["T_m"](times)),
    drawstyle="steps-pre", lw = 3,
    c = "darkgreen",
    label = "simulated {}".format(netatmomodel.variables["T_m"].name)
    )

ax.plot(
    MEAS["kt19"]["time_seconds"] / 60, 
    MEAS["kt19"]["temperature"], 
    lw = 2,
    ls = "--",
    c = "darkorange",
    label = "measured surface temperature",
    )

ax.plot(
    times / 60, 
    kel2cel(netatmomodel.variables["T_s"](times)),
    drawstyle="steps-pre", lw = 2,
    c = "darkorange",
    label = "simulated {}".format(netatmomodel.variables["T_s"].name)
    )


ax.legend()
ax.grid(ls="--")
ax.set_xlabel("time [min]")
ax.set_ylabel("temperature [°C]")

if args.show:
    plt.show()

logging.info("Saving plot to {}".format(args.output))
plt.tight_layout()
plt.savefig(args.output)
plt.close("all")
