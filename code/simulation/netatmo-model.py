#!/usr/bin/env python3
# system module
import argparse
import textwrap
import logging
import json
import glob
import os
import re

# own modules
import numericalmodel
from numericalmodel.numericalmodel import NumericalModel
from numericalmodel.interfaces import *
from numericalmodel.numericalschemes import *
from numericalmodel.equations import *
from numericalmodel.examples.numericalmodel import \
    NumericalModelExampleArgumentParser

import meteorology
from meteorology.temperature import cel2kel,kel2cel
from meteorology.humidity import saturation_water_vapour_pressure as e_magnus
from meteorology.constants import *

# external modules
import numpy as np

logging.basicConfig(level=logging.INFO)

parser = NumericalModelExampleArgumentParser(
    description = "NetatmoSimulator"
    )

# read calibration results
calibration = {}
RESULTS_DIR=os.path.join(os.path.dirname(__file__),
    "..","..","calibration","results")
calibration_files = glob.glob(os.path.join(RESULTS_DIR,"*.json"))
for f in calibration_files:
    with open(f) as fh:
        calibration.update(json.load(fh))

# the model is a numerical model
model = NumericalModel()

# Fill metadata
model.name = "netatmo outdoor module energy balance model"
model.version = "0.0.1-dev"
model.description = """
zero-dimensional energy balance model of a netatmo outdoor module's temperature
""".strip()
model.long_description = """
This model is developed as part of the master thesis of Yann Büchau at the
University of Hamburg.
""".strip()
model.authors = {
"theory": "BSc. Met. Yann Büchau & Prof. Dr. Felix Ament",
"idea": "Prof. Dr. Felix Ament",
"code": "BSc. Met. Yann Büchau",
}

# set initial time
# TIME_OFFSET = 1e11
model.initial_time = 0

# State variables

module_temperature = StateVariable(
    id = "T_m", name="simulated measured temperature", unit = "K",
    bounds = [0,cel2kel(100)],
    )
surface_temperature = StateVariable(
    id = "T_s", name="simulated surface temperature", unit = "K",
    bounds = [0,cel2kel(100)],
    )

state_variables = SetOfStateVariables(
        [
        module_temperature,
        surface_temperature,
        ]
    )

# Parameters

surface_emissivity = Parameter(
    id = "eps_s", name = "long-wave outdoor module surface emissivity",
    bounds = [0,1],
    )
surface_albedo = Parameter(
    id = "alpha_s", name = "short-wave outdoor module surface albedo",
    bounds = [0,1],
    )
module_heatcapacity = Parameter(
    id = "C_m", name = "effective inner module heat capacity", unit = "J/kg",
    bounds = [0,np.Inf],
    )
surface_heatcapacity = Parameter(
    id = "C_s", name = "effective outdoor module surface heat capacity",
    unit = "J/kg", bounds = [0,np.Inf],
    )
ventilated_hf_parameter = Parameter(
    id = "eta", name = "ventilated heat flux parameter",
    bounds = [0,np.Inf],
    )
heat_conduction_parameter = Parameter(
    id = "lambda", name = "inward heat conduction parameter",
    bounds = [0,np.Inf],
    )
module_diameter = Parameter(
    id = "d_m", name = "total module diameter",
    bounds = [0,np.Inf], unit = "m",
    )
module_height = Parameter(
    id = "h_m", name = "total module height",
    bounds = [0,np.Inf], unit = "m",
    )

parameters = SetOfParameters(
        [
        surface_emissivity,
        surface_albedo,
        module_heatcapacity,
        surface_heatcapacity,
        ventilated_hf_parameter,
        heat_conduction_parameter,
        module_diameter,
        module_height,
        ]
    )


# forcing

air_density = ForcingValue(
    id = "rho_a", name = "surrounding air density", unit = "kg/m^3",
    bounds = [0,3],
    )
air_spec_hum = ForcingValue(
    id = "q_a", name = "surrounding air specifiy humidity", unit = "kg/kg",
    bounds = [0,0.015],
    )
air_temperature = ForcingValue(
    id = "T_a", name = "surrounding air temperature", unit = "K",
    bounds = [cel2kel(-50),np.Inf],
    )
wind = ForcingValue(
    id = "v", name = "wind velocity", unit = "m/s",
    bounds = [0,20],
    )
wet_fraction = ForcingValue(
    id = "a_wet", name = "fraction of wet area on outdoor module's "
        "exposed surface",
    bounds = [0,1],
    )
lw_in_total = ForcingValue(
    id = "LW_in", name = "total long-wave incoming radiation", unit = "W/m^2",
    bounds = [0,1000],
    )
sw_in_total = ForcingValue(
    id = "SW_in", name = "total short-wave incoming radiation", unit = "W/m^2",
    bounds = [0,1500],
    )
pressure = ForcingValue(
    id = "p", name = "pressure", unit = "Pa",
    bounds = [500e2,1200e2],
    )

forcing = SetOfForcingValues(
        [
        air_density,
        air_spec_hum,
        air_temperature,
        wet_fraction,
        lw_in_total,
        sw_in_total,
        wind,
        pressure,
        ]
    )

# observations

air_temperature_obs = Observation(
    id = "T_a", name = "measured surrounding air temperature", unit = "K",
    bounds = [cel2kel(-50),cel2kel(100)],
    )

observations = SetOfObservations(
    [
    air_temperature_obs
    ]
    )

# add model interfaces to model

model.variables = state_variables
model.parameters = parameters
model.forcing = forcing
model.observations = observations

# Now the time function is set and value may be set
model.parameters["eps_s"].value = calibration["weighted_emissivity"]
model.parameters["alpha_s"].value = calibration["albedo"]
model.parameters["eta"].value = calibration["heat_flux_parameter"]
model.parameters["C_s"].value = calibration["surface_heat_capacity"]
model.parameters["C_m"].value = calibration["module_heat_capacity"]
model.parameters["d_m"].value = calibration["total_diameter"]
model.parameters["h_m"].value = calibration["total_height"]
model.parameters["lambda"].value = \
    calibration["heat_conduction_parameter"]

# estimate taken from
# http://wettermast-hamburg.zmaw.de/frame.php?doc=Zeitreihen48h.htm#STRAHLUNG
model.forcing["SW_in"].value = 10
# same
model.forcing["LW_in"].value = \
    meteorology.radiation.blackbody_radiation(cel2kel(23))
# same
model.forcing["T_a"].value = cel2kel(15)
# same
model.forcing["rho_a"].value = 1.2
# same
model.forcing["q_a"].value = 0
# same
model.forcing["v"].value = 4
# assume dry module
model.forcing["a_wet"].value = 0
# guess
model.forcing["p"].value = 1000e2

# observations
model.observations["T_a"].value = cel2kel(24)

# initial condition
model.variables["T_s"].value = cel2kel(15)
model.variables["T_m"].value = cel2kel(20)

#################
### Equations ###
#################
def q_sat(p,T):
    e_s = e_magnus(T)
    return ( 0.622 * e_s ) / (p - 0.378 * e_s)

def area(d,h):
    return np.pi * (d/2)**2 + h * d

class SurfaceTemperatureEquation(PrognosticEquation):
    def linear_factor(self, time = None ):
        def v(var): return self.input[var](time)
        res = (
        # sensible heat flux
        - v("rho_a") * specific_heat_capacity_dry_air * v("eta") * v("v") \
        # inward energy flux
        - v("lambda") / v("d_m") \
        ) \
        * area(v("d_m"),v("h_m")) / v("C_s")
        return res

    def independent_addend(self, time = None ):
        def v(var): return self.input[var](time)
        res = (
        # long-wave in
        + v("eps_s") * v("LW_in") \
        # short-wave in
        + ( 1 - v("alpha_s") ) * v("SW_in") / \
            area(v("d_m"),v("h_m")) * v("h_m") * v("d_m")  \
        # sensible heat flux
        + v("rho_a") * specific_heat_capacity_dry_air \
            * v("T_a") * v("eta") * v("v") \
        # latent heat flux
        + v("a_wet") * v("rho_a") * enthalpy_of_vaporization_water_100C \
            * v("q_a") * v("eta") * v("v")
        # inward energy flux
        + v("lambda") / v("d_m") * v("T_m") \
        ) \
        * area(v("d_m"),v("h_m")) / v("C_s")

        return res

    def nonlinear_addend(self, time = None, variablevalue = None):
        def v(var):
            if not variablevalue is None and var == self.variable.id:
                return variablevalue # return the specified value
            return self.input[var](time)

        res = (
        # long-wave out
        - v("eps_s") * stefan_boltzmann_constant * v("T_s") ** 4 \
        # latent heat flux
        - v("a_wet") * v("rho_a") * enthalpy_of_vaporization_water_100C \
            * q_sat(v("p"),v("T_s")) * v("eta") * v("v") \
        ) \
        * area(v("d_m"),v("h_m")) / v("C_s")

        return res


class ModuleTemperatureEquation(PrognosticEquation):
    def linear_factor(self, time = None):
        def v(var): return self.input[var](time)
        res = - v("lambda") / v("C_m") * area(v("d_m"),v("h_m")) / v("d_m")
        return res

    def independent_addend(self, time = None):
        def v(var): return self.input[var](time)
        res = v("lambda") / v("C_m") * \
            area(v("d_m"),v("h_m")) / v("d_m") * v("T_s")
        return res

    def nonlinear_addend(self, *args, **kwargs):
        return 0

surface_temperature_equation = SurfaceTemperatureEquation(
    variable = model.variables["T_s"],
    description = "energy balance for the module surface",
    long_description = "energy balance for the module surface",
    input = SetOfInterfaceValues(elements=[
        model.parameters["alpha_s"],
        model.parameters["C_s"],
        model.parameters["eps_s"],
        model.parameters["eta"],
        model.parameters["lambda"],
        model.parameters["d_m"],
        model.parameters["h_m"],
        model.forcing["rho_a"],
        model.forcing["q_a"],
        model.forcing["a_wet"],
        model.forcing["LW_in"],
        model.forcing["SW_in"],
        model.forcing["T_a"],
        model.forcing["v"],
        model.forcing["p"],
        model.variables["T_m"],
        model.variables["T_s"],
        ]),
    )

module_temperature_equation = ModuleTemperatureEquation(
    variable = model.variables["T_m"],
    description = "energy balance for the module measured inner",
    long_description = "energy balance for the module measure inner",
    input = SetOfInterfaceValues(elements=[
        model.variables["T_s"],
        model.variables["T_m"],
        model.parameters["C_m"],
        model.parameters["lambda"],
        model.parameters["d_m"],
        model.parameters["h_m"],
        ]),
    )

surface_temperature_equation_scheme = RungeKutta4(
    equation = surface_temperature_equation,
    fallback_max_timestep = 10,
    )

module_temperature_equation_scheme = EulerExplicit(
    equation = module_temperature_equation,
    fallback_max_timestep = 10,
    )

equation_schemes = SetOfNumericalSchemes( elements =
        [
        surface_temperature_equation_scheme,
        module_temperature_equation_scheme,
        ]
        )
equation_schemes.fallback_plan = [
    [ module_temperature_equation.variable.id, [0.5,1] ],
    [ surface_temperature_equation.variable.id, [1] ],
    ]

# add equations to model
model.numericalschemes = equation_schemes

# the model summary
# print(model)

parser.cli(model)
