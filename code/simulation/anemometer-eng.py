#!/usr/bin/env python3

# import the module
import numericalmodel
from numericalmodel.interfaces import *
from numericalmodel.numericalschemes import *
import numpy as np

# create a model
model = numericalmodel.numericalmodel.NumericalModel( 
    name = "4-cup anemometer model"
    )
model.initial_time = 0

# add the values to the model
model.variables  = SetOfStateVariables( [ 
    StateVariable( id = "f", name = "frequency", unit = "1/s",
        bounds = [0,np.Inf]),
    ]  )

model.parameters = SetOfParameters( [ 
    Parameter( id = "m", name = "cup mass", unit = "kg", 
        bounds = [0,0.3] ),
    Parameter( id = "A", name = "cup area", unit = "m^2", 
        bounds = [0,0.1] ),
    Parameter( id = "R", name = "distance cup - rotation axis", 
        unit = "m", bounds = [0,np.Inf] ),
    Parameter( id = "cw1", name = "frontal drag value", 
        unit = "1", bounds = [0,np.Inf] ),
    Parameter( id = "cw2", name = "back drag value", 
        unit = "1", bounds = [0,np.Inf] ),
    Parameter( id = "F_s", name = "stitction anguluar momentum", 
        unit = "Nm", bounds = [0,np.Inf] ),
    Parameter( id = "F_f0", name = "friction angular momentum offset", 
        unit = "Nm", bounds = [0,np.Inf] ),
    Parameter( id = "F_fa", name = "friction angular momentum linear parameter" 
        "momentum", unit = "1/s^2", bounds = [0,np.Inf] ),
    ] )

model.forcing = SetOfForcingValues( [
    ForcingValue( id = "v", name = "wind velocity", unit = "m/s", 
        bounds = [0,40] ),
    ForcingValue( id = "rho", name = "air density", unit = "kg/m^3", 
        bounds = [0,np.Inf] ),
    ] )

# set initial values
model.variables["f"].value  = 0
model.parameters["R"].value = 0.08
model.parameters["A"].value = 5e-4
model.parameters["m"].value = 0.001
model.parameters["F_s"].value = model.parameters["R"].value*9.81*0.002
model.parameters["F_f0"].value = model.parameters["F_s"].value * 0.1
model.parameters["F_fa"].value = 0
model.parameters["F_s"].bounds = [0,0.3*9.81*0.005]
model.parameters["F_f0"].bounds = model.parameters["F_s"].bounds
model.parameters["cw1"].value = 1.4
model.parameters["cw2"].value = 0.4

nwind = 500
wind = np.concatenate([
    np.linspace(0,10,nwind),
    np.convolve(
        (np.random.random(nwind)+1.5)**3,
        np.ones((int(nwind/20)))/int(nwind/20),
        mode="same",
        ),
    np.linspace(10,4,nwind),
    np.full(nwind,0),
    np.full(nwind,5),
    np.full(nwind,0),
    ])
model.forcing["v"].times =  np.linspace(0,60,wind.size)
model.forcing["v"].values = wind

model.forcing["rho"].value = 1.2

# define the equation
class AngularMomentumEquation(numericalmodel.equations.PrognosticEquation):
    """
    Class for the anemometer equation
    """
    def factor(self, time):
        i = lambda n: self.input[n](time)
        I = 4 * i("m") * i("R") ** 2
        return 1/2 * (i("rho")*i("A")*i("cw2")*i("R")) / (2*np.pi*I)

    def derivative(self, time = None, variablevalue = None):
        def i(var): 
            if not variablevalue is None and var == self.variable.id: 
                return variablevalue # return the specified value
            return self.input[var](time)
        u = 2 * np.pi * i("f") * i("R")
        v_f = max(i("v"),u)
        windforce = self.factor(time) * \
            ( i("cw1")/i("cw2") * (v_f-u)**2 - 2*u**2 - (i("v")+u)**2)
        if i("f") == 0: # does not turn
            if windforce > i("F_s"):
                friction = - i("F_s")
            else:
                friction = - windforce
        else: # turns
            dynfriction = (i("F_fa") * i("f") + i("F_f0"))
            if windforce > dynfriction:
                friction = - dynfriction
            else:
                fkrit = 1e-2
                if i("f") < fkrit:
                    # This is a little magic to prevent the anemometer from
                    # turning backwards because the dynamic friction is greater
                    # than the wind-driven angular moments.
                    # The divisor 2 is an arbitrary choice.
                    friction = - (windforce/2)
                else:
                    friction = - dynfriction

        return windforce + friction

# create an equation object
equation_input = []
for ivs in model.variables, model.parameters, model.forcing:
    equation_input.extend(ivs.elements)
equation = AngularMomentumEquation(
    variable = model.variables["f"],
    input = SetOfInterfaceValues( equation_input ),
    )

# add the numerical scheme to the model
model.numericalschemes = SetOfNumericalSchemes(
    [RungeKutta4(equation=equation,fallback_max_timestep=0.1)])

# plot the results
model.gui(use_fallback=True)

