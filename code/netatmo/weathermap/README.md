# Netatmo Weathermap

## Plots

Run `make` (optionally with `-kj10` to run it in parallel) to produce plots from
the snapshots.

Run `make animation` (you might need to run it with `-B` if it complains that
`nothing has to be done`) to create a movie slideshow of the plots.

## Interesting days

### Radiation error

- **30.08.2017 8:00 - 12:00 UTC**
    - **8:00 UTC**: all module temperatures are pretty homogeneous (before effective sunrise?)
    - **10:00 UTC**: some modules begin to warm up pretty dramatically (radiation error?)
    - **12:00 UTC**: all modules cool down again (clouds?)
- actually pretty much any day with some solar input...

### fronts

- **13.09.2017 11:00 - 12:00 UTC**
    - cold front
