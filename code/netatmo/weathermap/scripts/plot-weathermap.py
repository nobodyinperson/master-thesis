#!/usr/bin/env python3
# system modules
import argparse
import textwrap
import itertools
import logging

# internal modules

# external modules
import pandas as pd
from mpl_toolkits.basemap import Basemap
import numpy as np

parser = argparse.ArgumentParser(
    description = textwrap.dedent("""
        Plot a Getpublicdata snapshot
        """)
    )
parser.add_argument("-i","--input",help = "Getpublicdata csv input file",
    required = True)
parser.add_argument("-o","--output",help = "output file", required = True)
parser.add_argument("--lat_ne", required = False,
    help = "North-east latitude in degrees [-85;85]",type = float)
parser.add_argument("--lon_ne", required = False,
    help = "North-east longitude [-180;180]", type = float)
parser.add_argument("--lat_sw", required = False,
    help = "South-west latitude [-85;85]",type = float)
parser.add_argument("--lon_sw", required = False,
    help = "South-west longitude [-180;180]",type=float)
parser.add_argument("--border", help = "enlarge view by what percentage?",
    required = False, default = 0.1, type = float)
parser.add_argument("--bgfile", help = "background map file",
    required = False )
parser.add_argument("--resolution", help = "map resolution",
    choices = ["c","l","i","h","f"], default = "i")
parser.add_argument("--distances", help = "plot a distances histogram instead",
    action="store_true", default = False)
parser.add_argument("-v","--verbose", help = "verbose output",
    action="store_true", default = False)
parser.add_argument("--debug", help = "even more verbose output",
    action="store_true", default = False)
parser.add_argument("--show", help = "show the plot?",
    action="store_true", default = False)
# parse args
args = parser.parse_args()

if not args.show:
    import matplotlib
    matplotlib.use("agg")
import matplotlib.pyplot as plt
from matplotlib.image import imread

# set up logging
loglevel = logging.WARNING
if args.verbose: loglevel = logging.INFO
if args.debug:   loglevel = logging.DEBUG
logging.basicConfig(level = loglevel)

# read input
logging.info("reading input file '{}'...".format(args.input))
data = pd.read_csv(args.input)
logging.info("converting times...")
for col in data.columns:
    if col.startswith("time_"):
        logging.info("converting column '{}'...".format(col))
        data[col] = pd.to_datetime(data[col])
lat = data.latitude
lon = data.longitude
lat_ne, lon_ne, lat_sw, lon_sw = lat.max(), lon.max(), lat.min(), lon.min()
latrange = lat_ne - lat_sw
lonrange = lon_ne - lon_sw

# plot
logging.info("begin plotting...")

# set up map
logging.info("loading the map...")
llcrnrlon = args.lon_sw if args.lon_sw else lon_sw - lonrange * args.border
urcrnrlon = args.lon_ne if args.lon_ne else lon_ne + lonrange * args.border
llcrnrlat = args.lat_sw if args.lat_sw else lat_sw - latrange * args.border
urcrnrlat = args.lat_ne if args.lat_ne else lat_ne + latrange * args.border
logging.debug("plot region: {}".format(
    [llcrnrlon,llcrnrlat,urcrnrlon,urcrnrlat]))
m = Basemap(
    llcrnrlon=llcrnrlon,llcrnrlat=llcrnrlat,
    urcrnrlon=urcrnrlon,urcrnrlat=urcrnrlat,
    projection="merc",
    resolution="c" if args.distances else args.resolution,
    )

notime = data.time_temperature.isnull() # indices without time
if not args.distances:
    data = data[~notime] # drop stations without temperature time

temperature = data.temperature
time = np.array(data.time_temperature)
time_diff = (time - np.datetime64("1970-01-01")).astype("float")
time_mean   = time[np.argmin(time_diff.mean()     - time_diff)]
time_median = time[np.argmin(np.median(time_diff) - time_diff)]
time_median = pd.to_datetime(time_median)

# only keep data that is inside sensible time range
# data = data[abs((time-time_median).astype(float)*1e-9)<10*60]

if args.distances:
    logging.info("Calculating distances...")
    x,y = m(np.array(data.longitude),np.array(data.latitude))
    distances = np.array([min(np.sqrt((x1-x2)**2+(y1-y2)**2) \
        for x1,y1 in zip(x,y) if (x1!=x2 and y1!=y2)) for x2,y2 in zip(x,y)])
    distances = distances[distances>0]

    h=plt.hist(distances,bins=np.arange(0,3000,50),normed=False,
        label=r"Hamburg ({date})".format(
            date=time_median.strftime("%Y-%m-%d")))
    plt.title(
        r"bin size: \SI{{{binsize:.0f}}}{{\metre}}, "
        # r"mean: \SI{{{mean:.0f}}}{{\metre}}, "
        r"median: \SI{{{median:.0f}}}{{\metre}}, "
        r"mode: \SI{{{mode:.0f}}}{{\metre}}".format(
        binsize=int(np.unique(np.diff(h[1])).mean()),
        mode=h[1][h[0].argmax():(h[0].argmax()+2)].mean(),
        mean=distances.mean(),
        median=np.median(distances),
        ))
    plt.xlabel(r"horiz. spacing of adjacent Netatmo CWS in \si{{\metre}}")
    plt.ylabel("count")
    plt.legend(handlelength=0)
else:
    if args.bgfile:
        logging.info("drawing map...")
        logging.info("reading background file '{}'".format(args.bgfile))
        bgimg = imread(args.bgfile)
        logging.info("drawing background image...")
        m.imshow(np.flipud(bgimg),zorder=0,alpha=1)
        latlines = m.drawparallels(np.arange(50,60,0.1),labels=[1,0,0,0],
            zorder=9,linewidth=0.5)
        lonlines = m.drawmeridians(np.arange(8,11,0.1),labels=[0,0,0,1],
            zorder=9,linewidth=0.5)
        for lon,(line,text) in lonlines.items():
            text[0].set_rotation(30)
        for lon,(line,text) in latlines.items():
            text[0].set_rotation(30)
        plt.xlabel("longitude", labelpad = 20)
        plt.ylabel("latitude", labelpad = 30)
    else:
        m.fillcontinents(zorder=0)
        m.drawcoastlines()
        m.drawcountries()
        m.drawrivers()
        m.drawmapboundary(fill_color='royalblue')



    plt.title("{} - {} Netatmo readings".format(
        pd.to_datetime(time_median).strftime("%d.%m.%Y %H:%M UTC"),
        data.shape[0]))

    x,y = m(np.array(data.longitude),np.array(data.latitude))
    m.scatter( x, y,
        c = data.temperature,
        vmin = 0, vmax=15,
        zorder=10,
        alpha = 0.9,
        edgecolor="None",
        )
    cbar = m.colorbar(
        label=r"outdoor module temperature in \si{\celsius}",
        )
    cbar.solids.set(alpha=1) # let colorbar be opaque

logging.info("save plot to file '{}'...".format(args.output))
plt.savefig(args.output)
logging.info("plot saved to file '{}'".format(args.output))
