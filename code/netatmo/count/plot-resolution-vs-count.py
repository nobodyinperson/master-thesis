#!/usr/bin/env python3
import pandas as pd
import matplotlib.pyplot as plt

HAM = {
    "lat_ne": 53.7373,
    "lat_sw": 53.3964,
    "lon_ne": 10.3217,
    "lon_sw":  9.7284,
    }
hamburg_degrees = max(HAM["lat_ne"]-HAM["lat_sw"],HAM["lon_ne"]-HAM["lon_sw"])

# read data
count = pd.read_csv("resolution-vs-count.csv")

optimum_res = count["res.degree"][count["count"].argmax()]
plt.scatter(count["res.degree"],count["count"],label="amount of counted stations")
plt.vlines(x=optimum_res,label="optimium at ~{:.2f} degrees".format(
    optimum_res), ymin=plt.gca().get_ylim()[0],ymax=plt.gca().get_ylim()[1],
    linestyles="dashed",color="black")
plt.vlines(x=hamburg_degrees,label="approx diameter of Hamburg",
    ymin=plt.gca().get_ylim()[0],ymax=plt.gca().get_ylim()[1],
    linestyles="dotted",color="darkred")
plt.grid()
plt.legend()
plt.title("Netatmo zoom levels")
plt.xlim(0,hamburg_degrees*1.1)
plt.xlabel("request resolution [degrees]")
plt.ylabel("count of stations in square Hamburg region")
plt.savefig("resolution-vs-count.pdf")
plt.close("all")
