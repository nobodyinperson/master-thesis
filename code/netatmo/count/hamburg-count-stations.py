#!/usr/bin/env python3
from patatmo.utils import *
from patatmo.cli import get_client
import numpy as np
import logging
import re
from math import floor
from reverse_geocoder import search
import matplotlib.pyplot as plt

plt.ion()

logging.basicConfig(level = logging.INFO)

client = get_client()

HAM = {
    "lat_ne": 53.7373,
    "lat_sw": 53.3964,
    "lon_ne": 10.3217,
    "lon_sw":  9.7284,
    }

f = open("resolution-vs-count.csv","a")

# nsplit = 0
# lats = np.linspace(
#     HAM["lat_sw"],
#     HAM["lat_ne"],
#     2+nsplit)
# lons = np.linspace(
#     HAM["lon_sw"],
#     HAM["lon_ne"],
#     2+nsplit)

resolutions = np.linspace(0.2,0.6,num=20)
stations_by_resolution = []

check_really_inside = False

for resdiff in resolutions:
    logging.info("Current resolution: {} degrees".format(resdiff))
    nlat = floor((HAM["lat_ne"] - HAM["lat_sw"]) / resdiff)
    nlat = max(nlat,2)
    lats = np.linspace(
        HAM["lat_sw"],
        HAM["lat_ne"],
        num = nlat,
        )
    nlon = floor((HAM["lon_ne"] - HAM["lon_sw"]) / resdiff)
    nlon = max(nlon,2)
    lons = np.linspace(
        HAM["lon_sw"],
        HAM["lon_ne"],
        num = nlon,
        )

    hamburg_stations = 0
    hamburg_ids = np.array([])
    for lat_sw,lat_ne,in zip(lats[:-1],lats[1:]):
        for lon_sw,lon_ne in zip(lons[:-1],lons[1:]):
            region = \
                {"lat_sw":lat_sw,"lat_ne":lat_ne,"lon_sw":lon_sw,"lon_ne":lon_ne}
            logging.debug("region: {}".format(region))
            if check_really_inside:
                center = ( np.mean([lat_sw,lat_ne]), np.mean([lon_sw,lon_ne]) )
                logging.debug("region center {}".format(center))
                admin = search( center )[0].get("admin1")
                center_in_hamburg = bool(re.match("hamburg",admin,flags=re.IGNORECASE))
            else:
                center_in_hamburg = True
            if center_in_hamburg:
                hamburg = client.Getpublicdata(region=region)
                dataframe = hamburg.dataframe(only_inside=True)
                hamburg_ids = np.unique(np.concatenate((dataframe.id,hamburg_ids)))
                hamburg_stations += dataframe.shape[0]
                logging.info("{} stations in Hamburg so far by "
                    "counting api results".format(hamburg_stations))
                logging.info("{} UNIQUE stations in Hamburg so far".format(
                    hamburg_ids.size))
            else:
                logging.info("center of region {} is not in Hamburg".format(region))
    logging.info("At resolution of {} degrees there are {} " 
        "stations in Hamburg".format(resdiff,hamburg_ids.size))
    # add to stationslist
    stations_by_resolution.append(hamburg_ids.size)
    f.write("{},{}\n".format(resdiff,hamburg_ids.size))
    f.flush()

f.close()

# convert to np.array
stations_by_resolution = np.array(stations_by_resolution)

