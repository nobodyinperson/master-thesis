#!/usr/bin/env python3
import argparse
import textwrap
import logging
import re,os
import json

# external modules
import pandas as pd
import numpy as np
import statsmodels.api as sm
from meteorology.temperature import cel2kel,kel2cel
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjust_emissivity
from meteorology.constants import stefan_boltzmann_constant

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser(
    description = textwrap.dedent("""
        surface heat capacity calibration
        """)
    )

# Getmeasure arguments
parser.add_argument("--kt19", help = "KT19 measurement data", required = True)
parser.add_argument("--hmp", help = "HMP measurement data", required = True)
parser.add_argument("--netatmo", help = "NETATMO measurement data", nargs="+")
parser.add_argument("--results", help = "results folder",
    default = os.path.join(os.path.dirname(__file__),"..","results"))
parser.add_argument("-t","--type",choices=["timeseries-reindexed",
    "timeseries-raw","fit"], help = "plot type", default = "fit")
parser.add_argument("-o","--output",help = "output plot file", required = True)
parser.add_argument("--show",help = "show the plot?", action = "store_true",
    default = False)
parser.add_argument("--result", help="write resultfile instead of plot",
    action="store_true")
parser.add_argument("--what",choices=["module","surface"],
    default="surface",help="fit the heat capacity for ...?")
parser.add_argument("--markers",help = "netatmo markers", nargs = "+",
    default = ["s",8,"^","v","d","X","P"])
parser.add_argument("--smooth",help = "smoothing window in seconds",
    default=60*5, type = int)

# parse the arguments
args = parser.parse_args()

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
from matplotlib import rc
plt.rcParams["font.size"] = 10
plt.rcParams["lines.markersize"] = 6
plt.rcParams["figure.figsize"] = [x*0.9 for x in (8, 5)]

logging.info("reading results from {}".format(args.results))
with open(os.path.join(args.results,"dimensions.json")) as f:
    dimensions = json.load(f)
with open(os.path.join(args.results,"weight.json")) as f:
    weight = json.load(f)
with open(os.path.join(args.results,"heat-conduction-parameter.json")) as f:
    heat_conduction = json.load(f)
with open(os.path.join(args.results,"long-wave-emissivity.json")) as f:
    emissivity = json.load(f)

def resample(x):
    return x.resample(
        "{}s".format(args.smooth),
        loffset="{}s".format(int(round(args.smooth/2))),
        )
def smooth(x):
    return resample(resample(x).bfill()).mean()

logging.info("read KT19 data {}".format(args.kt19))
kt19 = {}
kt19["raw"] = pd.read_csv(
    args.kt19,sep=";",parse_dates=["time"],index_col="time")
kt19["raw"] = kt19["raw"][~kt19["raw"].index.duplicated(keep="first")]
logging.info("smooth KT19 data...")
kt19["smoothed"] = smooth(kt19["raw"])

logging.info("read HMP data {}".format(args.hmp))
hmp = {}
hmp["raw"] = pd.read_csv(args.hmp,sep=";",parse_dates={"time":["DATE","TIME"]},
    index_col="time")
logging.info("smooth HMP data...")
hmp["smoothed"] = smooth(hmp["raw"])


netatmo = {}
netatmo["raw"] = {}
for f in args.netatmo:
    netatmo_name = re.sub(string=os.path.basename(f),
        pattern="^.*?_([A-Z]+).*$",repl="\g<1>")
    logging.info("read netatmo data {} ({})".format(f,netatmo_name))
    netatmo["raw"][netatmo_name] = pd.read_csv(
        f,sep=",",parse_dates=["time"], index_col="time")

logging.info("smoothing netatmo data...")
netatmo["smoothed"] = { n:smooth(v) for n,v in netatmo["raw"].items()}

# reindex
kt19["reindexed"] = {}
hmp["reindexed"] = {}
for netatmo_name,netatmo_data in netatmo["smoothed"].items():
    kt19["reindexed"][netatmo_name] = kt19["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)
    hmp["reindexed"][netatmo_name]  = hmp["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)

kt19_emissivity = 1
for netatmo_name,netatmo_data in netatmo["smoothed"].items():
    kt19["reindexed"][netatmo_name]["surface_temperature"] = kel2cel(
        adjust_emissivity(
            T = cel2kel( kt19["reindexed"][netatmo_name]["temperature"] ),
            T_ambient = cel2kel( hmp["reindexed"][netatmo_name]["TT3"] ),
            emissivity_old = kt19_emissivity, # KT19 emissivity
            emissivity_new = emissivity["aluminium_emissivity"],
            )
        )

plt.close("all")
fig = plt.figure()
ax = fig.add_subplot(111)

if args.type == "timeseries-raw":
    # Plot
    # ax.set_title("raw data")
    ax.plot(hmp["raw"].index,hmp["raw"].TT3,label="HMP (raw)")
    ax.plot(hmp["smoothed"].index,hmp["smoothed"].TT3,label="HMP (smoothed)")
    ax.plot(kt19["raw"].index,kt19["raw"].temperature,label="KT19 (raw)")
    ax.plot(kt19["smoothed"].index,kt19["smoothed"]["temperature"],
        label="KT19 (smoothed)")
    for typ in ["raw","smoothed"]:
        for netatmo_name,netatmo_data in netatmo[typ].items():
            ax.plot(
                netatmo_data.index,
                netatmo_data.Temperature,
                label="{} ({})".format(netatmo_name,typ))
    ax.legend()
    # this is problematic somehow (OverflowError)...
    # ax.get_xaxis().set_major_formatter(DateFormatter("%H:%M"))
    ax.set_xlabel(r"time [UTC]")
    ax.set_ylabel(r"temperature [\si{\celsius}]")
elif args.type == "timeseries-reindexed":
    # ax.set_title("reindexed to NETATMO")
    for netatmo_name,netatmo_data in netatmo["smoothed"].items():
        # Plot
        ax.plot(
            hmp["reindexed"][netatmo_name].index,
            hmp["reindexed"][netatmo_name].TT3,
            label="HMP (smoothed+reindexed to {})".format(netatmo_name)
            )
        ax.plot(
            kt19["reindexed"][netatmo_name].index,
            kt19["reindexed"][netatmo_name]["temperature"],
            label="KT19 (smoothed+reindexed to {})".format(netatmo_name))
        ax.plot(
            kt19["reindexed"][netatmo_name].index,
            kt19["reindexed"][netatmo_name]["surface_temperature"],
            label="KT19 (smoothed+ambient+reindexed to {})".format(netatmo_name))
        ax.plot(
            netatmo_data.index,
            netatmo_data.Temperature,
            label="{} (smoothed)".format(netatmo_name))
    ax.legend()
    # this is problematic somehow (OverflowError)...
    # ax.get_xaxis().set_major_formatter(DateFormatter("%H:%M"))
    ax.set_xlabel(r"time [UTC]")
    ax.set_ylabel(r"temperature [\si{\celsius}]")
elif args.type == "fit":
    # ax.set_title("{} heat capacity calibration".format(args.what))
    fit = {}
    for (netatmo_name,netatmo_data),cyclestyle \
        in zip(netatmo["smoothed"].items(),plt.rcParams["axes.prop_cycle"]):
        if args.what == "surface":
            temperature = kt19["reindexed"][netatmo_name]["surface_temperature"]
        elif args.what == "module":
            temperature = netatmo_data["Temperature"]
        temperature_diff = np.diff(cel2kel(temperature))
        time_diff = np.diff(temperature.index).astype(float)*1e-9
        temperature_slope = temperature_diff / time_diff
        radiation_budget = dimensions["exposed_area"] * \
            emissivity["aluminium_emissivity"] * stefan_boltzmann_constant \
                * (cel2kel(hmp["reindexed"][netatmo_name].TT3[1:])**4 \
                 - cel2kel(kt19["reindexed"][netatmo_name]\
                    ["surface_temperature"][1:])**4 )
        inward_heat_conduction = dimensions["exposed_area"] * \
                heat_conduction["heat_conduction_parameter"] / \
                dimensions["total_diameter"] \
                *(cel2kel(kt19["reindexed"][netatmo_name]\
                    ["surface_temperature"][1:]) \
                - cel2kel(netatmo_data.Temperature[1:]))
        if args.what == "surface":
            energy_budget = radiation_budget - inward_heat_conduction
        elif args.what == "module":
            energy_budget = inward_heat_conduction

        data = pd.DataFrame({
            "temperature_slope"      : temperature_slope,
            "radiation_budget"       : radiation_budget,
            "inward_heat_conduction" : inward_heat_conduction,
            "energy_budget"          : energy_budget,
            })

        logging.info("fit {} data:\n{}...".format(netatmo_name,data))
        fit[netatmo_name] = sm.OLS(
            data["energy_budget"],
            data["temperature_slope"],
            ).fit()
        logging.info("{} fit summary:\n{}".format(netatmo_name,
            fit[netatmo_name].summary()))

        style = cyclestyle.copy()
        style.update({"linestyle":"None"})
        ax.plot(
            data["temperature_slope"],
            data["energy_budget"],
            label = r"{}".format(netatmo_name),
            **style
            )
        style = cyclestyle.copy()
        style.update({"marker":"None"})
        ax.plot(
            temperature_slope,
            fit[netatmo_name].predict(data["temperature_slope"]),
            label = r"fit ($C_s = \SI{{{:.2f}}}"
                "{{\joule\per\kelvin}}$)".format(
                float(fit[netatmo_name].params["temperature_slope"]),),
            **style
            )
        # make sure that (0,0) is within axes range
        ax.plot(0,0,linestyle="None",marker="None")
        ax.plot(
            {"module":0.001}.get(args.what,0),
            {}.get(args.what,0),
            linestyle="None",marker="None")

    ax.set_xlabel(r"{} temperature tendency [\si{{\kelvin\per\second}}]"\
        .format(args.what))
    ax.set_ylabel(r"{} energy budget [\si{{\joule\per\second}}]"\
        .format({"module":"sensor unit"}.get(args.what,args.what)))
    ax.legend()

if args.show:
    plt.show()

if args.result:
    if not args.type == "fit":
        raise ValueError("Writing result is only possible with --type='fit'")
    result = {}
    heatcaps = np.array([f.params["temperature_slope"] for n,f in fit.items()])
    heatcaperrors =np.array([f.bse["temperature_slope"] for n,f in fit.items()])
    result["{}_heat_capacity".format(args.what)] = heatcaps.mean()
    result["{}_heat_capacity_std".format(args.what)] = \
        np.sqrt(np.mean(heatcaperrors**2))
    result["{}_heat_capacity_rel_std".format(args.what)] = \
        result["{}_heat_capacity_std".format(args.what)] / \
        result["{}_heat_capacity".format(args.what)]
    result["{}_heat_capacity_rel_std_percent".format(args.what)] = \
        result["{}_heat_capacity_rel_std".format(args.what)] * 100
    result["{}_heat_capacity_smooth_seconds".format(args.what)] = args.smooth
    result["kt19_emissivity"] = kt19_emissivity
    result["aluminium_heat_capacity_literature"] = 900
    result["cover_heat_capacity_full_aluminium"] = \
        result["aluminium_heat_capacity_literature"] * weight["weight_casing"]
    with open(args.output,"w") as f:
        logging.info("Writing result to {}".format(args.output))
        json.dump(result,f,indent=4,sort_keys=True)
else:
    logging.info("Saving plot to {}".format(args.output))
    fig.savefig(args.output)
    plt.close("all")
