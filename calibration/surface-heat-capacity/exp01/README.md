# Experiment 1

21.09.2017 09:20 - 13:50 CEST

## Setup

- *09:20 CEST*: Heat up the modules in the oveo (set to $`60^\circ C`$)
- measure **room** temperature with **HMP** on the desk (device standing)
- *09:41 CEST*: set oven to $`50^\circ C`$. No risk :-)
- measure surface temperature with a **KT-19** with emissitity set to
  $`\epsilon=1`$ on the same laptop as the **HMP**
- *10:58 CEST*: take modules out of oven and place the modules on the desk and put back
  row (EPSILON,ZETA,ETA) top-down onto the first row (ALPHA,BETA,GAMMA,DELTA)
- wait for cooldown until *13:50 CEST*

## Problems

- took **BETA** out at *09:35 CEST* because it didn't measure
- outside, **BETA** started measuring again at *09:37 CEST*
- put **BETA** back inside at  *09:39 CEST*
- until *09:42 CEST*, the 2 oven holes were **unplugged**
- opened oven at *09:54 CEST* for a short time
- stopped HMP because of settings failure at around *10:55 CEST*
