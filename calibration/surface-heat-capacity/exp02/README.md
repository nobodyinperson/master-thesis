# Experiment 2

25.09.2017 15:20 - 26.09.2017 10:40 CEST

## Setup


- measure **room temperature**  with **HMP** on the desk (device standing)
- measure surface temperature of netatmo devices on the desk with a **KT-19**
  with emissitity set to $`\epsilon=1`$

- *25.09.2017 15:20 CEST*: Heat up all the modules in the oveo (set to $`60^\circ C`$)
- *25.09.2017 15:32 CEST*: set oven to $`70^\circ C`$ because warmup takes **AGES**...
- *25.09.2017 15:38 CEST*: set oven back to $`60^\circ C`$
- *25.09.2017 15:39 CEST*: take DELTA on the table 
- *25.09.2017 15:53 CEST*: set oven to $`50^\circ C`$
- *25.09.2017 16:12 CEST*: take ALPHA on the table, put DELTA back in oven
- *25.09.2017 16:50 CEST*: take BETA on the table, put ALPHA back in oven
- *25.09.2017 16:50 CEST*: take BETA off the table, put it back in oven
- *25.09.2017 16:50 CEST*: switch oven off
- *26.09.2017 16:50 CEST*: all modules in oven, oven set to $`70^\circ C`$ to
      speed up heating
- *26.09.2017 07:50 CEST*: put GAMMA on the table
- *26.09.2017 07:50 CEST*: take ALPHA, BETA and DELTA out of oven (for weighing)
- *26.09.2017 08:33 CEST*: put EPSILON on the table
- *26.09.2017 09:24 CEST*: put ZETA on the table
- *26.09.2017 10:05 CEST*: put ETA on the table and switch oven off
- *26.09.2017 10:40 CEST*: take ETA off the table and end experiment

## Problems

