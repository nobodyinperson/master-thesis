#!/usr/bin/make -f
git_root := $(shell git rev-parse --show-toplevel)
NETATMO_CREDENTIALS_SHELL_FILE:=$(git_root)/code/credentials/netatmo-credentials.sh
NETATMO_DEVICES_MAKE_FILE:=$(git_root)/code/credentials/netatmo-device-ids.mk

include $(NETATMO_DEVICES_MAKE_FILE)

ifeq ($(ONLY_NETATMO_DEVICES),)
# nothing
else
NETATMO_DEVICES := $(filter $(ONLY_NETATMO_DEVICES),$(NETATMO_DEVICES))
endif

date_to_unix = $(shell date +%s -d '$(1)')

ifeq ($(EXPERIMENT_START),)
EXPERIMENT_START:=$(shell date +'%F %T %z' -d '-10min')
$(warning EXPERIMENT_START set to $(EXPERIMENT_START))
endif
ifeq ($(EXPERIMENT_END),)
EXPERIMENT_END:=$(shell date +'%F %T %z')
$(warning EXPERIMENT_END set to $(EXPERIMENT_END))
endif

experiment_start_unix=$(call date_to_unix,$(EXPERIMENT_START))
experiment_end_unix=$(call date_to_unix,$(EXPERIMENT_END))

ifeq ($(experiment_start_unix),)
$(error Something went wrong converting time EXPERIMENT_START <<$(EXPERIMENT_START)>>)
endif
ifeq ($(experiment_end_unix),)
$(error Something went wrong converting time EXPERIMENT_END <<$(EXPERIMENT_END)>>)
endif


# make sure every device has a start/end time
$(foreach name,$(NETATMO_DEVICES),$(eval EXPERIMENT_START_$(name) = $(if $(EXPERIMENT_START_$(name)),$(EXPERIMENT_START_$(name)),$(EXPERIMENT_START))))
$(foreach name,$(NETATMO_DEVICES),$(eval EXPERIMENT_END_$(name)   = $(if $(EXPERIMENT_END_$(name)),$(EXPERIMENT_END_$(name)),  $(EXPERIMENT_END))  ))
# convert times to unix
$(foreach name,$(NETATMO_DEVICES),$(eval experiment_start_unix_$(name) = $(call date_to_unix,$(EXPERIMENT_START_$(name)))))
$(foreach name,$(NETATMO_DEVICES),$(eval experiment_end_unix_$(name)   = $(call date_to_unix,$(EXPERIMENT_END_$(name)))))

data_prefix=netatmo_
define data_rule
$(if $(NETATMO_OUTPUT_DIR),$(NETATMO_OUTPUT_DIR)/)$(data_prefix)$(1).csv: \
	| $(NETATMO_OUTPUT_DIR)
	. $(NETATMO_CREDENTIALS_SHELL_FILE) && $\\
		netatmo-getmeasure $\\
		--device '$(DEVICE_ID_$(1))' $\\
		--module '$(MODULE_ID_$(1))' $\\
		--begin '$(experiment_start_unix_$(1))' $\\
		--end '$(experiment_end_unix_$(1))' $\\
		--output '$$@'

all: $(if $(NETATMO_OUTPUT_DIR),$(NETATMO_OUTPUT_DIR)/)$(data_prefix)$(1).csv
endef


.PHONY: all
all: $(foreach name,$(NETATMO_DEVICES),$(if $(NETATMO_OUTPUT_DIR),$(NETATMO_OUTPUT_DIR)/)$(data_prefix)$(name).csv)

$(foreach name,$(NETATMO_DEVICES),$(eval $(call data_rule,$(name))))

ifeq ($(NETATMO_OUTPUT_DIR),)
else
$(NETATMO_OUTPUT_DIR):
	mkdir -p $@
endif

$(foreach rule,$(NETATMO_GET_POST_EVAL),$(eval $(call $(rule))))
