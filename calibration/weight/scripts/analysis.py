#!/usr/bin/env python3
# system modules
import argparse
import textwrap
import logging
import json

# external modules
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser(description = textwrap.dedent("""
    Weight determination
    """))

# Getmeasure arguments
parser.add_argument("-i","--input", help = "measurement data",
    required = True)
parser.add_argument("-o","--output",help = "output result file",
    required = True)

# parse the arguments
args = parser.parse_args()

logging.info("Read input file {}")
data = pd.read_csv(args.input)

result = {
        "weight_casing":     data["weight_casing"].mean(),
        "weight_casing_std": data["weight_casing"].std(),
        "weight_inner":      data["weight_inner"].mean(),
        "weight_inner_std":  data["weight_inner"].std(),
        "weight_total":      data["weight_total"].mean(),
        "weight_total_std":  data["weight_total"].std(),
        "weight_inconsistency": np.sqrt(np.sum((
            data["weight_casing"] + data["weight_inner"] - data["weight_total"]
            )**2)),
        }
for k,v in result.copy().items():
    result["{}_gram".format(k)] = v * 1000
logging.info("result:\n{}".format(json.dumps(result,indent=4,sort_keys=True)))

with open(args.output, "w") as f:
    logging.info("Writing results to file {}".format(args.output))
    json.dump(result,f,indent=4,sort_keys=True)
