#!/usr/bin/env python3
import argparse
import textwrap
import logging
import re,os
import json

# external modules
import pandas as pd
import numpy as np
import statsmodels.api as sm
from meteorology.temperature import cel2kel,kel2cel
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjust_emissivity
from meteorology.constants import stefan_boltzmann_constant, \
    gas_constant_dry_air, specific_heat_capacity_dry_air

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser(
    description = textwrap.dedent("""
        sensible heat flux parameter calibration
        """)
    )

# Getmeasure arguments
parser.add_argument("--kt19", help = "KT19 measurement data", required = True)
parser.add_argument("--hmp", help = "HMP measurement data", required = True)
parser.add_argument("--ptb", help = "PTB measurement data", required = True)
parser.add_argument("--wind", help = "Wind measurement data", required = True)
parser.add_argument("--indoor", help = "NETATMO indoor measurement data" )
parser.add_argument("--netatmo", help = "NETATMO measurement data", nargs="+")
parser.add_argument("--results", help = "results folder",
    default = os.path.join(os.path.dirname(__file__),"..","results"))
parser.add_argument("-t","--type",choices=["timeseries-reindexed",
    "timeseries-raw","fit"], help = "plot type", default = "fit")
parser.add_argument("-o","--output",help = "output plot file", required = True)
parser.add_argument("--show",help = "show the plot?", action = "store_true",
    default = False)
parser.add_argument("--result", help="write resultfile instead of plot",
    action="store_true")
parser.add_argument("--what",choices=["module","surface"],
    default="surface",help="fit the heat capacity for ...?")
parser.add_argument("--smooth",help = "smoothing window in seconds",
    default=60*5, type = int)

# parse the arguments
args = parser.parse_args()

import matplotlib
if not args.show:
    matplotlib.use("pdf")
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
plt.rcParams["font.size"] = 10
plt.rcParams["lines.markersize"] = 6
plt.rcParams["figure.figsize"] = [x*0.9 for x in (8, 5)]


logging.info("reading results from {}".format(args.results))
with open(os.path.join(args.results,"dimensions.json")) as f:
    dimensions = json.load(f)
with open(os.path.join(args.results,"heat-conduction-parameter.json")) as f:
    heat_conduction = json.load(f)
with open(os.path.join(args.results,"long-wave-emissivity.json")) as f:
    emissivity = json.load(f)
with open(os.path.join(args.results,"surface-heat-capacity.json")) as f:
    surface_heat_capacity = json.load(f)

def resample(x):
    return x.resample(
        "{}s".format(args.smooth),
        loffset="{}s".format(int(round(args.smooth/2))),
        )
def smooth(x):
    return resample(resample(x).bfill()).mean()

logging.info("read KT19 data {}".format(args.kt19))
kt19 = {}
kt19["raw"] = pd.read_csv(
    args.kt19,sep=";",parse_dates=["time"],index_col="time")
kt19["raw"] = kt19["raw"][~kt19["raw"].index.duplicated(keep="first")]
logging.info("smooth KT19 data...")
kt19["smoothed"] = smooth(kt19["raw"])

logging.info("read HMP data {}".format(args.hmp))
hmp = {}
hmp["raw"] = pd.read_csv(args.hmp,sep=";",parse_dates={"time":["DATE","TIME"]},
    index_col="time")
logging.info("smooth HMP data...")
hmp["smoothed"] = smooth(hmp["raw"])

logging.info("read indoor data {}".format(args.indoor))
indoor = {}
indoor["raw"] = pd.read_csv( args.indoor,sep=",",
    parse_dates=["time"], index_col="time")
logging.info("smooth indoor data...")
indoor["smoothed"] = smooth(indoor["raw"])

logging.info("read ptb data {}".format(args.ptb))
ptb = {}
ptb["raw"] = pd.read_csv(args.ptb,sep=";",parse_dates={"time":["DATE","TIME"]},
    index_col="time")
logging.info("smooth ptb data...")
ptb["smoothed"] = smooth(ptb["raw"])

logging.info("read wind data {}".format(args.wind))
wind = {}
wind["raw"] = pd.read_csv(args.wind,parse_dates=["time"],index_col="time")
logging.info("smooth wind data...")
wind["smoothed"] = smooth(wind["raw"])


netatmo = {}
netatmo["raw"] = {}
for f in args.netatmo:
    netatmo_name = re.sub(string=os.path.basename(f),
        pattern="^.*?_([A-Z]+).*$",repl="\g<1>")
    logging.info("read netatmo data {} ({})".format(f,netatmo_name))
    netatmo["raw"][netatmo_name] = pd.read_csv(
        f,sep=",",parse_dates=["time"], index_col="time")

logging.info("smoothing netatmo data...")
netatmo["smoothed"] = { n:smooth(v) for n,v in netatmo["raw"].items()}

# reindex
kt19["reindexed"] = {}
hmp["reindexed"] = {}
indoor["reindexed"] = {}
wind["reindexed"] = {}
ptb["reindexed"] = {}
for netatmo_name,netatmo_data in netatmo["smoothed"].items():
    kt19["reindexed"][netatmo_name] = kt19["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)
    hmp["reindexed"][netatmo_name]  = hmp["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)
    indoor["reindexed"][netatmo_name] = indoor["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)
    wind["reindexed"][netatmo_name] = wind["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)
    ptb["reindexed"][netatmo_name] = ptb["smoothed"].reindex(
        netatmo_data.index, method="nearest",copy=True)

for netatmo_name,netatmo_data in netatmo["smoothed"].items():
    kt19["reindexed"][netatmo_name]["surface_temperature"] = kel2cel(
        adjust_emissivity(
            T = cel2kel( kt19["reindexed"][netatmo_name]["temperature"] ),
            T_ambient = cel2kel( hmp["reindexed"][netatmo_name]["TT3"] ),
            emissivity_old = 1, # KT19 emissivity
            emissivity_new = emissivity["aluminium_emissivity"],
            )
        )

wind["median"] = {}
for netatmo_name,netatmo_data in netatmo["smoothed"].items():
    wind["median"][netatmo_name] = \
        wind["reindexed"][netatmo_name]["windspeed"].median()

plt.close("all")
fig = plt.figure()
ax = fig.add_subplot(111)

if args.type == "timeseries-raw":
    # Plot
    ax.set_title(r"raw data (v = $\SI{{{:.1f}}}{{\metre\per\second}}$)".format(
        np.median(list(wind["median"].values()))))
    ax.plot(hmp["raw"].index,hmp["raw"].TT3,label="HMP (raw)")
    ax.plot(hmp["smoothed"].index,hmp["smoothed"].TT3,label="HMP (smoothed)")
    ax.plot(kt19["raw"].index,kt19["raw"].temperature,label="KT19 (raw)")
    ax.plot(kt19["smoothed"].index,kt19["smoothed"]["temperature"],
        label="KT19 (smoothed)")
    ax.plot(indoor["raw"].index,indoor["raw"].Temperature,
        label="NETATMO indoor (raw)")
    ax.plot(indoor["smoothed"].index,indoor["smoothed"].Temperature,
        label="NETATMO indoor (smoothed)")
    for typ in ["raw","smoothed"]:
        for netatmo_name,netatmo_data in netatmo[typ].items():
            ax.plot(
                netatmo_data.index,
                netatmo_data.Temperature,
                label="{} ({})".format(netatmo_name,typ))
    ax.legend()
    # ax.get_xaxis().set_major_formatter(DateFormatter("%H:%M"))
    ax.set_xlabel(r"time [UTC]")
    ax.set_ylabel(r"temperature [\si{\celsius}]")
elif args.type == "timeseries-reindexed":
    ax.set_title(r"reindexed to NETATMO "
        "(v = $\SI{{{:.1f}}}{{\metre\per\second}}$)".format(
        np.median(list(wind["median"].values()))))
    for netatmo_name,netatmo_data in netatmo["smoothed"].items():
        # Plot
        ax.plot(
            hmp["reindexed"][netatmo_name].index,
            hmp["reindexed"][netatmo_name].TT3,
            label="HMP (smoothed+reindexed to {})".format(netatmo_name)
            )
        ax.plot(
            kt19["reindexed"][netatmo_name].index,
            kt19["reindexed"][netatmo_name]["temperature"],
            label="KT19 (smoothed+reindexed to {})".format(netatmo_name))
        ax.plot(
            kt19["reindexed"][netatmo_name].index,
            kt19["reindexed"][netatmo_name]["surface_temperature"],
            label="KT19 (smoothed+ambient+reindexed to {})".format(netatmo_name))
        ax.plot(
            indoor["reindexed"][netatmo_name].index,
            indoor["reindexed"][netatmo_name]["Temperature"],
            label="NETATMO indoor (smoothed+reindexed to {})".format(
                netatmo_name))
        ax.plot(
            netatmo_data.index,
            netatmo_data.Temperature,
            label="{} (smoothed)".format(netatmo_name))
    ax.legend()
    # ax.get_xaxis().set_major_formatter(DateFormatter("%H:%M"))
    ax.set_xlabel(r"time [UTC]")
    ax.set_ylabel(r"temperature [\si{\celsius}]")
elif args.type == "fit":
    # ax.set_title(r"heat flux parameter calibration "
    #     "(v = $\SI{{{:.1f}}}{{\metre\per\second}}$)".format(
    #     np.median(list(wind["median"].values()))))
    fit = {}
    for (netatmo_name,netatmo_data),cyclestyle \
        in zip(netatmo["smoothed"].items(),plt.rcParams["axes.prop_cycle"]):
        temperature_diff = np.diff(cel2kel(kt19["reindexed"][netatmo_name]\
            ["surface_temperature"]))
        time_diff = np.diff(kt19["reindexed"][netatmo_name]\
            ["surface_temperature"].index).astype(float)*1e-9
        temperature_slope = temperature_diff / time_diff
        radiation_budget =  \
            emissivity["aluminium_emissivity"] * stefan_boltzmann_constant \
            * (cel2kel(indoor["reindexed"][netatmo_name].Temperature[1:])**4 \
             - cel2kel(kt19["reindexed"][netatmo_name]\
                ["surface_temperature"][1:])**4 )
        inward_heat_conduction = \
                heat_conduction["heat_conduction_parameter"] / \
                dimensions["total_diameter"] \
                *(cel2kel(kt19["reindexed"][netatmo_name]\
                    ["surface_temperature"][1:]) \
                - cel2kel(netatmo_data.Temperature[1:]))
        pressure = ptb["reindexed"][netatmo_name]["B1"][1:] * 1e2
        density = pressure / ( gas_constant_dry_air * \
            cel2kel ( hmp["reindexed"][netatmo_name]["TT3"][1:] ) )
        heat_flux = - wind["median"][netatmo_name] * density * \
            specific_heat_capacity_dry_air * \
            (cel2kel(kt19["reindexed"][netatmo_name]\
                ["surface_temperature"][1:]) - \
             cel2kel(hmp["reindexed"][netatmo_name]["TT3"][1:]))
        # calm_heat_flux = 0.1 * density * \
        #     specific_heat_capacity_dry_air * \
        #     (cel2kel(kt19["reindexed"][netatmo_name]\
        #         ["surface_temperature"][1:]) - \
        #      cel2kel(hmp["reindexed"][netatmo_name]["TT3"][1:]))
        energy_budget = surface_heat_capacity["surface_heat_capacity"] * \
            temperature_slope - radiation_budget + inward_heat_conduction

        data = pd.DataFrame({
            "heat_flux"     : heat_flux,
            "energy_budget" : energy_budget # + calm_heat_flux,
            })

        logging.info("fit {} data:\n{}...".format(netatmo_name,data))
        fit[netatmo_name] = sm.OLS(
            data["energy_budget"],
            # sm.add_constant(data["heat_flux"]),
            data["heat_flux"],
            ).fit()
        logging.info("{} fit summary:\n{}".format(netatmo_name,
            fit[netatmo_name].summary()))

        style = cyclestyle.copy()
        style.update({"linestyle":"None"})
        ax.plot( data["heat_flux"],
            data["energy_budget"],
            label = r"{} ("
                r"$v\approx\SI[per-mode=symbol]{{{}}}{{\metre\per\second}}$, "
                r"$T_a\approx\SI{{{:.0f}}}{{\celsius}}$"
                ")"\
                .format(netatmo_name,
                    float(wind["median"][netatmo_name]),
                    float(hmp["reindexed"][netatmo_name]["TT3"].median()),
                    ),
            **style
            )
        style = cyclestyle.copy()
        style.update({"marker":"None"})
        ax.plot( data["heat_flux"],
            fit[netatmo_name].predict(),
            label = r"fit : $\eta = {:.9f}$".format(
                float(fit[netatmo_name].params["heat_flux"]),),
            **style
            )

    # make sure that (0,0) is within axes range
    ax.plot(0,0,linestyle="None",marker="None")
    ax.set_xlabel(r"raw sensible heat flux [\si{\joule\per\second}]")
    ax.set_ylabel(r"surface energy budget [\si{\joule\per\second}]")
    ax.legend()

if args.show:
    ax.show()

if args.result:
    if not args.type == "fit":
        raise ValueError("Writing result is only possible with --type='fit'")
    result = {}
    heatfluxparams = np.array([f.params["heat_flux"] for n,f in fit.items()])
    heatfluxparamserrors =np.array([f.bse["heat_flux"] for n,f in fit.items()])
    result["heat_flux_parameter"] = heatfluxparams.mean()
    result["heat_flux_parameter_std"] = np.sqrt(np.mean(
        heatfluxparamserrors**2))
    result["heat_flux_parameter_std_percent"] = \
        result["heat_flux_parameter_std"] / result["heat_flux_parameter"] * 100
    result["heat_flux_parameter_smooth_seconds"] = args.smooth
    with open(args.output,"w") as f:
        logging.info("Writing result to {}".format(args.output))
        json.dump(result,f,indent=4,sort_keys=True)
else:
    logging.info("Saving plot to {}".format(args.output))
    fig.savefig(args.output)
    plt.close("all")
