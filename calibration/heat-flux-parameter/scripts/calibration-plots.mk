#!/usr/bin/make -f

data_prefix = netatmo_
calib_prefix = heatflux_
timeseries_prefix = timeseries_
regress_prefix = regress_

$(INDOOR_FILE): \
	| $(NETATMO_OUTPUT_DIR)
	. $(NETATMO_CREDENTIALS_SHELL_FILE) && \
		netatmo-getmeasure \
		--device '$(DEVICE_ID_GAMMA)' \
		--begin '$(experiment_start_unix)' \
		--end '$(experiment_end_unix)' \
		--output '$@'

# $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)%.pdf: \
# 	$(NETATMO_OUTPUT_DIR)/$(data_prefix)%.csv \
# 	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
# 	$(CALIB_PLOTTER) \
# 	| $(PLOTS_DIR)
# 	$(CALIB_PLOTTER) \
# 		--kt19 $(KT19_FILE) \
# 		--hmp $(HMP_FILE) \
# 		--wind $(WIND_FILE) \
# 		--indoor $(INDOOR_FILE) \
# 		--ptb $(PTB_FILE) \
# 		--type 'timeseries-reindexed' \
# 		--netatmo $(firstword $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^)) \
# 		--smooth $(SMOOTH_SECONDS) \
# 		--output $@

# $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)raw_%.pdf: \
# 	$(NETATMO_OUTPUT_DIR)/$(data_prefix)%.csv \
# 	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
# 	$(CALIB_PLOTTER) \
# 	| $(PLOTS_DIR)
# 	$(CALIB_PLOTTER) \
# 		--kt19 $(KT19_FILE) \
# 		--hmp $(HMP_FILE) \
# 		--wind $(WIND_FILE) \
# 		--indoor $(INDOOR_FILE) \
# 		--ptb $(PTB_FILE) \
# 		--type 'timeseries-raw' \
# 		--netatmo $(firstword $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^)) \
# 		--smooth $(SMOOTH_SECONDS) \
# 		--output $@

$(PLOTS_DIR)/$(calib_prefix)$(regress_prefix)%.pdf: \
	$(NETATMO_OUTPUT_DIR)/$(data_prefix)%.csv \
	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
	$(CALIB_PLOTTER) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) \
		--kt19 $(KT19_FILE) \
		--hmp $(HMP_FILE) \
		--wind $(WIND_FILE) \
		--indoor $(INDOOR_FILE) \
		--ptb $(PTB_FILE) \
		--type 'fit' \
		--netatmo $(firstword $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^)) \
		--smooth $(SMOOTH_SECONDS) \
		--output $@

NETATMO_DATA_FILES = $(foreach name,$(NETATMO_DEVICES),$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(name).csv)
$(PLOTS_DIR)/$(calib_prefix)$(regress_prefix)all.pdf: \
	$(NETATMO_DATA_FILES) \
	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
	$(CALIB_PLOTTER) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) \
		--kt19 $(KT19_FILE) \
		--hmp $(HMP_FILE) \
		--wind $(WIND_FILE) \
		--indoor $(INDOOR_FILE) \
		--ptb $(PTB_FILE) \
		--type 'fit' \
		--netatmo $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^) \
		--smooth $(SMOOTH_SECONDS) \
		--output $@

# NETATMO_DATA_FILES = $(foreach name,$(NETATMO_DEVICES),$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(name).csv)
# $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)all.pdf: \
# 	$(NETATMO_DATA_FILES) \
# 	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
# 	$(CALIB_PLOTTER) \
# 	| $(PLOTS_DIR)
# 	$(CALIB_PLOTTER) \
# 		--kt19 $(KT19_FILE) \
# 		--hmp $(HMP_FILE) \
# 		--wind $(WIND_FILE) \
# 		--indoor $(INDOOR_FILE) \
# 		--ptb $(PTB_FILE) \
# 		--type 'timeseries-reindexed' \
# 		--netatmo $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^) \
# 		--smooth $(SMOOTH_SECONDS) \
# 		--output $@

# NETATMO_DATA_FILES = $(foreach name,$(NETATMO_DEVICES),$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(name).csv)
# $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)raw_all.pdf: \
# 	$(NETATMO_DATA_FILES) \
# 	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
# 	$(CALIB_PLOTTER) \
# 	| $(PLOTS_DIR)
# 	$(CALIB_PLOTTER) \
# 		--kt19 $(KT19_FILE) \
# 		--hmp $(HMP_FILE) \
# 		--wind $(WIND_FILE) \
# 		--indoor $(INDOOR_FILE) \
# 		--ptb $(PTB_FILE) \
# 		--type 'timeseries-raw' \
# 		--netatmo $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^) \
# 		--smooth $(SMOOTH_SECONDS) \
# 		--output $@

$(RESULTS_DIR)/heat-flux-parameter.json: \
	$(NETATMO_DATA_FILES) \
	$(KT19_FILE) $(HMP_FILE) $(INDOOR_FILE) $(WIND_FILE) $(PTB_FILE) \
	$(CALIB_PLOTTER) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) \
		--kt19 $(KT19_FILE) \
		--hmp $(HMP_FILE) \
		--wind $(WIND_FILE) \
		--indoor $(INDOOR_FILE) \
		--ptb $(PTB_FILE) \
		--type 'fit' \
		--netatmo $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^) \
		--result \
		--smooth $(SMOOTH_SECONDS) \
		--output $@

# all: $(foreach name,$(NETATMO_DEVICES),$(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)$(name).pdf)
# all: $(foreach name,$(NETATMO_DEVICES),$(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)raw_$(name).pdf)
# all: $(foreach name,$(NETATMO_DEVICES),$(PLOTS_DIR)/$(calib_prefix)$(regress_prefix)$(name).pdf)
all: $(PLOTS_DIR)/$(calib_prefix)$(regress_prefix)all.pdf
# all: $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)all.pdf
# all: $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)raw_all.pdf
all: $(RESULTS_DIR)/heat-flux-parameter.json

$(PLOTS_DIR):
	mkdir -p $@
