# Experiment 1

## Time

2017-09-27 10:46 - ..... CEST

## Setup

- 3 **hair dryers** (2 red small Bosch and 1 white large Ciatronic) pointing
  towards table to simulate different wind speeds and temperatures
- measure **module surface temperature** with **KT19**
- measure **wind temperature** with **HMP**
- measure **room temperature** with the **indoor modules**
- measure **pressure** with **PTB** (also measures temperature but seems to be
  inaccurate)
- measure **wind speed** with **Windmaster 2** (handheld, readings obtained
  manually)
- **10:55 CEST**: put all modules into oven ($`15^\circ C`$, higher than dew
  point) - top row ALPHA, BETA, GAMMA, bottom row DELTA, EPSILON, ZETA, ETA
- **10:19 CEST**: put ALPHA on the table with **no wind**
- **12:18 CEST**: put BETA on the table with **low neutral wind** (no warming)
  (white hair dryer level 1 wind)
- **12:35 CEST**: put GAMMA on the table with **higher neutral wind** (no
  warming) (white hair dryer level 2 wind)
- **13:10 CEST**: put DELTA on the table with **low slightly warmer wind**
  (both red hair dryers level 1). Problem: HMP was too far away... Has to be
  repeated...
- **13:41 CEST**: put EPSILON on the table with **low slightly warmer wind**
  (both red hair dryers level 1)
- **13:43 CEST**: Windmaster was too far away as well... Now HMP and Windmaster
  measure directly in front of the NETATMO module
- **14:13 CEST**: put ZETA on the table with **warmer wind**
  (both red hair dryers level 2)
- **14:50 CEST**: put ETA on the table with **high warmer wind**
  (both red hair dryers level 2, white hair dryer level 2 wind)
- **15:20 CEST**: put BETA on the table with **low slightly warm wind**
  (only right red hair dryer level 1)
- **15:50 CEST**: put DELTA on the table with **low neutral wind**
  (white hair dryer level 1)
- **16:10 CEST**: end of experiment

## Problems

- **12:22 CEST**: BETA (on the table) stops sending...
- **13:39 CEST**: HMP was too far away from the hair dryer wind flow
