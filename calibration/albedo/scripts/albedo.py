#!/usr/bin/env python3
# system modules
import logging
import argparse
import json

# external modules
import matplotlib
import numpy as np
from scipy.interpolate import interp1d

parser = argparse.ArgumentParser(
    description = "Albedo determination from images")

# Getmeasure arguments
parser.add_argument("-i","--image", help = "input image file", required = True)
parser.add_argument("-o","--output",help = "output file", required = True)
parser.add_argument("--result",help = "result output file",
    action = "store_true")
parser.add_argument("--netatmo_region",help = "netatmo region in image "
    "(bottom row,left col,top row,right col)", nargs = 4, type = int)
parser.add_argument("--hole_region",help = "hole region in image "
    "(bottom row,left col,top row,right col)", nargs = 4, type = int)
parser.add_argument("--paper_region",help = "paper region in image "
    "(bottom row,left col,top row,right col)", nargs = 4, type = int)
parser.add_argument("--hole_albedo",help = "hole albedo", type = float,
    default = 0)
parser.add_argument("--grayscale",help = "grayscale type", default = "mean",
    choices = ["mean","luma"])
parser.add_argument("--paper_albedo",help = "paper albedo", type = float,
    default = 0.65)
parser.add_argument("--show",help = "show the plot?", action = "store_true",
    default = False)

# parse the arguments
args = parser.parse_args()

if not args.show:
    matplotlib.use("agg")
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
plt.rcParams["axes.grid"] = False
plt.rcParams["font.size"] = 10
plt.rcParams["font.weight"] = "bold"
plt.rcParams["figure.figsize"] = [x*0.9 for x in (8, 5)]

image = plt.imread(args.image)
image = image[:,:,0:3] # drop alpha channel

def rect(ax,b,l,t,r,*args,**kwargs):
    ax.plot((l,l,r,r,l),(b,t,t,b,b),marker="None",*args,**kwargs)

def rgb2gray_mean(rgb):
    return np.apply_along_axis(np.mean, axis=2, arr=rgb)

def rgb2gray_luma(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

rgb2gray_dict = {"mean":rgb2gray_mean, "luma":rgb2gray_luma}
rgb2gray = rgb2gray_dict.get(args.grayscale, rgb2gray_mean)

regions = [args.hole_region, args.paper_region, args.netatmo_region]
region_colors = ["orange","black","cyan"]
region_linestyles = ["--",":","-"]
region_names = ["hole","paper","Netatmo"]

brightness = {}
for region,name in zip(regions,region_names):
    b,l,t,r = region
    regmat = image[t:b,l:r,:]
    brightness[name] = regmat.mean()

brightness2albedo = interp1d(
    x = [brightness["hole"],brightness["paper"]],
    y = [args.hole_albedo,args.paper_albedo],
    kind = "linear"
    )
albedo = {name:float(brightness2albedo(b)) for name,b in brightness.items()}


if args.result:
    result = {}
    result["aluminium_albedo"] = albedo["Netatmo"]
    result["paper_albedo"] = albedo["paper"]
    result["hole_albedo"] = albedo["hole"]
    with open(args.output,"w") as f:
        logging.info("Writing result to {}".format(args.output))
        json.dump(result,f,indent=4,sort_keys=True)
else:
    fig, ax = plt.subplots(1,1)

    ax.imshow(image)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    for region,color,linestyle,name in zip(
            regions,region_colors,region_linestyles,region_names):
        b,l,t,r = region
        rect(ax,b,l,t,r,
            label=r"{} region ($\alpha\approx{:.2f}$)".format(
                name,albedo.get(name)),
            linestyle=linestyle,
            color=color)
        regmat = image[t:b,l:r,:]
        m = np.mean(rgb2gray(regmat))
        ax.text(np.mean([l,r]),np.mean([t,b]),
            r"{:.1f}".format(float(m)), color=color,
            horizontalalignment="center", verticalalignment="center")

    ax.legend()

    if args.show:
        plt.show()

    fig.savefig(args.output)
