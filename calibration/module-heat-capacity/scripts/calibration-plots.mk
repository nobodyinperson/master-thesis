#!/usr/bin/make -f

data_prefix = netatmo_
calib_prefix = calib_
timeseries_prefix = timeseries_

ALL_PLOT=$(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)all.pdf
$(ALL_PLOT): | $(PLOTS_DIR) $(CALIB_PLOTTER)
	$(CALIB_PLOTTER) \
		--input $^ \
		--output $@

all: $(ALL_PLOT)

define calib_rule
$(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)%.pdf: \
	$(NETATMO_OUTPUT_DIR)/$(data_prefix)%.csv \
	| $(PLOTS_DIR) $(CALIB_PLOTTER)
	$(CALIB_PLOTTER) $\\
		--input $$< $\\
		--output $$@

$(ALL_PLOT): $(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv

all: $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)$(1).pdf
endef

# rules for all devices
$(foreach device,$(NETATMO_DEVICES),$(eval $(call calib_rule,$(device))))

$(PLOTS_DIR):
	mkdir -p $@
