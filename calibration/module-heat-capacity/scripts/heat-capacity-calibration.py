#!/usr/bin/env python3
# system modules
import argparse
import textwrap
import logging
import re,os,sys
from itertools import cycle

# external modules
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit

# fix matplotlib<->pandas bug in a version
try:
    import pandas.plotting._converter as pandacnv
    pandacnv.register()
except:
    pass
# end fix

import unicodedata
def greek(x):
    unicode_greek = "GREEK SMALL LETTER {}".format(str(x).upper())
    try:
        return unicodedata.lookup(unicode_greek)
    except KeyError:
        return unicode_greek

parser = argparse.ArgumentParser(
    description = textwrap.dedent("""
        Module heat capacity calibration
        """)
    )

# Getmeasure arguments
parser.add_argument("-i","--input", help = "measurement data",
    required = True,nargs="+")
parser.add_argument("-o","--output",help = "output plot file", required = True)
parser.add_argument("--show",help = "show the plot?", action = "store_true",
    default = False)

# parse the arguments
args = parser.parse_args()

import matplotlib
if not args.show:
    matplotlib.use("agg")
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, MinuteLocator
plt.rcParams["legend.fontsize"] = "xx-small"

# read data
data = {}
for f in args.input:
    logging.info("Reading measurement data '{}'...".format(f))
    name = re.sub(
        string=os.path.basename(f),pattern="^.*?_([A-Z]+).*$",repl="\g<1>")
    data[name] = pd.read_csv(f,parse_dates=["time"])

def T_expdecay(time,T_delta,T_end,tau):
    return T_end + T_delta * np.exp(-time/tau)

fit = {}
for name,df in data.items():
    logging.info("Determining time constant of {}".format(name))
    try:
        initial_guess = T_delta, T_end, tau
    except NameError:
        initial_guess = 20, 25, 30 * 60
    optimum,cov = curve_fit(
        f=T_expdecay, # fit this function
        xdata= (np.array(df.time)-np.array(df.time).min()).astype(float) * 1e-9,
        ydata=np.array(df["Temperature"]),
        p0 = initial_guess,
        )
    T_delta,T_end,tau = optimum
    fit[name] = tau

fig, ax = plt.subplots(1,1)

for name,df in sorted(data.items(),key=lambda x: greek(x[0])):
    # extract device name from filename
    ax.plot(
        df.time,
        df.Temperature,
        alpha=0.8,
        label=r"{name}"
            # "($"
            # "\SI{{{seconds:.0f}}}{{s}} \approx "
            # "\SI{{{minutes:.2f}}}{{min}}"
            # "$)"
            "".format(
            name = name, seconds = fit[name], minutes = fit[name] / 60,
            ),
        )

title = r""
tau_mean = np.mean(list(fit.values()))
tau_std  = np.std(list(fit.values()))
if len(args.input) == 1:
    title += "of device " + name
else:
    title += (r"mean time const.: $"
        # r"\SI{{{mean:.2f}}}{{\second}} "
        # r"\left("
        # r"\approx"
        r"\SI{{{mean_min:.2f}}}{{min}}"
        # "\right)"
        r"\pm \SI{{{std:.0f}}}{{\second}}"
        r"\left(\pm \SI{{{rel:.2f}}}{{\percent}}\right)"
        r"$"
        ).format(mean=tau_mean,mean_min=tau_mean/60,
            std=tau_std,rel=tau_std/tau_mean*100)
ax.set_title(title)
ax.legend(ncol=2)
ax.get_xaxis().set_major_formatter(DateFormatter("%H:%M"))
ax.get_xaxis().set_major_locator(
    MinuteLocator(byminute=range(0,60,10)))
ax.set_xlabel(r"time of day in UTC")
ax.set_ylabel(r"temperature in \si{\celsius}")

if args.show:
    plt.show()

logging.info("Saving plot to '{}'...".format(args.output))
fig.savefig(args.output)
plt.close("all")
