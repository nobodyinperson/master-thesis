# Experiment 1

## Time frame

- 29.08.2017 14:20 - 15:15 CEST 

## Setup

- E-Labor back room
- modules placed in oven **lying** on a standing grid (like in a microwave)
- oven set to $`80^\circ C`$
- module taken out as they reached $`40^\circ C`$
- modules placed with the standing grid on the floor (no ventilation)
- waited for cooling

