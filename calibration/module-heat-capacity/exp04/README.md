# Experiment 2

## Time frame

- 30.08.2017 12:28 - 13:37 CEST

## Setup

- E-Labor back room
- modules placed **without cover** in oven **lying** on a standing grid (like in a microwave)
- oven set to **cooling** and $`15^\circ C`$ (~dew point)
- oven set to $`0^\circ C`$ because otherwise it takes too long
- module taken out at 12:48 CEST as they cooled down to around $`2-5^\circ C`$
- modules placed with the standing grid on the floor (no ventilation)
- waited for cooling until 13:37 CEST

## Problems

- slight condensation/freezing at the surfaces
