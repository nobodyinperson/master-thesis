# Experiment 2

## Time frame

- 29.08.2017 15:15 - 16:50 CEST

## Setup

- E-Labor back room
- modules placed in oven **lying** on a standing grid (like in a microwave)
- oven set to **cooling** and $`0^\circ C`$
- module taken out at 15:39 CEST as they cooled to around $`5^\circ C`$
- there was slight condensation/freezing at the surfaces!
- modules placed with the standing grid on the floor (no ventilation)
- waited for warmup until 16:50 CEST

