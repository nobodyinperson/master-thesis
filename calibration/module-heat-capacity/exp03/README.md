# Experiment 3

## Time frame

- 30.08.2017 11:25 - 12:25 CEST

## Setup

- E-Labor back room
- modules placed **without cover** in oven **lying** on a standing grid (like in a microwave)
- oven set to **heating** and $`60^\circ C`$
- module taken out at 11:35 CEST as they heated up to around $`45^\circ C`$
- modules placed with the standing grid on the floor (no ventilation)
- waited for cooling until 12:25 CEST

## Problems

- ALPHA and DELTA did to not send from 11:05 to 11:40 CEST, obviously not
  enough reception. Doesn't matter because after 11:40 CEST is what's
  interesting.

