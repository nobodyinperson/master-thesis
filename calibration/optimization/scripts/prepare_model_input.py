#!/usr/bin/env python3
import argparse
import logging
import re
import json

from meteorology.temperature import cel2kel
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjust_emissivity
from meteorology.radiation import blackbody_radiation

import pandas as pd
import numpy as np

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description="Prepare model input")
parser.add_argument("--kt19_in",help="kt19 raw input file", required=True)
parser.add_argument("--kt19_out",help="kt19 CSV output file", required=True)
parser.add_argument("--hmp_in",help="hmp raw input file", required=True)
parser.add_argument("--hmp_out",help="hmp CSV output file", required=True)
parser.add_argument("--outdoor_in",help="outdoor raw input file",required=True)
parser.add_argument("--outdoor_out",help="outdoor output file",required=True)
parser.add_argument("--indoor_in",help="indoor raw input file", required=True)
parser.add_argument("--indoor_out",help="indoor CSV output file",required=True)
parser.add_argument("--wind_in",help="wind raw input file", required=True)
parser.add_argument("--wind_out",help="wind CSV output file", required=True)
parser.add_argument("--result_files",help="result files",
    nargs="+",required=True)

args = parser.parse_args()

parts = ("kt19","hmp","outdoor","indoor","wind")

reader_kwargs = {k:{"parse_dates":["time"],"index_col":"time"} for k in parts}
reader_kwargs["kt19"].update({"sep":";"})
reader_kwargs["hmp"].update({"sep":";","parse_dates":{"time":["DATE","TIME"]}})

translations = {k:{"seconds":"time"} for k in parts}


calibration = {}
for fn in args.result_files:
    with open(fn) as f:
        d = json.load(f)
        calibration.update(d)

data = {}

# input
for p,kwargs in reader_kwargs.items():
    f = getattr(args,"{}_in".format(p))
    logging.info("read {} data '{}'...".format(p.upper(),f))
    data[p] = pd.read_csv(f,**kwargs)

# drop nan
for p,d in data.items():
    logging.info("Dropping nans from {}...".format(p.upper()))
    d.dropna(axis="columns", how = "all", inplace = True)
    d.dropna(axis="index", how = "any", inplace = True)

# time to seconds
for p,d in data.items():
    logging.info("Determining {} seconds since start time {}...".format(
        p.upper(),data["outdoor"].index.min()))
    d["seconds"] = (d.index - data["outdoor"].index.min()).total_seconds()

start_time = data["outdoor"].index.min()
end_time = data["outdoor"].index.max()
# drop past and future data
for p,d in data.items():
    inside = (start_time<=d.index)&(d.index<=end_time)
    logging.info("Keeping {} {} data rows...".format(inside.sum(),p))
    data[p] = d[inside]

# temperature conversion
temperature_cols_re = (
    re.compile("^[TD]T\d+$"),
    re.compile("^temperature$",flags=re.IGNORECASE),
    re.compile("^T_.*$",flags=re.IGNORECASE),
    )
for p,d in data.items():
    for col in d.columns:
        if any(r.match(col) for r in temperature_cols_re):
            logging.info("Converting {} column '{}' "
                "from celsius to kelvin...".format(p.upper(),col))
            d[col] = cel2kel(d[col])

# pressure conversion
pressure_cols_re = (
    re.compile("^p(ressure)?$",flags=re.IGNORECASE),
    )
for p,d in data.items():
    for col in d.columns:
        if any(r.match(col) for r in pressure_cols_re):
            logging.info("Converting {} column '{}' "
                "from hPa to Pa...".format(p.upper(),col))
            d[col] = d[col] * 100

# humidity conversion
humidity_cols_re = (
    re.compile("^RH\d+$"),
    re.compile("^humidity$",flags=re.IGNORECASE),
    )
for p,d in data.items():
    for col in d.columns:
        if any(r.match(col) for r in humidity_cols_re):
            logging.info("Converting {} column '{}' "
                "from percent to ratio...".format(p.upper(),col))
            d[col] = d[col] / 100

# specific conversions
# kt19
logging.info("Converting KT19 temperature to surface temperature")
data["kt19"]["surface_temperature"] = adjust_emissivity(
    T = data["kt19"]["temperature"],
    emissivity_old = calibration["kt19_emissivity"],
    emissivity_new = calibration["aluminium_emissivity"],
    T_ambient = data["indoor"].Temperature\
        .reindex_like(data["kt19"]["temperature"], method="nearest")
    )
data["indoor"]["Temperature_BBR"] = \
    blackbody_radiation( data["indoor"]["Temperature"] )

# apply translations
for p,d in data.items():
    logging.info("Renaming {} columns: {}".format(p,translations.get(p)))
    d.rename(translations.get(p), axis = "columns", inplace = True)

# set index
for p,d in data.items():
    d.set_index("time", inplace = True)

# output
for p in parts:
    f = getattr(args,"{}_out".format(p))
    logging.info("write {} data to '{}'...".format(p.upper(),f))
    data[p].to_csv(f)
