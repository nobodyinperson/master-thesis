#!/usr/bin/env python3
import argparse
import logging
import json
import re

import numpy as np

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description="Mean end result")
parser.add_argument("jsonfiles",help="",nargs="+")
parser.add_argument("-o","--output",help="output plot file",required=True)

args = parser.parse_args()

JSON = {}
for f in args.jsonfiles:
    with open(f) as fh:
        JSON.update(json.load(fh))

types=set(re.sub(string=k,pattern="-[A-Z]+$",repl="") for k,v in JSON.items())
result = {}
for t in types:
    result[t] = np.mean([v for k,v in JSON.items() if k.startswith(t)])

with open(args.output,"w") as f:
    json.dump(result,f,indent=4,sort_keys=True)
