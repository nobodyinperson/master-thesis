#!/usr/bin/env python3
import argparse
import logging
import json

from meteorology.temperature import kel2cel

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description="Plot model results")
parser.add_argument("--optimized",help="optimized model CSV output file",
    required=True)
parser.add_argument("--unoptimized",help="unoptimized model CSV output file",
    required=True)
parser.add_argument("--result",help="result output JSON file",
    required=True)
parser.add_argument("--name",help="device name", required=True)
parser.add_argument("-o","--output",help="output plot file",required=True)

args = parser.parse_args()

logging.info("read data from '{}'...".format(args.optimized))
optimized = pd.read_csv(args.optimized,comment="#")
logging.info("read data from '{}'...".format(args.unoptimized))
unoptimized = pd.read_csv(args.unoptimized,comment="#")

logging.info("Converting data...")
for col in [c for c in optimized.columns if c.startswith("T_")]:
    optimized[col] = kel2cel(optimized[col])
for col in [c for c in unoptimized.columns if c.startswith("T_")]:
    unoptimized[col] = kel2cel(unoptimized[col])

logging.info("Plotting...")
plt.close("all")
plt.rcParams["font.size"] = 10
plt.rcParams["figure.figsize"] = [x*0.9 for x in (8, 5)]
fig = plt.figure()
ax = fig.add_subplot(111)

def update_dict_otf(d,u):
    d2 = d.copy()
    d2.update(u)
    return d2

def rmse(x,y):
    return np.sqrt(np.mean((x-y)**2))

cyclestyle = iter(plt.rcParams["axes.prop_cycle"])

current_cyclestyle = next(cyclestyle)
style = current_cyclestyle.copy()
style.update({"marker":"None"})
style.pop("color")

ax.plot(optimized["time"],optimized["T_s_obs"],
    label="measured cover temp. (KT19)",
    **update_dict_otf(style,{"linestyle":"-","linewidth":2}))
ax.plot(unoptimized["time"],unoptimized["T_s"],
    label=r"sim. cover temp. "
        r"(\textit{{unoptimized}}) "
        r"$\text{{RMSE}}\approx\SI[round-precision=2,round-mode=places]"
            r"{{{}}}{{\kelvin}}$ " \
        .format(rmse(unoptimized["T_s"],unoptimized["T_s_obs"])),
    **update_dict_otf(style,{"linestyle":(0,(1,1))}))
ax.plot(optimized["time"],optimized["T_s"],
    label=r"sim. cover temp. "
        r"(\textit{{optimized}}) "
        r"$\text{{RMSE}}\approx\SI[round-precision=2,"
            r"round-mode=places]{{{}}}{{\kelvin}}$ " \
        .format(rmse(optimized["T_s"],optimized["T_s_obs"])),
    **update_dict_otf(style,{"linestyle":"--"}))

current_cyclestyle = next(cyclestyle)
style = current_cyclestyle.copy()
style.update({"marker":"None"})
style.pop("color")

ax.plot(optimized["time"],optimized["T_m_obs"],
    label="measured sensor temp. (Netatmo)",
    **update_dict_otf(style,{"linestyle":"-","linewidth":2}))
ax.plot(unoptimized["time"],unoptimized["T_m"],
    label=r"sim. sensor temp. "
        r"(\textit{{unoptimized}}) "
        r"$\text{{RMSE}}\approx\SI[round-precision=2,"
            r"round-mode=places]{{{}}}{{\kelvin}}$ " \
        .format(rmse(unoptimized["T_m"],unoptimized["T_m_obs"])),
    **update_dict_otf(style,{"linestyle":(0,(1,1))}))
ax.plot(optimized["time"],optimized["T_m"],
    label=r"sim. sensor temp. "
        r"(\textit{{optimized}}) "
        r"$\text{{RMSE}}\approx"
        r"\SI[round-precision=2,round-mode=places]{{{}}}{{\kelvin}}$ " \
        .format(rmse(optimized["T_m"],optimized["T_m_obs"])),
    **update_dict_otf(style,{"linestyle":"--"}))
ax.legend()

ax.set_xlabel(r"time in seconds")
ax.set_ylabel(r"temperature in \si{\celsius}")

logging.info("Saving result to file '{}'".format(args.result))
with open(args.result,"w") as f:
    result = {}
    result["optimized-surface-heat-capacity-{}".format(args.name)] = \
        float(optimized["C_s"].median())
    result["optimized-module-heat-capacity-{}".format(args.name)] = \
        float(optimized["C_m"].median())
    result["optimized-heat-conduction-parameter-{}".format(args.name)] = \
        float(optimized["lambda"].median())
    result["optimized-heat-flux-parameter-{}".format(args.name)] = \
        float(optimized["eta"].median())
    json.dump(result,f,indent=4,sort_keys=True)

logging.info("Saving plot to '{}'...".format(args.output))
fig.savefig(args.output)
