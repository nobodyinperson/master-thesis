#!/usr/bin/env python3
import json
import argparse
import pandas as pd

def key_equals_val(arg):
    try:
        oldname,newname = arg.split("=")
        return (oldname,newname)
    except:
        raise argparse.ArgumentTypeError(
            "expected 'NAME=NEWNAME', got '{}'".format(arg))

parser = argparse.ArgumentParser(description="Convert JSON to CSV")
parser.add_argument("-i","--input",help="input JSON files",nargs="+")
parser.add_argument("-o","--output",help="output CSV file", required = True)
parser.add_argument("--only",help="only what JSON names to include "
    "(before translation)? Specify just this to use only the "
    "names from --translate",nargs="*", default=False,type=str)
parser.add_argument("--translate",help="translate JSON names to CSV names",
    nargs="+",type=key_equals_val, default=[])
args = parser.parse_args()

TRANSLATION = dict(args.translate)

JSON = {}
for fn in args.input:
    with open(fn) as f:
        d = json.load(f)
        JSON.update(d)

ONLY = args.only if hasattr(args.only,"__iter__") and args.only else \
    (TRANSLATION.keys() if args.translate and not args.only is False \
        else JSON.keys())

JSON = {k:v for k,v in JSON.items() if k in ONLY}

JSON = {TRANSLATION.get(k,k):v for k,v in JSON.items()}

DATAFRAME = pd.DataFrame.from_records([JSON])

DATAFRAME.to_csv(args.output)
