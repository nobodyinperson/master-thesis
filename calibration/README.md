# Calibration

## Measuring Laptop in E-Labor

# Program for PTB pressure measurement

`D:/Wettermast/Programme/USATBoe/USATBoe.exe` with `PTB200.opt`
settings file

# Program for HMP temperature/humidity measurement

`D:/Wettermast/Programme/WMDataLog/WMDataLog.exe` with `HMP_33_155.opt`
settings file
