#!/usr/bin/env python3
import argparse
import re, os
import logging
import json

# external modules
import pandas as pd
import numpy as np
from meteorology.temperature import cel2kel,kel2cel
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjust_emissivity

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser("surface heat capacity calibration")

# Getmeasure arguments
parser.add_argument("--kt19", help = "KT19 measurement data", required = True)
parser.add_argument("--indoor", help = "NETATMO indoor measurement data",
    required = True)
parser.add_argument("--netatmo", help = "NETATMO measurement data", nargs="+")
parser.add_argument("--results", help = "results folder",
    default = os.path.join(os.path.dirname(__file__),"..","results"))
parser.add_argument("-o","--output",help = "output plot file", required = True)
parser.add_argument("--result", help="write resultfile instead of plot",
    action="store_true")

# parse the arguments
args = parser.parse_args()

from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (8,5)
plt.rcParams["legend.fontsize"] = "xx-small"

logging.info("read KT19 data {}".format(args.kt19))
kt19 = {}
kt19["raw"] = pd.read_csv(args.kt19,sep=";",parse_dates=["time"],
    index_col="time")
kt19["raw"] = kt19["raw"][~kt19["raw"].index.duplicated(keep="first")]
kt19["raw"].temperature = cel2kel(kt19["raw"].temperature)

logging.info("read indoor data {}".format(args.indoor))
indoor = {}
indoor["raw"] = pd.read_csv(args.indoor,sep=",",parse_dates=["time"],
    index_col="time")
indoor["raw"].Temperature = cel2kel(indoor["raw"].Temperature)

netatmo = {}
for f in args.netatmo:
    netatmo_name = re.sub(string=os.path.basename(f),
        pattern="^.*?_([A-Z]+).*$",repl="\g<1>")
    logging.info("read netatmo data {} ({})".format(f,netatmo_name))
    netatmo[netatmo_name] = pd.read_csv(
        f,sep=",",parse_dates=["time"], index_col="time")
    netatmo[netatmo_name].Temperature = \
        cel2kel(netatmo[netatmo_name].Temperature)

# reindex
kt19["reindexed"] = {}
indoor["reindexed"] = {}
for netatmo_name,netatmo_data in netatmo.items():
    kt19["reindexed"][netatmo_name] = kt19["raw"].reindex(
        netatmo_data.index, method="nearest",copy=True)
    indoor["reindexed"][netatmo_name] = indoor["raw"].reindex(
        netatmo_data.index, method="nearest",copy=True)

fig = plt.figure()
ax = fig.add_subplot(211)

def calc_emissivity(eps_rad,T_rad,T_a,T_s):
    return (eps_rad * T_rad**4 - T_a**4) / (T_s**4 - T_a**4)

def calc_emissivity_max_error(eps_rad,T_rad,T_a,T_s,dT_rad,dT_a,dT_s):
    eps_mean = calc_emissivity(eps_rad=eps_rad, T_rad=T_rad, T_a=T_a, T_s=T_s)
    p_T_s = + dT_s   * eps_mean * T_s**3
    p_T_a = + dT_a   * (1-eps_mean) * T_a**3
    p_T_rad = + dT_rad * eps_rad  * T_rad**3
    logging.debug("  error part T_s: {}".format(p_T_s))
    logging.debug("  error part T_a: {}".format(p_T_a))
    logging.debug("error part T_rad: {}".format(p_T_rad))
    return 4/abs(T_s**4 - T_a**4) * ( p_T_s + p_T_rad + p_T_a )

emissivity = {}
for netatmo_name,netatmo_data in netatmo.items():
    emissivity[netatmo_name] = calc_emissivity(
        eps_rad = 1,
        T_rad = kt19["reindexed"][netatmo_name].temperature,
        T_a = indoor["reindexed"][netatmo_name].Temperature,
        T_s = netatmo_data.Temperature,
        )

total_emissivity = calc_emissivity(
    eps_rad = 1,
    T_rad = float(kt19["reindexed"][netatmo_name].temperature.head(1)),
    T_a = float(indoor["reindexed"][netatmo_name].Temperature.head(1)),
    T_s = np.mean([float(d.Temperature.head(1)) for n,d in netatmo.items()]),
    )

dT_rad = 0.5
dT_a = 2
dT_s = 2
total_emissivity_max_error = calc_emissivity_max_error(
    eps_rad = 1,
    T_rad = float(kt19["reindexed"][netatmo_name].temperature.head(1)),
    T_a = float(indoor["reindexed"][netatmo_name].Temperature.head(1)),
    T_s = np.mean([float(d.Temperature.head(1)) for n,d in netatmo.items()]),
    dT_rad = dT_rad,
    dT_a = dT_a,
    dT_s = dT_s,
    )

ax.plot(kt19["raw"].index,kt19["raw"].temperature, label="KT19")
ax.plot(indoor["raw"].index,indoor["raw"].Temperature, label="indoor")
for netatmo_name,netatmo_data in netatmo.items():
    ax.plot(netatmo_data.index, netatmo_data.Temperature, label=netatmo_name)

ax.legend()

ax = fig.add_subplot(212)

for netatmo_name,netatmo_data in netatmo.items():
    ax.plot(emissivity[netatmo_name].index, emissivity[netatmo_name],
        label=netatmo_name)

ax.legend()

if args.result:
    result = {}
    result["aluminium_emissivity"] = total_emissivity
    result["aluminium_emissivity_max_error"] = total_emissivity_max_error
    result["aluminium_emissivity_rel_error"] = \
        total_emissivity_max_error / total_emissivity
    result["aluminium_emissivity_rel_error_percent"] = \
        total_emissivity_max_error / total_emissivity * 100
    result["aluminium_emissivity_radiation_temperature_max_error"] = dT_rad
    result["aluminium_emissivity_air_temperature_max_error"] = dT_a
    result["aluminium_emissivity_surface_temperature_max_error"] = dT_s
    result["aluminium_emissivity_max_heatup_celsius"] = \
        float(kel2cel(kt19["raw"].max()))
    with open(args.output,"w") as f:
        s = json.dumps(result,indent=4,sort_keys=True)
        logging.info("Writing result \n{}\n to {}".format(s,args.output))
        json.dump(result,f,indent=4,sort_keys=True)
else:
    logging.info("Saving plot to {}".format(args.output))
    fig.savefig(args.output)
