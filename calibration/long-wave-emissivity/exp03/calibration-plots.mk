
$(INDOOR_FILE):
	. $(NETATMO_CREDENTIALS_SHELL_FILE) && \
		netatmo-getmeasure \
		--device '$(DEVICE_ID_ALPHA)' \
		--begin '$(experiment_start_unix)' \
		--end '$(experiment_end_unix)' \
		--output '$@'

data_prefix = netatmo_
calib_prefix = emiss_

$(PLOTS_DIR)/$(calib_prefix)%.pdf: \
	$(NETATMO_OUTPUT_DIR)/$(data_prefix)%.csv $(KT19_FILE) $(INDOOR_FILE) \
	$(CALIB_PLOTTER) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) \
		--kt19 $(KT19_FILE) \
		--netatmo $(firstword $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^)) \
		--indoor $(INDOOR_FILE) \
		--output $@

NETATMO_DATA_FILES = $(foreach name,$(NETATMO_DEVICES),$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(name).csv)
$(PLOTS_DIR)/$(calib_prefix)all.pdf: \
	$(NETATMO_DATA_FILES) $(KT19_FILE) $(INDOOR_FILE) \
	$(CALIB_PLOTTER) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) \
		--kt19 $(KT19_FILE) \
		--netatmo $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^) \
		--indoor $(INDOOR_FILE) \
		--output $@

$(RESULT_FILE): \
	$(NETATMO_DATA_FILES) $(KT19_FILE) $(INDOOR_FILE) \
	$(CALIB_PLOTTER) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) \
		--kt19 $(KT19_FILE) \
		--netatmo $(filter $(NETATMO_OUTPUT_DIR)/$(data_prefix)%,$^) \
		--indoor $(INDOOR_FILE) \
		--result \
		--output $@

$(PLOTS_DIR):
	mkdir -p $@

all: $(foreach name,$(NETATMO_DEVICES),$(PLOTS_DIR)/$(calib_prefix)$(name).pdf)
all: $(PLOTS_DIR)/$(calib_prefix)all.pdf
all: $(RESULT_FILE)
