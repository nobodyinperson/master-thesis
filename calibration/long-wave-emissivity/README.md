# Emissivity calibration

It turns out, emissivity of the aluminum cover is around $`\epsilon = 0.85 \pm 3\%`$
