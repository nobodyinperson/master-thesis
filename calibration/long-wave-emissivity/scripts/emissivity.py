#!/usr/bin/env python3
import numpy as np
import pandas as pd
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjtemp
from meteorology.temperature import cel2kel

import matplotlib.pyplot as plt

# read data
data = pd.read_csv("data.csv")

for c in data.columns:
    data[c] = cel2kel(data[c])

def emissivity(T_room):
    IR_EMISSIVITY = 0.95 # emissivity of infrared thermometer

    return np.mean((IR_EMISSIVITY * data["T.IR"]**4 - T_room**4) \
        / (data["T.HMP"]**4 - T_room**4))

T_room = cel2kel(np.linspace(0,40,100))
emissivities = np.array([emissivity(t) for t in T_room])


plt.plot(T_room,emissivities,label="emissivity")
plt.plot(T_room,np.mean(data["T.HMP"]**4)-T_room**4,label="difference")
plt.axhline(np.median(emissivities),label="median")
plt.show()

# print(data)

# print("mean emissivity: {}".format(float(data["EPS"].mean())))
# print("emissivity std: {}".format(float(data["EPS"].std())))
# print("mean emissivity std: {}".format(
#     float(data["EPS"].std()/np.sqrt(data["EPS"].size))))
# print("median emissivity: {}".format(float(data["EPS"].median())))
# print("mean emissivity rel error: {}".format(
#     float(data["EPS"].std()/np.sqrt(data["EPS"].size/data["EPS"].mean()))))

