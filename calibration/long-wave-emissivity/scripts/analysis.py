#!/usr/bin/env python3
import json
import numpy as np
import pandas as pd
from meteorology.radiation import \
    adjust_radiation_temperature_to_other_emissivity as adjtemp
from meteorology.temperature import cel2kel

# read data
aluminum = pd.read_csv("exp01/data.csv")
plastic  = pd.read_csv("exp02/data.csv")

for data in [aluminum,plastic]:
    for c in data.columns:
        data[c] = cel2kel(data[c])

with open("results/dimensions.json") as f:
    dimensions = json.load(f)

IR_EMISSIVITY = 0.95 # emissivity of infrared thermometer

aluminum["T.BB"] = adjtemp(T=aluminum["T.IR"],emissivity_old=IR_EMISSIVITY,
    emissivity_new=1,T_ambient=aluminum["T.HMP"])
aluminum["EPS"]= (aluminum["T.BB"] / aluminum["T.HMP"]) ** 4
aluminum_epsilon = aluminum["EPS"].mean()

print("aluminum epsilon: {}".format(float(aluminum_epsilon)))

plastic["T.BB"] = adjtemp(T=plastic["T.IR"],emissivity_old=IR_EMISSIVITY,
    emissivity_new=1,T_ambient=plastic["T.HMP"])
plastic["EPS"]= (plastic["T.BB"] / plastic["T.HMP"]) ** 4
plastic_epsilon = plastic["EPS"].mean()

print("plastic epsilon: {}".format(float(plastic_epsilon)))

diameter_area = lambda d: np.pi * (d/2)**2
diameter_perimeter = lambda d: np.pi * d


top_area = diameter_area(dimensions["total_diameter"])
aluminum_area = diameter_perimeter(dimensions["total_diameter"]) \
    * dimensions["aluminum_cover_height"]
bottom_side_area = diameter_perimeter(dimensions["total_diameter"]) \
    * (dimensions["total_height"] - dimensions["aluminum_cover_height"])
plastic_area = top_area + bottom_side_area
total_area = diameter_perimeter(dimensions["total_diameter"]) \
    * dimensions["total_height"] + top_area

print("top area: {}".format(float(top_area)))
print("aluminum area: {}".format(float(aluminum_area)))
print("bottom side area: {}".format(float(bottom_side_area)))
print("plastic area: {}".format(float(plastic_area)))
print("total area: {}".format(float(total_area)))
print("aluminum&plastic area: {}".format(float(aluminum_area+plastic_area)))

assert total_area == aluminum_area + plastic_area

weighted_emissivity = (plastic_area * plastic["EPS"].mean() \
    + aluminum_area * aluminum["EPS"].mean()) / total_area
weighted_emissivity_std = np.sqrt((plastic_area * plastic["EPS"].std())**2 \
    + (aluminum_area * aluminum["EPS"].std())**2) / total_area

print("weighted emissivity: {}".format(float(weighted_emissivity)))

with open("results/long-wave-emissivity.json","w") as f:
    json.dump({
        "aluminum_emissivity":aluminum["EPS"].mean(),
        "aluminum_emissivity_std":aluminum["EPS"].std(),
        "plastic_emissivity":plastic["EPS"].mean(),
        "plastic_emissivity_std":plastic["EPS"].std(),
        "weighted_emissivity":weighted_emissivity,
        "weighted_emissivity_std":weighted_emissivity_std,
        },f,sort_keys=True,indent=4)
