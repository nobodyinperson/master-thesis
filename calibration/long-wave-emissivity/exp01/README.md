# Experiment 1 - aluminum cover

31.08.2017 11:30 - 13:45 CEST

## Setup

- all module shells places in a row before a carton
- the reference HMP temperature device on top
- wait until stationarity in HMP temperature
- measure radiation temperature ($`\epsilon = 0.95`$) with TFA Scantemp 380
- measured multiple times, sometimes over the ventilation grid, sometimes on the
  table, etc...
