# Experiment 1 - plastic top

20.09.2017 11:15 - ..... CEST

## Setup

- all module shells placed in a dense circle
- the reference HMP temperature device on top
- wait until stationarity in HMP temperature
- measure radiation temperature ($`\epsilon = 0.95`$) with TFA Scantemp 380
- measured multiple times, sometimes over the ventilation grid, sometimes on the
  table, etc...
