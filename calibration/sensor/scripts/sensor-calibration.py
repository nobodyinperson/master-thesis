#!/usr/bin/env python3
# system modules
import argparse
import textwrap
import logging
import re
import os

# external modules
import pandas as pd
import numpy as np

# fix matplotlib<->pandas bug in a version
try:
    import pandas.plotting._converter as pandacnv
    pandacnv.register()
except:
    pass
# end fix

parser = argparse.ArgumentParser(
    description = textwrap.dedent("""
        Analyse sensor calibration data
        """)
    )

# Getmeasure arguments
parser.add_argument("--reference", help = "reference measurement data",
    required = True)
parser.add_argument("--measurement", help = "measurement data",
    required = True, nargs = "+")
parser.add_argument("--type", help = "plot type",
    choices = ["timeseries", "scatter","diff"], default = "scatter", required = False)
parser.add_argument("-o","--output",help = "output plot file", required = True)
parser.add_argument("--accuracy",help = "device accuracy as of manufacturer",
    required = False, type = float, default = 0.3)
parser.add_argument("--show",help = "show the plot?", action = "store_true",
    default = False)

# parse the arguments
args = parser.parse_args()

import matplotlib
if not args.show:
    matplotlib.use("agg")
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, HourLocator
plt.rcParams["legend.fontsize"] = "xx-small"

import unicodedata
def greek(x):
    unicode_greek = "GREEK SMALL LETTER {}".format(str(x).upper())
    try:
        return unicodedata.lookup(unicode_greek)
    except KeyError:
        return unicode_greek

# read data
data = {}
for f in args.measurement:
    logging.info("Reading measurement data '{}'...".format(f))
    name = re.sub(
        string=os.path.basename(f),pattern="^.*?_([A-Z]+).*$",repl="\g<1>")
    df = pd.read_csv(f)
    df["time"] = pd.to_datetime(df["time"],
        format = "%Y-%m-%d %H:%M:%S", utc=True)
    df.set_index(df["time"], inplace = True)
    data[name] = df

logging.info("Reading reference data '{}'...".format(args.reference))
reference = pd.read_csv(args.reference, delimiter=";")
logging.info("Converting reference time...")
reference["time"] = pd.to_datetime(reference["DATE"]+" "+reference["TIME"],
    format = "%d.%m.%Y %H:%M:%S", utc = True)
reference.set_index(reference["time"], inplace = True)

fig, ax = plt.subplots(1,1)

accuracy_kwargs = {
    "color":"black","zorder":1,"alpha":0.8,"linestyle":"--","marker":"None"}
###################
### Scatterplot ###
###################
if args.type == "scatter":
    for name,measurement in sorted(data.items(),key=lambda x: greek(x[0])):
        resolution = lambda x: np.median(np.diff(x))
        if resolution(reference["time"]) < resolution(measurement["time"]):
            logging.info("Reindexing reference to measurement time...")
            reference = reference.reindex(measurement["time"], method = "nearest")
        else:
            logging.info("Reindexing measurement to reference time...")
            measurement = measurement.reindex(reference["time"], method = "nearest")
        roundto = lambda x,r: np.round(x/r)*r
        low,high = \
            roundto(np.min([measurement["Temperature"],reference["TT3"]])-2.5,5), \
            roundto(np.max([measurement["Temperature"],reference["TT3"]])+2.5,5)


        ax.plot(
            reference.TT3,# x
            measurement.Temperature, # y
            zorder = 2,
            label = name,
            # linestyle = "none",
            )
    # optimum line
    ax.plot([low,high],[low,high], color = "red",zorder=3, alpha = 1,
        label = "identity",)
    ax.plot([low,high],[low-args.accuracy,high-args.accuracy],
        label=r"accuracy $\left(\pm\SI{{{}}}{{\kelvin}})$"\
            .format(args.accuracy),
        **accuracy_kwargs)
    ax.plot([low,high],[low+args.accuracy,high+args.accuracy],
        **accuracy_kwargs)

    # ax.set_title(r"calibration of against Reference")
    ax.set_xlabel(r"reference temperature [\si{\celsius}]")
    ax.set_ylabel(r"measured temperature [\si{\celsius}]")
##################
### Timeseries ###
##################
elif args.type == "timeseries":
    for name, measurement in sorted(data.items(),key=lambda x: greek(x[0])):
        ax.plot(measurement["time"],measurement["Temperature"],
            label=name,
            zorder=2,
            # linewidth=3,
            marker="None")
    ax.plot(reference["time"],reference["TT3"],
        label=r"Reference",zorder=1,
        # linewidth=2,
        marker="None"
        )
    ax.get_xaxis().set_major_locator(HourLocator(byhour=range(0,24,2)))
    ax.get_xaxis().set_major_formatter(DateFormatter("%H"))
    # ax.set_title(r"calibration against Reference")
    ax.set_xlabel(r"hour of day in UTC")
    ax.set_ylabel(r"temperature in \si{\celsius}")
############
### Diff ###
############
elif args.type == "diff":
    rmse = {}
    for name, measurement in sorted(data.items(),key=lambda x: greek(x[0])):
        resolution = lambda x: np.median(np.diff(x))
        if resolution(reference["time"]) < resolution(measurement["time"]):
            logging.info("Reindexing reference to measurement time...")
            reference = reference.reindex(measurement["time"],
                method = "nearest")
        else:
            logging.info("Reindexing measurement to reference time...")
            measurement = measurement.reindex(reference["time"],
                method = "nearest")
        rmse[name] = np.sqrt(np.mean((
            measurement["Temperature"]-reference["TT3"])**2))
        ax.plot(
            reference["time"], # x
            measurement["Temperature"].values-reference["TT3"].values, # y
            label=name,zorder=1,
            linestyle = 'None',
            alpha = 0.8,
            )
    # accuracy
    ax.axhline(args.accuracy,
        label=r"accuracy $\left(\pm\SI{{{}}}{{\kelvin}})$"\
            .format(args.accuracy), **accuracy_kwargs)
    ax.axhline(-args.accuracy, **accuracy_kwargs)
    ax.get_xaxis().set_major_locator(HourLocator(byhour=range(0,24,2)))
    ax.get_xaxis().set_major_formatter(DateFormatter("%H"))
    ax.set_title(r"mean RMSE Netatmo/Reference: \SI{{{:.2f}}}{{\kelvin}}"\
        .format(np.mean([v for k,v in rmse.items()])))
    ax.set_xlabel(r"hour of day in UTC")
    ax.set_ylabel(r"temperature difference in \si{\kelvin}")
else:
    raise ValueError("Unknown plot type '{}'".format(args.type))

ax.legend(ncol=2)

if args.show:
    plt.show()

logging.info("Saving plot to '{}'...".format(args.output))
fig.savefig(args.output)
plt.close("all")
