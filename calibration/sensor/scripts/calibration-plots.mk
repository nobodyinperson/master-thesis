#!/usr/bin/make -f

calib_prefix = calib_
scatter_prefix = scatter_
timeseries_prefix = timeseries_
diff_prefix = diff_

ALL_TS_PLOT=$(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)all.pdf
ALL_DIFF_PLOT=$(PLOTS_DIR)/$(calib_prefix)$(diff_prefix)all.pdf
ALL_SCATTER_PLOT=$(PLOTS_DIR)/$(calib_prefix)$(scatter_prefix)all.pdf

$(ALL_TS_PLOT): | $(PLOTS_DIR) $(CALIB_PLOTTER)
	$(CALIB_PLOTTER) \
		--reference $(REFERENCE_FILE) \
		--measurement $^ \
		--type "timeseries" \
		--output $@

$(ALL_SCATTER_PLOT): | $(PLOTS_DIR) $(CALIB_PLOTTER)
	$(CALIB_PLOTTER) \
		--reference $(REFERENCE_FILE) \
		--measurement $^ \
		--type "scatter" \
		--output $@

$(ALL_DIFF_PLOT): | $(PLOTS_DIR) $(CALIB_PLOTTER)
	$(CALIB_PLOTTER) \
		--reference $(REFERENCE_FILE) \
		--measurement $^ \
		--type "diff" \
		--output $@

all: $(ALL_TS_PLOT) $(ALL_SCATTER_PLOT) $(ALL_DIFF_PLOT)

define calib_rule
$(PLOTS_DIR)/$(calib_prefix)$(scatter_prefix)$(1).pdf: \
	$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv $(REFERENCE_FILE) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) $\\
		--reference $(REFERENCE_FILE) $\\
		--measurement $$< $\\
		--type "scatter" $\\
		--output $$@

all: $(PLOTS_DIR)/$(calib_prefix)$(scatter_prefix)$(1).pdf

$(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)$(1).pdf: \
	$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv $(REFERENCE_FILE) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) $\\
		--reference $(REFERENCE_FILE) $\\
		--measurement $$< $\\
		--type "timeseries" $\\
		--output $$@

all: $(PLOTS_DIR)/$(calib_prefix)$(timeseries_prefix)$(1).pdf

$(PLOTS_DIR)/$(calib_prefix)$(diff_prefix)$(1).pdf: \
	$(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv $(REFERENCE_FILE) \
	| $(PLOTS_DIR)
	$(CALIB_PLOTTER) $\\
		--reference $(REFERENCE_FILE) $\\
		--measurement $$< $\\
		--type "diff" $\\
		--output $$@

all: $(PLOTS_DIR)/$(calib_prefix)$(diff_prefix)$(1).pdf

$(ALL_TS_PLOT): $(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv
$(ALL_DIFF_PLOT): $(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv
$(ALL_SCATTER_PLOT): $(NETATMO_OUTPUT_DIR)/$(data_prefix)$(1).csv
endef

# rules for all devices
$(foreach device,$(NETATMO_DEVICES),$(eval $(call calib_rule,$(device))))

$(PLOTS_DIR)/$(calib_prefix)all.pdf: $(NETATMO_OUTPUT_DIR)

$(PLOTS_DIR):
	mkdir -p $@
