# Experiment 4

## Time frame

- 31.08.2017 14:37 - 01.09.2017 09:00 CEST

## Setup

- E-Labor back room
- modules placed **with free sensor** in oven **standing** on a standing grid (like in a microwave)
- oven set to **heating** and $`50^\circ C`$
- measure temperature with HMP
- place small PC ventilator in center because it produces less heat than the
  oven's ventilator
- wait for stationarity of netatmo devices until 15:12 CEST
- then switch oven off and leave small PC ventilator on

## Problems

- The oven is obviously still cooling down too quickly, at least above
  $`40^\circ C`$.

