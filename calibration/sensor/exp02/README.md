# Experiment 2

## Time frame

- 31.08.2017 10:36 - 11:39 CEST

## Setup

- E-Labor back room
- modules placed **with free sensor** in oven **standing** on a standing grid (like in a microwave)
- oven set to **heating** and $`60^\circ C`$
- module taken out at 10:47 CEST as they heated up to around $`50-55^\circ C`$
- modules placed with the standing grid on the floor (no ventilation)
- measured room temperature with HMP
- waited for cooling until 11:39 CEST

## Problems

- still one opening of the oven had a tiny tube sticking out
- I think I need to ventilate the modules during cooling to prevent heat
  accumulation?
