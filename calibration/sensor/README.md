# Sensor calibration

## Insights

- The oven does definitely not adjust homogeneously to the environment
  temperature.
- The door side is obviously better insulated.
- Thus the modules **ALPHA**, **ETA** and **GAMMA** which are also amongst the
  closest to the reference device have the highest correspondence to the
  referece device
