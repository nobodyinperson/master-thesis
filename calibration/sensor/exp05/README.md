# Experiment 5

## Time frame

- 05.09.2017 11:33 - 06.09.2017 10:55 CEST

## Setup

- E-Labor back room
- modules placed **with free sensor** in oven **standing** on a standing grid (like in a microwave)
- oven set to **cooling** and $`0^\circ C`$
- measure temperature with HMP
- place small PC ventilator in center because it produces less heat than the
  oven's ventilator
- wait for stationarity of netatmo devices until 12:00 CEST (NETATMO at around
  $`-2^\circ C`$)
- then switch oven off and leave small PC ventilator on

## Problems

