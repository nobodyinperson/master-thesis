# Experiment 1 - Determine approx. duration of oven cooling

## Time frame

- 30.08.2017 14:30 - 31.08.2017 10:21 CEST 

## Setup

- E-Labor back room
- oven alone with HMP inserted (insulated)
- oven set to $`60^\circ C`$
- **switched off** oven at 12:43 CEST as HMP reached around $`60^\circ C`$
- wait for cooldown

## Problems

- one rubber plug had still a small tube. This has to be replaced for the next
  experiment.

