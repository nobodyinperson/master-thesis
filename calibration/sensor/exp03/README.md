# Experiment 3

## Time frame

- 31.08.2017 13:00 - 13:45 CEST

## Setup

- E-Labor back room
- modules placed **with free sensor** in oven **standing** on a standing grid (like in a microwave)
- oven set to **cooling** and $`0^\circ C`$
- module taken out at 13:08 CEST as they cooled down to around $`0^\circ C`$
- modules placed with the standing grid on the floor (**with** ventilation)
- waited for cooling until ... CEST

## Problems

- ETA stopped sending at 13:13 CEST
